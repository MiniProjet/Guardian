package com.example.ezzinemayssa.guardianfinale.request;

import com.android.volley.Response;
import com.android.volley.toolbox.StringRequest;
import com.example.ezzinemayssa.guardianfinale.activity.MainActivity;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by mayss on 13/12/2017.
 */

public class AddAppRequest extends StringRequest {


    private static final String REGISTER_REQUEST_URL = "http://"+ MainActivity.ipAdress+"/guardenScript/AddInstalledApp.php";
    private Map<String, String> params;

    public AddAppRequest(String email,String image_name,String image,Response.Listener<String> listener) {
        super(Method.POST, REGISTER_REQUEST_URL, listener, null);
        params = new HashMap<>();
        params.put("email_child",email);
        params.put("appname",image_name);
        params.put("image",image);


    }

    @Override
    public Map<String, String> getParams() {
        return params;
    }
}

