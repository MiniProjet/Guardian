package com.example.ezzinemayssa.guardianfinale.activity.map;

import android.app.DialogFragment;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.view.View;
import android.widget.Button;
import android.widget.SeekBar;

import com.example.ezzinemayssa.guardianfinale.R;
import com.example.ezzinemayssa.guardianfinale.entities.Tracking;
import com.example.ezzinemayssa.guardianfinale.fragment.PickMeUpParentDialog;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

public class PickMeUpMapActivity extends FragmentActivity implements OnMapReadyCallback {

    private GoogleMap mMap;


    String email="halim@jerbi.com";
    DatabaseReference locations;
    Double lat,lng;
    SeekBar seekBar;
    Button btnPick;

    LatLng latLngOfPolygone,friendLocation;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pick_me_up_map);
        seekBar =(SeekBar) findViewById(R.id.seekBar);
        btnPick=(Button)findViewById(R.id.btn_pick);
        btnPick.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
               confirmFireMissiles();
            }
        });

        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        locations = FirebaseDatabase.getInstance().getReference("locations");





    }

    private void loadLocationForThisUser(final String email) {
        Query user_Location = locations.orderByChild("email").equalTo(email);
        user_Location.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                for(DataSnapshot postSnapShot:dataSnapshot.getChildren())
                {
                    Tracking tracking = postSnapShot.getValue(Tracking.class);


                    friendLocation=new LatLng(Double.parseDouble(tracking.getLat()),
                            Double.parseDouble(tracking.getLng()));

                    mMap.clear();

                    mMap.addMarker(new MarkerOptions()
                            .position(friendLocation).title(email)
                            .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_BLUE)));


                    CameraPosition cameraPosition = new CameraPosition.Builder().target(friendLocation).zoom(15.0f).build();
                    CameraUpdate cameraUpdate = CameraUpdateFactory.newCameraPosition(cameraPosition);
                    mMap.moveCamera(cameraUpdate);





                }

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }


    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        loadLocationForThisUser(email.trim());
        mMap.setOnMapClickListener(new GoogleMap.OnMapClickListener() {
            @Override
            public void onMapClick(LatLng latLng) {

                //  double new_lat = my_lat + coef;
                //double new_long = my_long + coef / Math.cos(latLng.longitude * 0.018);
                mMap.clear();


                mMap.addMarker(new MarkerOptions()
                        .position(friendLocation).title(email)
                        .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_BLUE)));

            }
        });

    }
    public void confirmFireMissiles() {
        DialogFragment newFragment = new PickMeUpParentDialog();
        newFragment.show(getFragmentManager(), "missiles");

    }
}
