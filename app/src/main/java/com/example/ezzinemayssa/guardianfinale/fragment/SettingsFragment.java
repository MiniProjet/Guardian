package com.example.ezzinemayssa.guardianfinale.fragment;


import android.content.SharedPreferences;
import android.os.Bundle;
import android.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.Switch;

import com.example.ezzinemayssa.guardianfinale.R;

import static android.content.Context.MODE_PRIVATE;

public class SettingsFragment extends Fragment {
Switch calls,sms,zone,applications;

    public SettingsFragment() {
        // Required empty public constructor
    }



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view =inflater.inflate(R.layout.fragment_settings, container, false);

        calls=view.findViewById(R.id.settings_calls);
        sms=view.findViewById(R.id.settings_sms);
        zone=view.findViewById(R.id.settings_allowedZone);
        applications=view.findViewById(R.id.seetings_application);


        SharedPreferences prefs = getActivity().getSharedPreferences("USER", MODE_PRIVATE);
        String restoredText = prefs.getString("calls", null);
        if (restoredText != null) {
            String callsStatus = prefs.getString("calls", "no");//"No name defined" is the default value.
            if(callsStatus.equals("yes")){
                calls.setChecked(true);
            }

        }


        String restoredText2 = prefs.getString("sms", null);
        if (restoredText2 != null) {
            String callsStatus = prefs.getString("sms", "no");//"No name defined" is the default value.
            if(callsStatus.equals("yes")){
                sms.setChecked(true);
            }

        }

        String restoredText3 = prefs.getString("zone", null);
        if (restoredText3 != null) {
            String callsStatus = prefs.getString("zone", "no");//"No name defined" is the default value.
            if(callsStatus.equals("yes")){
                zone.setChecked(true);
            }

        }


        String restoredText4 = prefs.getString("app", null);
        if (restoredText != null) {
            String callsStatus = prefs.getString("app", "no");//"No name defined" is the default value.
            if(callsStatus.equals("yes")){
                applications.setChecked(true);
            }

        }

        calls.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {

            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked){
                    SharedPreferences pref = getActivity().getApplicationContext().getSharedPreferences("USER", 0);
                    SharedPreferences.Editor editor = pref.edit();
                    editor.putString("calls", "yes");
                    editor.commit();
                 }
                 else{
                    SharedPreferences pref = getActivity().getApplicationContext().getSharedPreferences("USER", 0);
                    SharedPreferences.Editor editor = pref.edit();
                    editor.putString("calls", "no");
                    editor.commit();


                }
            }

        });

        sms.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {

            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked){
                    SharedPreferences pref = getActivity().getApplicationContext().getSharedPreferences("USER", 0);
                    SharedPreferences.Editor editor = pref.edit();
                    editor.putString("sms", "yes");
                    editor.commit();
                }
                else{
                    SharedPreferences pref = getActivity().getApplicationContext().getSharedPreferences("USER", 0);
                    SharedPreferences.Editor editor = pref.edit();
                    editor.putString("sms", "no");
                    editor.commit();


                }
            }

        });


        zone.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {

            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked){
                    SharedPreferences pref = getActivity().getApplicationContext().getSharedPreferences("USER", 0);
                    SharedPreferences.Editor editor = pref.edit();
                    editor.putString("zone", "yes");
                    editor.commit();
                }
                else{
                    SharedPreferences pref = getActivity().getApplicationContext().getSharedPreferences("USER", 0);
                    SharedPreferences.Editor editor = pref.edit();
                    editor.putString("zone", "no");
                    editor.commit();


                }
            }

        });

        applications.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {

            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked){
                    SharedPreferences pref = getActivity().getApplicationContext().getSharedPreferences("USER", 0);
                    SharedPreferences.Editor editor = pref.edit();
                    editor.putString("app", "yes");
                    editor.commit();
                }
                else{
                    SharedPreferences pref = getActivity().getApplicationContext().getSharedPreferences("USER", 0);
                    SharedPreferences.Editor editor = pref.edit();
                    editor.putString("app", "no");
                    editor.commit();


                }
            }

        });
            return view;
    }


}
