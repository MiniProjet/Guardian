package com.example.ezzinemayssa.guardianfinale.request;

import com.android.volley.Response;
import com.android.volley.toolbox.StringRequest;
import com.squareup.picasso.Downloader;

import java.util.HashMap;
import java.util.Map;

import static com.example.ezzinemayssa.guardianfinale.activity.MainActivity.ipAdress;

/**
 * Created by mayss on 20/11/2017.
 */

public class GetLocationOfOneUser extends StringRequest {

    private static final String LOGIN_REQUEST_URL = "http://"+ipAdress+"/guardenScript/GetLocationOfOneUser.php";
    private Map<String, String> params;

    public GetLocationOfOneUser(String email, Response.Listener<String> listener, Response.ErrorListener listener2) {
        super(Method.POST, LOGIN_REQUEST_URL, listener, listener2);
        params = new HashMap<>();
        params.put("email", email);


    }

    @Override
    public Map<String, String> getParams() {
        return params;
    }

}
