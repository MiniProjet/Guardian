package com.example.ezzinemayssa.guardianfinale.service;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.IBinder;
import android.util.Log;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.toolbox.Volley;
import com.example.ezzinemayssa.guardianfinale.activity.map.AppercuChildMap;
import com.example.ezzinemayssa.guardianfinale.entities.Parent;
import com.example.ezzinemayssa.guardianfinale.entities.Tracking;
import com.example.ezzinemayssa.guardianfinale.entities.ZoneChild;
import com.example.ezzinemayssa.guardianfinale.request.AddLocationOfOneUser;
import com.example.ezzinemayssa.guardianfinale.request.GetLocationOfOneUser;
import com.example.ezzinemayssa.guardianfinale.request.GetRefreshTokken;
import com.example.ezzinemayssa.guardianfinale.request.GetZoneRequest;
import com.example.ezzinemayssa.guardianfinale.request.SendingNotifRequest;
import com.example.ezzinemayssa.guardianfinale.utils.MySingleton;
import com.google.android.gms.maps.model.LatLng;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;

/**
 * Created by mayss on 22/11/2017.
 */

public class SendingLocation extends Service {

    DatabaseReference locations;
    FirebaseAuth firebaseAuth;
    List<ZoneChild> zoneChildList;
    List<Parent> parent ;
    String userConnected;
    private static final String TAG = "BOOMBOOMTESTGPS";
    private LocationManager mLocationManager = null;
    private static final int LOCATION_INTERVAL = 1000;
    private static final float LOCATION_DISTANCE = 10f;
    String localTime;
    Location mLastLocation;
    String tokken;
    private class LocationListener implements android.location.LocationListener{

        public LocationListener(String provider)
        {
            Log.e(TAG, "LocationListener " + provider);
            mLastLocation = new Location(provider);
            Log.e(TAG, "Localization " + mLastLocation.getLatitude());
        }
        @Override
        public void onLocationChanged(Location location)
        {
            SharedPreferences prefs = getSharedPreferences("USER", MODE_PRIVATE);
            String restoredText = prefs.getString("userConnected", null);
            if (restoredText != null) {
                userConnected = prefs.getString("userConnected", "No name defined");//"No name defined" is the default value.

                Log.e("user",userConnected);
            }


            Log.e(TAG, "onLocationChanged: " + location);
            mLastLocation.set(location);
            locations= FirebaseDatabase.getInstance().getReference().child("locations");
            locations.child("FirebaseAuth")
                    .setValue(new Tracking(userConnected
                            ,"xxx",
                            String.valueOf(mLastLocation.getLatitude()),String.valueOf(mLastLocation.getLongitude())));
            getZoneOfOneChild(userConnected);
            historyLocation(userConnected,firebaseAuth.getCurrentUser().getUid(),location.getLatitude()+"",
                    location.getLongitude()+"");
            Log.e(TAG, "Localization " + mLastLocation.getLatitude());
        }
        @Override
        public void onProviderDisabled(String provider)
        {
            Log.e(TAG, "onProviderDisabled: " + provider);
        }
        @Override
        public void onProviderEnabled(String provider)
        {
            Log.e(TAG, "onProviderEnabled: " + provider);
        }
        @Override
        public void onStatusChanged(String provider, int status, Bundle extras)
        {
            Log.e(TAG, "onStatusChanged: " + provider);
        }
    }
    LocationListener[] mLocationListeners = new LocationListener[] {
            new LocationListener(LocationManager.GPS_PROVIDER),
            new LocationListener(LocationManager.NETWORK_PROVIDER)
    };
    @Override
    public IBinder onBind(Intent arg0)
    {
        return null;
    }
    @Override
    public int onStartCommand(Intent intent, int flags, int startId)
    {
        Log.e(TAG, "onStartCommand");
        super.onStartCommand(intent, flags, startId);
        Log.e("etape0","0");
        firebaseAuth = FirebaseAuth.getInstance();
        zoneChildList = new ArrayList<>();
        getZoneOfOneChild(userConnected);
        return START_STICKY;
    }
    @Override
    public void onCreate()
    {
        Log.e(TAG, "onCreate");
        parent= new ArrayList<>();
        initializeLocationManager();
        try {
            mLocationManager.requestLocationUpdates(
                    LocationManager.NETWORK_PROVIDER, LOCATION_INTERVAL, LOCATION_DISTANCE,
                    mLocationListeners[1]);
        } catch (SecurityException ex) {
            Log.i(TAG, "fail to request location update, ignore", ex);
        } catch (IllegalArgumentException ex) {
            Log.d(TAG, "network provider does not exist, " + ex.getMessage());
        }
        try {
            mLocationManager.requestLocationUpdates(
                    LocationManager.GPS_PROVIDER, LOCATION_INTERVAL, LOCATION_DISTANCE,
                    mLocationListeners[0]);
        } catch (SecurityException ex) {
            Log.i(TAG, "fail to request location update, ignore", ex);
        } catch (IllegalArgumentException ex) {
            Log.d(TAG, "gps provider does not exist " + ex.getMessage());
        }
    }
    @Override
    public void onDestroy()
    {
        Log.e(TAG, "onDestroy");
        super.onDestroy();
        if (mLocationManager != null) {
            for (int i = 0; i < mLocationListeners.length; i++) {
                try {
                    mLocationManager.removeUpdates(mLocationListeners[i]);
                } catch (Exception ex) {
                    Log.i(TAG, "fail to remove location listners, ignore", ex);
                }
            }
        }
    }
    private void initializeLocationManager() {
        Log.e(TAG, "initializeLocationManager");
        if (mLocationManager == null) {
            mLocationManager = (LocationManager) getApplicationContext().getSystemService(Context.LOCATION_SERVICE);
        }
    }

    private void historyLocation(String email,String uid ,String lat,String lng){
        Response.Listener<String> responseListener3 = new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {




            }
        };

        AddLocationOfOneUser addlocation = new AddLocationOfOneUser(email,uid,lat,lng,responseListener3);
        RequestQueue queue = MySingleton.getInstance(SendingLocation.this).getRequestQueue();
// Add a request (in this example, called stringRequest) to your RequestQueue.
        MySingleton.getInstance(SendingLocation.this).addToRequestQueue(addlocation);

    }

    public void getZoneOfOneChild(final String email){
        zoneChildList = new ArrayList<>();
        Log.e("etape1","1");
        Response.Listener<String> responseListener = new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {


                try {
                    JSONObject jsonResponse = new JSONObject(response);

                    JSONArray jsonArray= jsonResponse.getJSONArray("zone");
                    int count= 0;
                    Log.e("ListZoneChild",response);
                    while(count<jsonArray.length())
                    {
                        JSONObject jo= jsonArray.getJSONObject(count);
                        zoneChildList.add(new ZoneChild(jo.getString("email_enfant"), jo.getString("lat00"), jo.getString("lng00"),
                                jo.getString("lat01"), jo.getString("lng01"), jo.getString("lat10"), jo.getString("lng10"),
                                jo.getString("lat11"), jo.getString("lng11"), jo.getString("jour"),
                                jo.getString("mardi"),jo.getString("mercredi"),
                                jo.getString("jeudi"),jo.getString("vendredi"),jo.getString("samedi"),
                                jo.getString("dimanche"),jo.getString("datedebut")
                                , jo.getString("datefin")));
                        count++;




                    }


                    verifierZoneOfOneChild();

                } catch (JSONException e) {
                    e.printStackTrace();
                }


            }
        };
        GetZoneRequest addlocation = new GetZoneRequest(userConnected,responseListener);
        RequestQueue queue = MySingleton.getInstance(SendingLocation.this).getRequestQueue();
// Add a request (in this example, called stringRequest) to your RequestQueue.
        MySingleton.getInstance(SendingLocation.this).addToRequestQueue(addlocation);


    }

    public void verifierZoneOfOneChild(){


        Calendar cal = Calendar.getInstance(TimeZone.getTimeZone("GMT+1:00"));
        Date currentLocalTime = cal.getTime();
        DateFormat date = new SimpleDateFormat("HH:mm a");
        date.setTimeZone(TimeZone.getTimeZone("GMT+1:00"));

        localTime = date.format(currentLocalTime);


        Calendar calendar = Calendar.getInstance();
        int day = calendar.get(Calendar.DAY_OF_WEEK);


        // sendNotification("helloo","a7la 3alam");
        LatLng childLocation=new LatLng(mLastLocation.getLatitude(),mLastLocation.getLatitude());


        switch (day) {
            case Calendar.THURSDAY:
                Log.e("Verifier4","Verifier");
                verifierJeudi(childLocation);
                break;
            case Calendar.MONDAY:
                Log.e("Verifier1","Verifier");
                verifierLundi(childLocation);
                break;
            case Calendar.TUESDAY:
                Log.e("Verifier2","Verifier");
                verifierMardi(childLocation);
                break;
            case Calendar.WEDNESDAY:
                Log.e("Verifier3","Verifier");
                verifierMercredi(childLocation);
                break;
            case Calendar.FRIDAY:
                Log.e("Verifier5","Verifier");
                verifierVendredi(childLocation);
                break;
            case Calendar.SATURDAY:
                Log.e("Verifier6","Verifier");
                verifierSamedi(childLocation);
                break;
            case Calendar.SUNDAY:
                Log.e("Verifier7","Verifier");
                verifierDimanche(childLocation);
                break;

        }








    }

    public void verifierJeudi(LatLng childLocation){
        for(int i=0;i<zoneChildList.size();i++)  {
            Log.e("debut",zoneChildList.get(i).getDateDeubt().substring(0,2).trim());
            Log.e("actuel",localTime.substring(0,2).trim());
            Log.e("fin",zoneChildList.get(i).getDateFin().substring(0,2).trim());

            if(zoneChildList.get(i).getJeudi().equals("oui")){
                Log.e("0.5","0.5");
                if((Integer.parseInt(zoneChildList.get(i).getDateDeubt().substring(0,1))
                        <=Integer.parseInt(localTime.substring(0,2)))
                        &&
                        (Integer.parseInt(localTime.substring(0,2))
                                <=Integer.parseInt(zoneChildList.get(i).getDateFin().substring(0,2)))){
                    Log.e("1","1");
                    if((Float.parseFloat(zoneChildList.get(i).getLat00()) <childLocation.latitude)&&
                            (childLocation.latitude<Float.parseFloat(zoneChildList.get(i).getLat10()))
                            &&(Float.parseFloat(zoneChildList.get(i).getLng00()) <childLocation.longitude)&&
                            (childLocation.longitude<Float.parseFloat(zoneChildList.get(i).getLng01()))){
                        //enfant jawou behi//
                        Log.e("2","2");
                    }else{
                        Log.e("3","3");

                        Response.Listener<String> responseListener2 = new Response.Listener<String>() {
                            @Override
                            public void onResponse(String response) {

                                try {
                                    JSONObject jsonResponse = new JSONObject(response);

                                    JSONArray jsonArray= jsonResponse.getJSONArray("tokken");
                                    int count= 0;
                                    while(count<jsonArray.length())
                                    {
                                        JSONObject jo= jsonArray.getJSONObject(count);

                                        tokken= jo.getString("tokken");
                                        count++;

                                        Log.e("Response",tokken);
                                        Response.Listener<String> responseListener = new Response.Listener<String>() {
                                            @Override
                                            public void onResponse(String response) {


                                            }
                                        };

                                        SendingNotifRequest getChildrenByEmailParentRequest = new SendingNotifRequest(
                                                "Zone",  tokken,"Your child "+ userConnected+" has passed an allowed zone", responseListener);
                                        RequestQueue queue = MySingleton.getInstance(SendingLocation.this).getRequestQueue();
// Add a request (in this example, called stringRequest) to your RequestQueue.
                                        MySingleton.getInstance(SendingLocation.this).addToRequestQueue(getChildrenByEmailParentRequest);
                                    }

                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }




                            }
                        };

                        GetRefreshTokken tokkenParent = new GetRefreshTokken(userConnected, responseListener2);
                        RequestQueue queue = MySingleton.getInstance(SendingLocation.this).getRequestQueue();
// Add a request (in this example, called stringRequest) to your RequestQueue.
                        MySingleton.getInstance(SendingLocation.this).addToRequestQueue(tokkenParent);




                    }

                }}}
    }
    public void verifierVendredi(LatLng childLocation){

        for(int i=0;i<zoneChildList.size();i++)  {
            Log.e("debut",zoneChildList.get(i).getDateDeubt().substring(0,2).trim());
            Log.e("actuel",localTime.substring(0,2).trim());
            Log.e("fin",zoneChildList.get(i).getDateFin().substring(0,2).trim());

            if(zoneChildList.get(i).getVendredi().equals("oui")){
                Log.e("0.5","0.5");
                if((Integer.parseInt(zoneChildList.get(i).getDateDeubt().substring(0,1))
                        <=Integer.parseInt(localTime.substring(0,2)))
                        &&
                        (Integer.parseInt(localTime.substring(0,2))
                                <=Integer.parseInt(zoneChildList.get(i).getDateFin().substring(0,2)))){
                    Log.e("1","1");
                    if((Float.parseFloat(zoneChildList.get(i).getLat00()) <childLocation.latitude)&&
                            (childLocation.latitude<Float.parseFloat(zoneChildList.get(i).getLat10()))
                            &&(Float.parseFloat(zoneChildList.get(i).getLng00()) <childLocation.longitude)&&
                            (childLocation.longitude<Float.parseFloat(zoneChildList.get(i).getLng01()))){
                        //enfant jawou behi//
                        Log.e("2","2");
                    }else{
                        Log.e("3","3");

                        Response.Listener<String> responseListener2 = new Response.Listener<String>() {
                            @Override
                            public void onResponse(String response) {

                                try {
                                    JSONObject jsonResponse = new JSONObject(response);

                                    JSONArray jsonArray= jsonResponse.getJSONArray("tokken");
                                    int count= 0;
                                    while(count<jsonArray.length())
                                    {
                                        JSONObject jo= jsonArray.getJSONObject(count);

                                        tokken= jo.getString("tokken");
                                        count++;

                                        Log.e("Response",tokken);
                                        Response.Listener<String> responseListener = new Response.Listener<String>() {
                                            @Override
                                            public void onResponse(String response) {


                                            }
                                        };

                                        SendingNotifRequest getChildrenByEmailParentRequest = new SendingNotifRequest(
                                                "Zone",  tokken,"Votre Enfant"+userConnected+"a depassé une zone", responseListener);
                                        RequestQueue queue = MySingleton.getInstance(SendingLocation.this).getRequestQueue();
// Add a request (in this example, called stringRequest) to your RequestQueue.
                                        MySingleton.getInstance(SendingLocation.this).addToRequestQueue(getChildrenByEmailParentRequest);
                                    }

                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }




                            }
                        };

                        GetRefreshTokken tokkenParent = new GetRefreshTokken(userConnected, responseListener2);
                        RequestQueue queue = MySingleton.getInstance(SendingLocation.this).getRequestQueue();
// Add a request (in this example, called stringRequest) to your RequestQueue.
                        MySingleton.getInstance(SendingLocation.this).addToRequestQueue(tokkenParent);





                    }

                }}}
    }
    public void verifierSamedi(LatLng childLocation){

        for(int i=0;i<zoneChildList.size();i++)  {
            Log.e("debut",zoneChildList.get(i).getDateDeubt().substring(0,2).trim());
            Log.e("actuel",localTime.substring(0,2).trim());
            Log.e("fin",zoneChildList.get(i).getDateFin().substring(0,2).trim());

            if(zoneChildList.get(i).getSamedi().equals("oui")){
                Log.e("0.5","0.5");
                if((Integer.parseInt(zoneChildList.get(i).getDateDeubt().substring(0,2))
                        <=Integer.parseInt(localTime.substring(0,2)))
                        &&
                        (Integer.parseInt(localTime.substring(0,2))
                                <=Integer.parseInt(zoneChildList.get(i).getDateFin().substring(0,2)))){
                    Log.e("1","1");
                    if((Float.parseFloat(zoneChildList.get(i).getLat00()) <childLocation.latitude)&&
                            (childLocation.latitude<Float.parseFloat(zoneChildList.get(i).getLat10()))
                            &&(Float.parseFloat(zoneChildList.get(i).getLng00()) <childLocation.longitude)&&
                            (childLocation.longitude<Float.parseFloat(zoneChildList.get(i).getLng01()))){
                        //enfant jawou behi//
                        Log.e("2","2");
                    }else{
                        Log.e("3","3");

                        Response.Listener<String> responseListener2 = new Response.Listener<String>() {
                            @Override
                            public void onResponse(String response) {

                                try {
                                    JSONObject jsonResponse = new JSONObject(response);

                                    JSONArray jsonArray= jsonResponse.getJSONArray("tokken");
                                    int count= 0;
                                    while(count<jsonArray.length())
                                    {
                                        JSONObject jo= jsonArray.getJSONObject(count);

                                        tokken= jo.getString("tokken");
                                        count++;

                                        Log.e("Response",tokken);
                                        Response.Listener<String> responseListener = new Response.Listener<String>() {
                                            @Override
                                            public void onResponse(String response) {


                                            }
                                        };

                                        SendingNotifRequest getChildrenByEmailParentRequest = new SendingNotifRequest(
                                                "Zone",  tokken,"Votre Enfant"+userConnected+"a depassé une zone", responseListener);
                                        RequestQueue queue = MySingleton.getInstance(SendingLocation.this).getRequestQueue();
// Add a request (in this example, called stringRequest) to your RequestQueue.
                                        MySingleton.getInstance(SendingLocation.this).addToRequestQueue(getChildrenByEmailParentRequest);
                                    }

                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }




                            }
                        };

                        GetRefreshTokken tokkenParent = new GetRefreshTokken(userConnected, responseListener2);
                        RequestQueue queue = MySingleton.getInstance(SendingLocation.this).getRequestQueue();
// Add a request (in this example, called stringRequest) to your RequestQueue.
                        MySingleton.getInstance(SendingLocation.this).addToRequestQueue(tokkenParent);
                    }

                }}}
    }
    public void verifierDimanche(LatLng childLocation){
        for(int i=0;i<zoneChildList.size();i++)  {
            Log.e("debut",zoneChildList.get(i).getDateDeubt().substring(0,2).trim());
            Log.e("actuel",localTime.substring(0,2).trim());
            Log.e("fin",zoneChildList.get(i).getDateFin().substring(0,2).trim());

            if(zoneChildList.get(i).getDimanche().equals("oui")){
                Log.e("0.5","0.5");
                if((Integer.parseInt(zoneChildList.get(i).getDateDeubt().substring(0,1))
                        <=Integer.parseInt(localTime.substring(0,2)))
                        &&
                        (Integer.parseInt(localTime.substring(0,2))
                                <=Integer.parseInt(zoneChildList.get(i).getDateFin().substring(0,2)))){
                    Log.e("1","1");
                    if((Float.parseFloat(zoneChildList.get(i).getLat00()) <childLocation.latitude)&&
                            (childLocation.latitude<Float.parseFloat(zoneChildList.get(i).getLat10()))
                            &&(Float.parseFloat(zoneChildList.get(i).getLng00()) <childLocation.longitude)&&
                            (childLocation.longitude<Float.parseFloat(zoneChildList.get(i).getLng01()))){
                        //enfant jawou behi//
                        Log.e("2","2");
                    }else{
                        Log.e("3","3");

                        Response.Listener<String> responseListener2 = new Response.Listener<String>() {
                            @Override
                            public void onResponse(String response) {

                                try {
                                    JSONObject jsonResponse = new JSONObject(response);

                                    JSONArray jsonArray= jsonResponse.getJSONArray("tokken");
                                    int count= 0;
                                    while(count<jsonArray.length())
                                    {
                                        JSONObject jo= jsonArray.getJSONObject(count);

                                        tokken= jo.getString("tokken");
                                        count++;

                                        Log.e("Response",tokken);
                                        Response.Listener<String> responseListener = new Response.Listener<String>() {
                                            @Override
                                            public void onResponse(String response) {


                                            }
                                        };

                                        SendingNotifRequest getChildrenByEmailParentRequest = new SendingNotifRequest(
                                                "Zone",  tokken,"Votre Enfant"+ userConnected+"a depassé une zone", responseListener);
                                        RequestQueue queue = MySingleton.getInstance(SendingLocation.this).getRequestQueue();
// Add a request (in this example, called stringRequest) to your RequestQueue.
                                        MySingleton.getInstance(SendingLocation.this).addToRequestQueue(getChildrenByEmailParentRequest);
                                    }

                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }




                            }
                        };

                        GetRefreshTokken tokkenParent = new GetRefreshTokken(userConnected, responseListener2);
                        RequestQueue queue = MySingleton.getInstance(SendingLocation.this).getRequestQueue();
// Add a request (in this example, called stringRequest) to your RequestQueue.
                        MySingleton.getInstance(SendingLocation.this).addToRequestQueue(tokkenParent);



                    }

                }}}
    }
    public void verifierLundi(LatLng childLocation){
        for(int i=0;i<zoneChildList.size();i++)  {
            Log.e("debut",zoneChildList.get(i).getDateDeubt().substring(0,2).trim());
            Log.e("actuel",localTime.substring(0,2).trim());
            Log.e("fin",zoneChildList.get(i).getDateFin().substring(0,2).trim());

            if(zoneChildList.get(i).getJour().equals("oui")){
                Log.e("0.5","0.5");
                if((Integer.parseInt(zoneChildList.get(i).getDateDeubt().substring(0,1))
                        <=Integer.parseInt(localTime.substring(0,2)))
                        &&
                        (Integer.parseInt(localTime.substring(0,2))
                                <=Integer.parseInt(zoneChildList.get(i).getDateFin().substring(0,2)))){
                    Log.e("1","1");
                    if((Float.parseFloat(zoneChildList.get(i).getLat00()) <childLocation.latitude)&&
                            (childLocation.latitude<Float.parseFloat(zoneChildList.get(i).getLat10()))
                            &&(Float.parseFloat(zoneChildList.get(i).getLng00()) <childLocation.longitude)&&
                            (childLocation.longitude<Float.parseFloat(zoneChildList.get(i).getLng01()))){
                        //enfant jawou behi//
                        Log.e("2","2");
                    }else{
                        Log.e("3","3");

                        Response.Listener<String> responseListener2 = new Response.Listener<String>() {
                            @Override
                            public void onResponse(String response) {

                                try {
                                    JSONObject jsonResponse = new JSONObject(response);

                                    JSONArray jsonArray= jsonResponse.getJSONArray("tokken");
                                    int count= 0;
                                    while(count<jsonArray.length())
                                    {
                                        JSONObject jo= jsonArray.getJSONObject(count);

                                        tokken= jo.getString("tokken");
                                        count++;

                                        Log.e("Response",tokken);
                                        Response.Listener<String> responseListener = new Response.Listener<String>() {
                                            @Override
                                            public void onResponse(String response) {


                                            }
                                        };

                                        SendingNotifRequest getChildrenByEmailParentRequest = new SendingNotifRequest(
                                                "Zone",  tokken,"Votre Enfant"+userConnected+"a depassé une zone", responseListener);
                                        RequestQueue queue = MySingleton.getInstance(SendingLocation.this).getRequestQueue();
// Add a request (in this example, called stringRequest) to your RequestQueue.
                                        MySingleton.getInstance(SendingLocation.this).addToRequestQueue(getChildrenByEmailParentRequest);
                                    }

                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }




                            }
                        };

                        GetRefreshTokken tokkenParent = new GetRefreshTokken(userConnected, responseListener2);
                        RequestQueue queue = MySingleton.getInstance(SendingLocation.this).getRequestQueue();
// Add a request (in this example, called stringRequest) to your RequestQueue.
                        MySingleton.getInstance(SendingLocation.this).addToRequestQueue(tokkenParent);

                    }

                }}}
    }
    public void verifierMardi(LatLng childLocation){
        for(int i=0;i<zoneChildList.size();i++)  {
            Log.e("debut",zoneChildList.get(i).getDateDeubt().substring(0,2).trim());
            Log.e("actuel",localTime.substring(0,2).trim());
            Log.e("fin",zoneChildList.get(i).getDateFin().substring(0,2).trim());

            if(zoneChildList.get(i).getMardi().equals("oui")){
                Log.e("0.5","0.5");
                if((Integer.parseInt(zoneChildList.get(i).getDateDeubt().substring(0,1))
                        <=Integer.parseInt(localTime.substring(0,2)))
                        &&
                        (Integer.parseInt(localTime.substring(0,2))
                                <=Integer.parseInt(zoneChildList.get(i).getDateFin().substring(0,2)))){
                    Log.e("1","1");
                    if((Float.parseFloat(zoneChildList.get(i).getLat00()) <childLocation.latitude)&&
                            (childLocation.latitude<Float.parseFloat(zoneChildList.get(i).getLat10()))
                            &&(Float.parseFloat(zoneChildList.get(i).getLng00()) <childLocation.longitude)&&
                            (childLocation.longitude<Float.parseFloat(zoneChildList.get(i).getLng01()))){
                        //enfant jawou behi//
                        Log.e("2","2");
                    }else{
                        Log.e("3","3");

                        Response.Listener<String> responseListener2 = new Response.Listener<String>() {
                            @Override
                            public void onResponse(String response) {

                                try {
                                    JSONObject jsonResponse = new JSONObject(response);

                                    JSONArray jsonArray= jsonResponse.getJSONArray("tokken");
                                    int count= 0;
                                    while(count<jsonArray.length())
                                    {
                                        JSONObject jo= jsonArray.getJSONObject(count);

                                        tokken= jo.getString("tokken");
                                        count++;

                                        Log.e("Response",tokken);
                                        Response.Listener<String> responseListener = new Response.Listener<String>() {
                                            @Override
                                            public void onResponse(String response) {


                                            }
                                        };

                                        SendingNotifRequest getChildrenByEmailParentRequest = new SendingNotifRequest(
                                                "Zone",  tokken,"Votre Enfant"+userConnected+"a depassé une zone", responseListener);
                                        RequestQueue queue = MySingleton.getInstance(SendingLocation.this).getRequestQueue();
// Add a request (in this example, called stringRequest) to your RequestQueue.
                                        MySingleton.getInstance(SendingLocation.this).addToRequestQueue(getChildrenByEmailParentRequest);
                                    }

                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }




                            }
                        };

                        GetRefreshTokken tokkenParent = new GetRefreshTokken(userConnected, responseListener2);
                        RequestQueue queue = MySingleton.getInstance(SendingLocation.this).getRequestQueue();
// Add a request (in this example, called stringRequest) to your RequestQueue.
                        MySingleton.getInstance(SendingLocation.this).addToRequestQueue(tokkenParent);



                    }

                }}}
    }
    public void verifierMercredi(LatLng childLocation){
        for(int i=0;i<zoneChildList.size();i++)  {
            Log.e("debut",zoneChildList.get(i).getDateDeubt().substring(0,2).trim());
            Log.e("actuel",localTime.substring(0,2).trim());
            Log.e("fin",zoneChildList.get(i).getDateFin().substring(0,2).trim());

            if(zoneChildList.get(i).getMercredi().equals("oui")){
                Log.e("0.5","0.5");
                if((Integer.parseInt(zoneChildList.get(i).getDateDeubt().substring(0,1))
                        <=Integer.parseInt(localTime.substring(0,2)))
                        &&
                        (Integer.parseInt(localTime.substring(0,2))
                                <=Integer.parseInt(zoneChildList.get(i).getDateFin().substring(0,2)))){
                    Log.e("1","1");
                    if((Float.parseFloat(zoneChildList.get(i).getLat00()) <childLocation.latitude)&&
                            (childLocation.latitude<Float.parseFloat(zoneChildList.get(i).getLat10()))
                            &&(Float.parseFloat(zoneChildList.get(i).getLng00()) <childLocation.longitude)&&
                            (childLocation.longitude<Float.parseFloat(zoneChildList.get(i).getLng01()))){
                        //enfant jawou behi//
                        Log.e("2","2");
                    }else{
                        Log.e("3","3");

                        Response.Listener<String> responseListener2 = new Response.Listener<String>() {
                            @Override
                            public void onResponse(String response) {

                                try {
                                    JSONObject jsonResponse = new JSONObject(response);

                                    JSONArray jsonArray= jsonResponse.getJSONArray("tokken");
                                    int count= 0;
                                    while(count<jsonArray.length())
                                    {
                                        JSONObject jo= jsonArray.getJSONObject(count);

                                        tokken= jo.getString("tokken");
                                        count++;

                                        Log.e("Response",tokken);
                                        Response.Listener<String> responseListener = new Response.Listener<String>() {
                                            @Override
                                            public void onResponse(String response) {


                                            }
                                        };

                                        SendingNotifRequest getChildrenByEmailParentRequest = new SendingNotifRequest(
                                                "Zone",  tokken,"Votre Enfant"+userConnected+"a depassé une zone", responseListener);
                                        RequestQueue queue = MySingleton.getInstance(SendingLocation.this).getRequestQueue();
// Add a request (in this example, called stringRequest) to your RequestQueue.
                                        MySingleton.getInstance(SendingLocation.this).addToRequestQueue(getChildrenByEmailParentRequest);
                                    }

                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }




                            }
                        };

                        GetRefreshTokken tokkenParent = new GetRefreshTokken(userConnected, responseListener2);
                        RequestQueue queue = MySingleton.getInstance(SendingLocation.this).getRequestQueue();
// Add a request (in this example, called stringRequest) to your RequestQueue.
                        MySingleton.getInstance(SendingLocation.this).addToRequestQueue(tokkenParent);





                    }

                }}}
    }

}




