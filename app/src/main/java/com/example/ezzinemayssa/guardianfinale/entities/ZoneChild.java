package com.example.ezzinemayssa.guardianfinale.entities;

/**
 * Created by mayss on 21/11/2017.
 */

public class ZoneChild {
    String emailChild,lat00,lng00,lat01,lng01,lat10,lng10,lat11,lng11,jour,mardi,mercredi,jeudi,vendredi,samedi,dimanche,dateDeubt,dateFin;

    public String getMardi() {
        return mardi;
    }

    public void setMardi(String mardi) {
        this.mardi = mardi;
    }

    public String getMercredi() {
        return mercredi;
    }

    public void setMercredi(String mercredi) {
        this.mercredi = mercredi;
    }

    public String getJeudi() {
        return jeudi;
    }

    public void setJeudi(String jeudi) {
        this.jeudi = jeudi;
    }

    public String getVendredi() {
        return vendredi;
    }

    public void setVendredi(String vendredi) {
        this.vendredi = vendredi;
    }

    public String getSamedi() {
        return samedi;
    }

    public void setSamedi(String samedi) {
        this.samedi = samedi;
    }

    public String getDimanche() {
        return dimanche;
    }

    public void setDimanche(String dimanche) {
        this.dimanche = dimanche;
    }

    public ZoneChild(String emailChild, String lat00, String lng00, String lat01, String lng01, String lat10,
                     String lng10, String lat11, String lng11, String jour,
                     String mardi, String mercredi, String jeudi, String vendredi, String samedi, String dimanche,
                     String dateDeubt, String dateFin) {
        this.emailChild = emailChild;
        this.lat00 = lat00;
        this.lng00 = lng00;
        this.lat01 = lat01;
        this.lng01 = lng01;
        this.lat10 = lat10;
        this.lng10 = lng10;
        this.lat11 = lat11;
        this.lng11 = lng11;
        this.jour = jour;
        this.mardi = mardi;
        this.mercredi = mercredi;
        this.jeudi = jeudi;
        this.vendredi = vendredi;
        this.samedi = samedi;
        this.dimanche = dimanche;
        this.dateDeubt = dateDeubt;
        this.dateFin = dateFin;
    }

    @Override
    public String toString() {
        return "ZoneChild{" +
                "emailChild='" + emailChild + '\'' +
                ", lat00='" + lat00 + '\'' +
                ", lng00='" + lng00 + '\'' +
                ", lat01='" + lat01 + '\'' +
                ", lng01='" + lng01 + '\'' +
                ", lat10='" + lat10 + '\'' +
                ", lng10='" + lng10 + '\'' +
                ", lat11='" + lat11 + '\'' +
                ", lng11='" + lng11 + '\'' +
                ", jour='" + jour + '\'' +
                ", dateDeubt='" + dateDeubt + '\'' +
                ", dateFin='" + dateFin + '\'' +
                '}';
    }

    public ZoneChild() {
    }

    public String getEmailChild() {
        return emailChild;
    }

    public void setEmailChild(String emailChild) {
        this.emailChild = emailChild;
    }

    public String getLat00() {
        return lat00;
    }

    public void setLat00(String lat00) {
        this.lat00 = lat00;
    }

    public String getLng00() {
        return lng00;
    }

    public void setLng00(String lng00) {
        this.lng00 = lng00;
    }

    public String getLat01() {
        return lat01;
    }

    public void setLat01(String lat01) {
        this.lat01 = lat01;
    }

    public String getLng01() {
        return lng01;
    }

    public void setLng01(String lng01) {
        this.lng01 = lng01;
    }

    public String getLat10() {
        return lat10;
    }

    public void setLat10(String lat10) {
        this.lat10 = lat10;
    }

    public String getLng10() {
        return lng10;
    }

    public void setLng10(String lng10) {
        this.lng10 = lng10;
    }

    public String getLat11() {
        return lat11;
    }

    public void setLat11(String lat11) {
        this.lat11 = lat11;
    }

    public String getLng11() {
        return lng11;
    }

    public void setLng11(String lng11) {
        this.lng11 = lng11;
    }

    public String getJour() {
        return jour;
    }

    public void setJour(String jour) {
        this.jour = jour;
    }

    public String getDateDeubt() {
        return dateDeubt;
    }

    public void setDateDeubt(String dateDeubt) {
        this.dateDeubt = dateDeubt;
    }

    public String getDateFin() {
        return dateFin;
    }

    public void setDateFin(String dateFin) {
        this.dateFin = dateFin;
    }
}
