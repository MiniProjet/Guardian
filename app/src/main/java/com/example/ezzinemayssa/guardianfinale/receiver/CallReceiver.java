package com.example.ezzinemayssa.guardianfinale.receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.provider.ContactsContract;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.widget.Toast;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.toolbox.Volley;
import com.example.ezzinemayssa.guardianfinale.activity.RegisterActivity;
import com.example.ezzinemayssa.guardianfinale.entities.Call;
import com.example.ezzinemayssa.guardianfinale.entities.Parent;
import com.example.ezzinemayssa.guardianfinale.request.AddCallsByEmailChildRequest;
import com.example.ezzinemayssa.guardianfinale.request.SignupRequest;
import com.example.ezzinemayssa.guardianfinale.utils.MySingleton;
import com.google.firebase.auth.FirebaseAuth;

import org.json.JSONException;
import org.json.JSONObject;

import java.sql.Timestamp;
import java.util.Date;

import static com.example.ezzinemayssa.guardianfinale.activity.MainActivity.role;

/**
 * Created by Ezzine Mayssa on 14/11/2017.
 */

public class CallReceiver extends BroadcastReceiver {


    private static int lastState = TelephonyManager.CALL_STATE_IDLE;
    private static Timestamp callStartTime;
    private static boolean isIncoming;
    private static String savedNumber;
    String contactName;
    String stateCall;
    private FirebaseAuth firebaseAuth;

    @Override
    public void onReceive(Context context, Intent intent) {

        firebaseAuth = FirebaseAuth.getInstance();

        if (intent.getAction().equals("android.intent.action.NEW_OUTGOING_CALL")) {
            savedNumber = intent.getExtras().getString("android.intent.extra.PHONE_NUMBER");

        }
        else{
            String stateStr = intent.getExtras().getString(TelephonyManager.EXTRA_STATE);
            String number = intent.getExtras().getString(TelephonyManager.EXTRA_INCOMING_NUMBER);
            int state = 0;
            if(stateStr.equals(TelephonyManager.EXTRA_STATE_IDLE)){
                state = TelephonyManager.CALL_STATE_IDLE;
            }
            else if(stateStr.equals(TelephonyManager.EXTRA_STATE_OFFHOOK)){
                state = TelephonyManager.CALL_STATE_OFFHOOK;
            }
            else if(stateStr.equals(TelephonyManager.EXTRA_STATE_RINGING)){
                state = TelephonyManager.CALL_STATE_RINGING;
            }

            onCallStateChanged(context, state, number);
        }



        if(role.equals("CHILD"))
        {
           addCall(context);
        }

    }


    public void onCallStateChanged(Context context, int state, String number) {
        if(lastState == state){
            //No change, debounce extras
            return;
        }
        switch (state) {
            //Incoming call ringing
            case TelephonyManager.CALL_STATE_RINGING:
                isIncoming = true;
                callStartTime = new Timestamp(System.currentTimeMillis());
                savedNumber = number;


                break;
            case TelephonyManager.CALL_STATE_OFFHOOK:

                if(lastState != TelephonyManager.CALL_STATE_RINGING){
                    isIncoming = false;
                    callStartTime = new Timestamp(System.currentTimeMillis());

                }

                break;
            case TelephonyManager.CALL_STATE_IDLE:
                if(lastState == TelephonyManager.CALL_STATE_RINGING){
                    contactName = getContactName(savedNumber, context);
                    stateCall="MISSED";
                    Log.e("missed call: ",savedNumber + " Call time " + callStartTime +"Name: "+contactName);

                }
                else if(isIncoming){
                    contactName = getContactName(savedNumber, context);
                    Log.e("call from: ",savedNumber + " Call time " + callStartTime+"Name: "+contactName );
                    stateCall="INCOMING";
                }
                else{
                    contactName = getContactName(savedNumber, context);
                    Log.e("call to: ",savedNumber + " Call time " + callStartTime+"Name: "+contactName  );
                    stateCall="OUTGOING";
                }


                break;
        }
        lastState = state;
    }

    private String getContactName(String number, Context context) {

        String name = "Unknown";

        // define the columns I want the query to return
        String[] projection = new String[] {
                ContactsContract.PhoneLookup.DISPLAY_NAME,
                ContactsContract.PhoneLookup._ID};

        // encode the phone number and build the filter URI
        Uri contactUri = Uri.withAppendedPath(ContactsContract.PhoneLookup.CONTENT_FILTER_URI, Uri.encode(number));

        // query time
        Cursor cursor = context.getContentResolver().query(contactUri, projection, null, null, null);

        if(cursor != null) {
            if (cursor.moveToFirst()) {
                name =cursor.getString(cursor.getColumnIndex(ContactsContract.PhoneLookup.DISPLAY_NAME));
            }
            cursor.close();
        }
        return name;
    }

public void addCall(Context context)
{
    Response.Listener<String> responseListener = new Response.Listener<String>() {
        @Override
        public void onResponse(String response) {
            try {
                JSONObject jsonResponse = new JSONObject(response);
                String activated = jsonResponse.getString("addCalls");
                if (activated.equals("true")) {
                    Log.e("addCall", "add ");
                } else {
                    Log.e("addCall", "no ");
                }

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    };
    Call call = new Call();
    call.setNumber(savedNumber);
    call.setContact_name(contactName);
    call.setState(stateCall);
    call.setEmail_child(firebaseAuth.getCurrentUser().getEmail());
    call.setStart_time(callStartTime);
    AddCallsByEmailChildRequest register = new AddCallsByEmailChildRequest(call.getState(),call.getNumber(),call.getStart_time(),call.getContact_name(),call.getEmail_child(), responseListener);
    RequestQueue queue = MySingleton.getInstance(context).getRequestQueue();
// Add a request (in this example, called stringRequest) to your RequestQueue.
    MySingleton.getInstance(context).addToRequestQueue(register);
}
}