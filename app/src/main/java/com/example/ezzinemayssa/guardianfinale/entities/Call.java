package com.example.ezzinemayssa.guardianfinale.entities;

import java.sql.Timestamp;
import java.util.Date;

/**
 * Created by Ezzine Mayssa on 26/11/2017.
 */

public class Call {
    int id;
    String state;
    String number;
    Timestamp start_time;
    String contact_name;
    String email_child;

    public Call() {
    }

    public Call(String state, String number, Timestamp start_time, String contact_name, String email_child) {
        this.state = state;
        this.number = number;
        this.start_time = start_time;
        this.contact_name = contact_name;
        this.email_child = email_child;
    }

    public Timestamp getStart_time() {
        return start_time;
    }

    public void setStart_time(Timestamp start_time) {
        this.start_time = start_time;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }




    public String getContact_name() {
        return contact_name;
    }

    public void setContact_name(String contact_name) {
        this.contact_name = contact_name;
    }

    public String getEmail_child() {
        return email_child;
    }

    public void setEmail_child(String email_child) {
        this.email_child = email_child;
    }


}
