package com.example.ezzinemayssa.guardianfinale.request;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.toolbox.StringRequest;
import com.example.ezzinemayssa.guardianfinale.activity.MainActivity;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by mayss on 09/01/2018.
 */

public class RefreshTokkenRequest extends StringRequest {


    private static final String REGISTER_REQUEST_URL = "http://"+ MainActivity.ipAdress+"/guardenScript/refreshToken.php";
    private Map<String, String> params;

    public RefreshTokkenRequest(String email, String tokken, Response.Listener<String> listener) {
        super(Request.Method.POST, REGISTER_REQUEST_URL, listener, null);
        params = new HashMap<>();
        params.put("email_parent",email);
        params.put("tokken",tokken);
    }

    @Override
    public Map<String, String> getParams() {
        return params;
    }
}


