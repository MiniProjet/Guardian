package com.example.ezzinemayssa.guardianfinale.request;

import android.util.Log;

import com.android.volley.Response;
import com.android.volley.toolbox.StringRequest;

import java.util.HashMap;
import java.util.Map;

import static com.example.ezzinemayssa.guardianfinale.activity.MainActivity.ipAdress;

/**
 * Created by mayss on 21/11/2017.
 */

public class GetZoneRequest extends StringRequest {

    private static final String LOGIN_REQUEST_URL = "http://"+ipAdress+"/guardenScript/GetZone.php";
    private Map<String, String> params;

    public GetZoneRequest(String email, Response.Listener<String> listener) {
        super(Method.POST, LOGIN_REQUEST_URL, listener, null);
        params = new HashMap<>();
        Log.e("etape2","2");
        params.put("email", email);

    }

    @Override
    public Map<String, String> getParams() {
        return params;
    }
}
