package com.example.ezzinemayssa.guardianfinale.entities;

/**
 * Created by mayss on 24/11/2017.
 */

public class PickMeUp {
    String nameChild;
    String nameParent;
    String time;

    public PickMeUp() {
    }

    public PickMeUp(String nameChild, String nameParent, String time) {
        this.nameChild = nameChild;
        this.nameParent = nameParent;
        this.time = time;
    }

    public String getNameChild() {
        return nameChild;
    }

    public void setNameChild(String nameChild) {
        this.nameChild = nameChild;
    }

    public String getNameParent() {
        return nameParent;
    }

    public void setNameParent(String nameParent) {
        this.nameParent = nameParent;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    @Override
    public String toString() {
        return "PickMeUp{" +
                "nameChild='" + nameChild + '\'' +
                ", nameParent='" + nameParent + '\'' +
                ", time='" + time + '\'' +
                '}';
    }
}
