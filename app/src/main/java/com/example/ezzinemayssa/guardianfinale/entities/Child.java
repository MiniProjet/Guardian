package com.example.ezzinemayssa.guardianfinale.entities;

import android.view.View;

/**
 * Created by Ezzine Mayssa on 18/11/2017.
 */

public class Child {
    private String code;
    private String email;
    private String username;
    private String phone;
    private String image_name;
    private String email_parent;
    private String etat;
    private String birthdate;
    private String image_qr;

    private View.OnClickListener requestBtnClickListenerDelete;
    private View.OnClickListener requestBtnClickListenerEdit;
    private View.OnClickListener requestBtnClickListenerEnableForward;
    private View.OnClickListener requestBtnClickListenerDisableForward;
    public Child() {
    }

    public Child(String email, String username, String email_parent) {
        this.email = email;
        this.username = username;
        this.email_parent = email_parent;
    }

    public Child(String code, String email, String username, String phone, String image_name, String email_parent, String birthdate, String image_qr) {
        this.code = code;
        this.email = email;
        this.username = username;
        this.phone = phone;
        this.image_name = image_name;
        this.email_parent = email_parent;
        this.birthdate = birthdate;
        this.image_qr = image_qr;
    }

    public Child(String code, String email, String username, String phone, String image_name, String email_parent) {
        this.code = code;
        this.email = email;
        this.username = username;
        this.phone = phone;
        this.image_name = image_name;
        this.email_parent = email_parent;


    }

    public Child(String email, String etat) {
        this.email = email;
        this.etat = etat;
    }



    public View.OnClickListener getRequestBtnClickListenerDelete() {
        return requestBtnClickListenerDelete;
    }

    public void setRequestBtnClickListenerDelete(View.OnClickListener requestBtnClickListenerDelete) {
        this.requestBtnClickListenerDelete = requestBtnClickListenerDelete;
    }

    public View.OnClickListener getRequestBtnClickListenerEdit() {
        return requestBtnClickListenerEdit;
    }

    public void setRequestBtnClickListenerEdit(View.OnClickListener requestBtnClickListenerEdit) {
        this.requestBtnClickListenerEdit = requestBtnClickListenerEdit;
    }

    public View.OnClickListener getRequestBtnClickListenerEnableForward() {
        return requestBtnClickListenerEnableForward;
    }

    public void setRequestBtnClickListenerEnableForward(View.OnClickListener requestBtnClickListenerEnableForward) {
        this.requestBtnClickListenerEnableForward = requestBtnClickListenerEnableForward;
    }

    public View.OnClickListener getRequestBtnClickListenerDisableForward() {
        return requestBtnClickListenerDisableForward;
    }

    public void setRequestBtnClickListenerDisableForward(View.OnClickListener requestBtnClickListenerDisableForward) {
        this.requestBtnClickListenerDisableForward = requestBtnClickListenerDisableForward;
    }

    public String getImage_qr() {
        return image_qr;
    }

    public void setImage_qr(String image_qr) {
        this.image_qr = image_qr;
    }

    public String getBirthdate() {
        return birthdate;
    }

    public void setBirthdate(String birthdate) {
        this.birthdate = birthdate;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getImage_name() {
        return image_name;
    }

    public void setImage_name(String image_name) {
        this.image_name = image_name;
    }

    public String getEmail_parent() {
        return email_parent;
    }

    public void setEmail_parent(String email_parent) {
        this.email_parent = email_parent;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getEtat() {
        return etat;
    }

    public void setEtat(String etat) {
        this.etat = etat;
    }


    @Override
    public String toString() {
        return "Child{" +
                "code='" + code + '\'' +
                ", email='" + email + '\'' +
                ", username='" + username + '\'' +
                ", phone='" + phone + '\'' +
                ", image_name='" + image_name + '\'' +
                ", email_parent='" + email_parent + '\'' +
                ", etat='" + etat + '\'' +
                ", birthdate='" + birthdate + '\'' +
                ", image_qr='" + image_qr + '\'' +
                '}';
    }
}
