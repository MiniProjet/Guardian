package com.example.ezzinemayssa.guardianfinale.activity.map;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.PorterDuff;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;
import com.example.ezzinemayssa.guardianfinale.R;
import com.example.ezzinemayssa.guardianfinale.activity.MainActivity;

import com.example.ezzinemayssa.guardianfinale.adapter.RecyclerAdapterChooseChildren;
import com.example.ezzinemayssa.guardianfinale.fragment.EditPropChildFragment;
import com.example.ezzinemayssa.guardianfinale.request.AddZoneRequest;
import com.example.ezzinemayssa.guardianfinale.request.UpdateParent;
import com.example.ezzinemayssa.guardianfinale.utils.MySingleton;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;
import com.theartofdev.edmodo.cropper.CropImage;

import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;

import static com.example.ezzinemayssa.guardianfinale.activity.MainActivity.listParent;


public class DiagonalLayoutMainActivity extends AppCompatActivity {
    private FirebaseAuth firebaseAuth;

    ImageButton imageButton;
    TextView username;
    EditText password, passwordAgain, editName, editEmail, phoneNumber;
    Boolean toggle = false;
    ProgressBar progress;
    ImageView imgProfile;
    Bitmap bitmapProfil;
    int CODE_GALLERY_REQUEST = 203;
    Toolbar toolbar;
    Button btnUpdate;
    ProgressDialog progressDialog;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        progressDialog = new ProgressDialog(DiagonalLayoutMainActivity.this);
        progressDialog.setTitle("Please wait..");
        progressDialog.setMessage("Processing   ");
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.setIndeterminate(true);
        //setupFloatingLabelError();
        firebaseAuth = FirebaseAuth.getInstance();
        setContentView(R.layout.diagonallayout_activity_main);
        username=(TextView)findViewById(R.id.username_diagonal);
        imageButton = (ImageButton) findViewById(R.id.image);
        imgProfile = (ImageView) findViewById(R.id.imageprofile);
        password = (EditText) findViewById(R.id.password);
        passwordAgain = (EditText) findViewById(R.id.passwordAgain);
        editName = (EditText) findViewById(R.id.editName);
        editEmail = (EditText) findViewById(R.id.editEmail);
        phoneNumber = (EditText) findViewById(R.id.phoneNumber);

        btnUpdate = (Button) findViewById(R.id.btnUpdate);
        progress = findViewById(R.id.progressprofile);
        editEmail.setKeyListener(null);

        password.setText(listParent.get(0).getPassword());
        passwordAgain.setHint("Confirm your password");
        editName.setText(listParent.get(0).getUsername());
        editEmail.setText(listParent.get(0).getEmail());
        phoneNumber.setText(listParent.get(0).getPhone());
        Log.e("dd", listParent.get(0).getPicture());
        username.setText(listParent.get(0).getUsername().toUpperCase());

        passwordAgain.getBackground().mutate().setColorFilter(getResources().getColor(R.color.rose), PorterDuff.Mode.SRC_ATOP);
        password.getBackground().mutate().setColorFilter(getResources().getColor(R.color.rose), PorterDuff.Mode.SRC_ATOP);

        editName.getBackground().mutate().setColorFilter(getResources().getColor(R.color.rose), PorterDuff.Mode.SRC_ATOP);
        editEmail.getBackground().mutate().setColorFilter(getResources().getColor(R.color.rose), PorterDuff.Mode.SRC_ATOP);
        phoneNumber.getBackground().mutate().setColorFilter(getResources().getColor(R.color.rose), PorterDuff.Mode.SRC_ATOP);
        setImageProfile();
        imageButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (toggle == false) {
                    toggle = true;
                    password.setVisibility(View.VISIBLE);
                    passwordAgain.setVisibility(View.VISIBLE);
                    imageButton.setImageResource(R.drawable.ic_arrowup);

                } else {
                    toggle = false;
                    password.setVisibility(View.GONE);
                    passwordAgain.setVisibility(View.GONE);
                    imageButton.setImageResource(R.drawable.ic_arrowdown);


                }
            }
        });
        imgProfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                CropImage.activity()
                        .start(DiagonalLayoutMainActivity.this);
                Intent intent = new Intent(Intent.ACTION_PICK);
                intent.setType("image/*");
                startActivityForResult(Intent.createChooser(intent, "Select Image"), CODE_GALLERY_REQUEST);

            }
        });




        btnUpdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(phoneNumber.getText().length()<8) {
                    phoneNumber.setError("8 number required");
                }
                    if(editName.getText().length()==0){
                        editName.setError("username is empty");
                    }
                    if((phoneNumber.getText().length()==8)&&(editName.getText().length()!=0)){

                        AlertDialog.Builder builder = new AlertDialog.Builder(DiagonalLayoutMainActivity.this);
                        builder.setTitle("Confirm to update profile ");
                        builder.setMessage("You are about to update your profile . Do you really want to proceed ?");
                        builder.setCancelable(false);
                        builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                progressDialog.show();


                                Response.Listener<String> responseListener3 = new Response.Listener<String>() {
                                    @Override
                                    public void onResponse(String response) {

                                        progressDialog.dismiss();
                                        Log.d("TAG", response);


                                    }
                                };

                                Response.ErrorListener responseError = new Response.ErrorListener() {

                                    @Override
                                    public void onErrorResponse(VolleyError error) {
                                        progressDialog.dismiss();
                                    }
                                };


                                FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();


                                user.updatePassword(password.getText().toString())
                                        .addOnCompleteListener(new OnCompleteListener<Void>() {
                                            @Override
                                            public void onComplete(@NonNull Task<Void> task) {
                                                if (task.isSuccessful()) {
                                                    Log.d("TAG", "User password updated.");
                                                }
                                            }
                                        });

                                UpdateParent addlocation = new UpdateParent(firebaseAuth.getCurrentUser().getEmail(), editName.getText().toString(), phoneNumber.getText().toString()
                                        , getStringImage(((BitmapDrawable) imgProfile.getDrawable()).getBitmap()), password.getText().toString(), responseListener3,responseError);

                                RequestQueue queue = MySingleton.getInstance(DiagonalLayoutMainActivity.this).getRequestQueue();
// Add a request (in this example, called stringRequest) to your RequestQueue.
                                MySingleton.getInstance(DiagonalLayoutMainActivity.this).addToRequestQueue(addlocation);



                            }
                        });

                        builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.cancel();
                            }
                        });

                        builder.show();



                    }







            }
        });


    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE && resultCode == RESULT_OK && data != null) {
            CropImage.ActivityResult result = CropImage.getActivityResult(data);
            Uri filePath = result.getUri();
            Context applicationContext = MainActivity.getContextOfApplication();
            try {
                InputStream inputStream = applicationContext.getContentResolver().openInputStream(filePath);
                bitmapProfil = BitmapFactory.decodeStream(inputStream);

                imgProfile.setImageBitmap(bitmapProfil);

            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
        }
        super.onActivityResult(requestCode, resultCode, data);
    }


    public String getStringImage(Bitmap bmp) {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bmp.compress(Bitmap.CompressFormat.JPEG, 100, baos);
        byte[] imageBytes = baos.toByteArray();
        String encodedImage = Base64.encodeToString(imageBytes, Base64.DEFAULT);
        return encodedImage;
    }


    @Override
    protected void onResume() {
        super.onResume();


    }


    public void setImageProfile() {
        if (listParent.get(0).getPicture().equals("null")) {

            Picasso.with(this).load("http://192.168.1.11/guardenScript/images/mystery_man.png").into(imgProfile, new Callback() {
                @Override
                public void onSuccess() {
                    progress.setVisibility(View.GONE);
                    imgProfile.setVisibility(View.VISIBLE);

                }

                @Override
                public void onError() {

                }
            });
        } else {
            Log.e("dd2", listParent.get(0).getPicture());

            Picasso.with(this).load(listParent.get(0).getPicture()).into(imgProfile, new Callback() {
                @Override
                public void onSuccess() {
                    progress.setVisibility(View.GONE);
                    imgProfile.setVisibility(View.VISIBLE);
                }

                @Override
                public void onError() {

                }
            });
        }
    }


    private void setupFloatingLabelError() {
        final TextInputLayout floatingUsernameLabel = (TextInputLayout) findViewById(R.id.username_text_input_layout);
        floatingUsernameLabel.getEditText().addTextChangedListener(new TextWatcher() {
            // ...
            @Override
            public void onTextChanged(CharSequence text, int start, int count, int after) {
                if (text.length() > 0 && text.length() <= 4) {
                    floatingUsernameLabel.setError("R.string.username_required");
                    floatingUsernameLabel.setErrorEnabled(true);
                } else {
                    floatingUsernameLabel.setErrorEnabled(false);
                }
            }
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) {
                // TODO Auto-generated method stub
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }
}
