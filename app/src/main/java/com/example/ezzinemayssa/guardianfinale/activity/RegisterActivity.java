package com.example.ezzinemayssa.guardianfinale.activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;
import com.example.ezzinemayssa.guardianfinale.R;

import com.example.ezzinemayssa.guardianfinale.entities.Parent;
import com.example.ezzinemayssa.guardianfinale.request.SignupRequest;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;

import org.json.JSONException;
import org.json.JSONObject;

public class RegisterActivity extends AppCompatActivity {
    private FirebaseAuth firebaseAuth;
    TextView tvGuardian;
    EditText emailSignup;
    EditText passwordSignup;
    EditText mobileSignup;
    Button btnSignup;
    TextView tvSignin;

    ProgressDialog progress;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        //Declaration
        tvGuardian =  findViewById(R.id.tvGuardianSignup);
        emailSignup =  findViewById(R.id.emailSignup);
        passwordSignup =  findViewById(R.id.passwordSignup);
        mobileSignup =  findViewById(R.id.mobileSignup);
        tvSignin =  findViewById(R.id.tvSignin);
        btnSignup =  findViewById(R.id.btnSignup);
        firebaseAuth = FirebaseAuth.getInstance();

        //Set font for title
        Typeface type = Typeface.createFromAsset(getAssets(), "fonts/Dopestyle.ttf");
        tvGuardian.setTypeface(type);


        progress = new ProgressDialog(this);
        progress.setTitle("Please wait..");
        progress.setMessage("Processing   ");
        progress.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progress.setIndeterminate(true);



        emailSignup.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                if (emailSignup.getText().length() == 0) {
                    emailSignup.setError("Email is empty");
                }

            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        passwordSignup.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                if (passwordSignup.getText().length() == 0) {
                    passwordSignup.setError("Password is empty");
                }
                if (passwordSignup.getText().length() < 6) {
                    passwordSignup.setError("6 characters or more");
                }

            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        mobileSignup.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                if (mobileSignup.getText().length() != 8) {
                    mobileSignup.setError("8 numbers required");
                }


            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });





        tvSignin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(RegisterActivity.this,LoginActivity.class);
                startActivity(intent);
            }
        });
        btnSignup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if ((emailSignup.getText().length() != 0) && (passwordSignup.getText().length() != 0) && (passwordSignup.getText().length() >= 6)&& (mobileSignup.getText().length() ==8)) {
                    signupFirebase();
                }
            }
        });

    }



    void signupFirebase()
    {
        progress.show();
        (firebaseAuth.createUserWithEmailAndPassword(emailSignup.getText().toString(), passwordSignup.getText().toString()))
                .addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        progress.dismiss();
                        if (task.isSuccessful()) {
                            signupMySQL();
                            Intent intent = new Intent(RegisterActivity.this, LoginActivity.class);
                            startActivity(intent);
                        } else {

                            progress.setTitle("Error");
                            progress.setMessage(task.getException().getMessage());
                            progress.setProgressStyle(ProgressDialog.STYLE_SPINNER);
                            progress.setIndeterminate(true);
                            progress.setCanceledOnTouchOutside(true);                        }
                    }
                });
    }
    void signupMySQL()
    {


        Response.Listener<String> responseListener = new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    JSONObject jsonResponse = new JSONObject(response);
                    String activated = jsonResponse.getString("signup");
                    if (activated.equals("true")) {

                        progress.dismiss();

                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }


            }
        };


        Response.ErrorListener responseListener5 = new Response.ErrorListener() {


            @Override
            public void onErrorResponse(VolleyError error) {
                progress.setTitle("Error..");
                progress.setMessage("An error has occurred when sign up ");
                progress.setProgressStyle(ProgressDialog.STYLE_SPINNER);
                progress.setIndeterminate(true);
                progress.setCanceledOnTouchOutside(true);


            }
        };
        Parent parent = new Parent();
        parent.setEmail(emailSignup.getText().toString());
        parent.setPassword(passwordSignup.getText().toString());
        parent.setPhone(mobileSignup.getText().toString());

        SignupRequest register = new SignupRequest(parent.getEmail(), parent.getPassword(),parent.getPhone(), responseListener,responseListener5);
        RequestQueue queue = Volley.newRequestQueue(RegisterActivity.this);
        queue.add(register);

    }
}
