package com.example.ezzinemayssa.guardianfinale.fragment;


import android.app.AlertDialog;
import android.app.Fragment;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;

import android.renderscript.Sampler;
import android.support.annotation.NonNull;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;
import com.example.ezzinemayssa.guardianfinale.R;
import com.example.ezzinemayssa.guardianfinale.activity.map.CreationZoneMap;
import com.example.ezzinemayssa.guardianfinale.activity.map.DiagonalLayoutMainActivity;
import com.example.ezzinemayssa.guardianfinale.adapter.ListViewZoneAdapter;
import com.example.ezzinemayssa.guardianfinale.entities.ZoneChild;
import com.example.ezzinemayssa.guardianfinale.request.DeleteChildRequest;
import com.example.ezzinemayssa.guardianfinale.request.DeleteZoneRequest;
import com.example.ezzinemayssa.guardianfinale.request.GetZoneRequest;
import com.example.ezzinemayssa.guardianfinale.request.UpdateParent;
import com.example.ezzinemayssa.guardianfinale.utils.MySingleton;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;


import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import static com.example.ezzinemayssa.guardianfinale.adapter.RecyclerAdapterDisplayChildren.emailChildSelected;

/**
 * A simple {@link Fragment} subclass.
 */
public class ListOfZoneByChild extends Fragment {
List<ZoneChild>zoneChildList;
    ListViewZoneAdapter adapter;
    ListView list ;
    public ListOfZoneByChild() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view =inflater.inflate(R.layout.fragment_list_of_zone_by_child, container, false);
zoneChildList=new ArrayList<>();
        list=(ListView)view.findViewById(R.id.listZone);
        Response.Listener<String> responseListener = new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {


                try {
                    JSONObject jsonResponse = new JSONObject(response);

                    JSONArray jsonArray= jsonResponse.getJSONArray("zone");
                    int count= 0;
                    Log.e("lool",response);
                    while(count<jsonArray.length()) {
                        JSONObject jo = jsonArray.getJSONObject(count);
                        zoneChildList.add(new ZoneChild("halimjerbi.com", jo.getString("lat00"), jo.getString("lng00"),
                                jo.getString("lat01"), jo.getString("lng01"), jo.getString("lat10"), jo.getString("lng10"),
                                jo.getString("lat11"), jo.getString("lng11"), jo.getString("jour"),
                                jo.getString("mardi"),jo.getString("mercredi"),
                                jo.getString("jeudi"),jo.getString("vendredi"),jo.getString("samedi"),
                                jo.getString("dimanche"),jo.getString("datedebut")
                                , jo.getString("datefin")));
                        count++;


                    }


                    adapter =new ListViewZoneAdapter(getActivity(),
                            R.layout.item_zone, zoneChildList);
                    list.setAdapter(adapter);





                } catch (JSONException e) {
                    e.printStackTrace();
                }


            }
        };
        GetZoneRequest addlocation = new GetZoneRequest(emailChildSelected,responseListener);
        RequestQueue queue = Volley.newRequestQueue(getActivity());
        queue.add(addlocation);
        Log.e("d5al","d5al");
        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
//                TextView txtDateDebut=(TextView)view.findViewById(R.id.txtDateDebut);
//                TextView txtDateFin=(TextView)view.findViewById(R.id.txtDateFin);
//                TextView txtLundi=(TextView)view.findViewById(R.id.txtlundi);
//                TextView txtMardi=(TextView)view.findViewById(R.id.txtMardi);
//                TextView txtMercredi=(TextView)view.findViewById(R.id.txtMercredi);
//                TextView txtJeudi=(TextView)view.findViewById(R.id.txtJeudi);
//                TextView txtVendredi=(TextView)view.findViewById(R.id.txtVendredi);
//                TextView txtSamedi=(TextView)view.findViewById(R.id.txtSamedi);
//                TextView txtDimanche=(TextView)view.findViewById(R.id.txtDiamanche);
//                Log.e("TAG",txtDateDebut.getText().toString());
//
//                Bundle bundle =new Bundle();
//                bundle.putString("DateDebut",txtDateDebut.getText().toString());
//                bundle.putString("DateFin",txtDateFin.getText().toString());
//
//                bundle.putString("lat00",zoneChildList.get(i).getLat00());
//                bundle.putString("lat01",zoneChildList.get(i).getLat01());
//                bundle.putString("lat10",zoneChildList.get(i).getLat10());
//                bundle.putString("lat11",zoneChildList.get(i).getLat11());
//
//                bundle.putString("lng00",zoneChildList.get(i).getLng00());
//                bundle.putString("lng01",zoneChildList.get(i).getLat01());
//                bundle.putString("lng10",zoneChildList.get(i).getLng10());
//                bundle.putString("lng11",zoneChildList.get(i).getLng11());
//                bundle.putString("jour",zoneChildList.get(i).getJour());
//                bundle.putString("mardi",zoneChildList.get(i).getMardi());
//                bundle.putString("mercredi",zoneChildList.get(i).getMercredi());
//                bundle.putString("jeudi",zoneChildList.get(i).getJeudi());
//                bundle.putString("vendredi",zoneChildList.get(i).getVendredi());
//                bundle.putString("samedi",zoneChildList.get(i).getSamedi());
//                bundle.putString("dimanche",zoneChildList.get(i).getDimanche());
//
//
//                bundle.putString("state","update");
//                Intent intent = new Intent(getActivity(), CreationZoneMap.class);
//                intent.putExtra("bundle",bundle);
//                startActivity(intent);
            }
        });

        list.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> adapterView, final View view, final int i, long l) {


                AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                builder.setTitle("Confirm to update profile ");
                builder.setMessage("You are about to update your profile . Do you really want to proceed ?");
                builder.setCancelable(false);
                builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {


                        TextView txtDateDebut=(TextView)view.findViewById(R.id.txtDateDebut);
                        TextView txtDateFin=(TextView)view.findViewById(R.id.txtDateFin);
                        Log.e("DateDebut",txtDateDebut.getText().toString());
                        Log.e("DateFin",txtDateFin.getText().toString());
                        Log.e("lat00",zoneChildList.get(i).getLat00());
                        Response.Listener<String> responseListener = new Response.Listener<String>() {
                            @Override
                            public void onResponse(String response) {
                            adapter.notifyDataSetChanged();                            }
                        };
                        DeleteZoneRequest deleteChildRequest = new DeleteZoneRequest(zoneChildList.get(i).getLat00(),txtDateDebut.getText().toString(),txtDateFin.getText().toString(),responseListener);
                        MySingleton.getInstance(getActivity()).addToRequestQueue(deleteChildRequest);


                    }
                });

                builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });

                builder.show();


                return false;
            }
        });
    }
}
