package com.example.ezzinemayssa.guardianfinale.activity.map;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.widget.ImageView;

import com.example.ezzinemayssa.guardianfinale.R;
import com.squareup.picasso.Picasso;

import static com.example.ezzinemayssa.guardianfinale.adapter.RecyclerAdapterDisplayChildren.pathQR;

public class QRActivity extends AppCompatActivity {

    ImageView fullScreenQR;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_qr);

        fullScreenQR= findViewById(R.id.IVQRFullScreen);
        Picasso.with(this).load(pathQR).into(fullScreenQR);


    }
}
