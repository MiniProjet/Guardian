package com.example.ezzinemayssa.guardianfinale.adapter;

/**
 * Created by Shade on 5/9/2016.
 */

import android.app.Activity;
import android.app.Dialog;
import android.app.DialogFragment;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Context;
import android.support.design.widget.Snackbar;
import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import com.example.ezzinemayssa.guardianfinale.R;
import com.example.ezzinemayssa.guardianfinale.entities.Child;
import com.example.ezzinemayssa.guardianfinale.fragment.AddChildFragment;
import com.example.ezzinemayssa.guardianfinale.fragment.ChooseChildFragment;
import com.example.ezzinemayssa.guardianfinale.fragment.HomeFragment;
import com.squareup.picasso.Callback;
import com.squareup.picasso.MemoryPolicy;
import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;
import java.util.ArrayList;
import java.util.List;


public class RecyclerAdapterChooseChildren extends RecyclerView.Adapter<RecyclerAdapterChooseChildren.ViewHolder> {
    public static String email_child_chosen;
    private Child current;
    private  Context context;
    private FragmentActivity context2;
    List<Child> listChooseChildren = new ArrayList<>();
    private LayoutInflater inflater;
    Dialog dialog;

    // create constructor to innitilize context and data sent from MainActivity
    public RecyclerAdapterChooseChildren(Context context, List<Child> listChooseChildren,Dialog dialog){
        this.context=context;
        this.dialog=dialog;
        this.listChooseChildren=listChooseChildren;
        inflater= LayoutInflater.from(context);


    }

    class ViewHolder extends RecyclerView.ViewHolder{

        private ImageView itemImage;
        private TextView itemUsername;
        private ImageView itemStatus;
        ProgressBar progressBarImageProfile;


        private ViewHolder(View itemView) {
            super(itemView);
            itemImage =itemView.findViewById(R.id.item_image_chooseChild);
            itemUsername = itemView.findViewById(R.id.usernameChooseChild);
            itemStatus = itemView.findViewById(R.id.statusChooseChild);
            progressBarImageProfile = itemView.findViewById(R.id.progressChooseChildren);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int position = getAdapterPosition();

                    email_child_chosen=listChooseChildren.get(position).getEmail();
                    Snackbar.make(v, email_child_chosen,
                            Snackbar.LENGTH_LONG)
                            .setAction("Action", null).show();

                    dialog.dismiss();



                }
            });
        }
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_choose_child, viewGroup, false);
        ViewHolder viewHolder = new ViewHolder(v);
        return viewHolder;


    }


    @Override
    public void onBindViewHolder(ViewHolder viewHolder, int i ) {
        final ViewHolder myHolder= (ViewHolder) viewHolder;
        current=listChooseChildren.get(i);
        myHolder.itemUsername.setText(current.getUsername());
        Log.e("chooseChildren2",listChooseChildren.toString());

        Picasso.with(context).load(current.getImage_name()).networkPolicy(NetworkPolicy.NO_CACHE).into(myHolder.itemImage, new Callback() {
            @Override
            public void onSuccess() {
                myHolder.progressBarImageProfile.setVisibility(View.GONE);
                myHolder.itemImage.setVisibility(View.VISIBLE);
                myHolder.itemStatus.setVisibility(View.VISIBLE);

            }

            @Override
            public void onError() {

            }
        });


    }


    @Override
    public int getItemCount() {
        return listChooseChildren.size();
    }
}