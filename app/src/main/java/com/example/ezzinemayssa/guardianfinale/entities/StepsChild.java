package com.example.ezzinemayssa.guardianfinale.entities;

/**
 * Created by mayss on 04/12/2017.
 */

public class StepsChild {
    public int Hours0;
    public int Hours1;
    public int Hours2;
    public int Hours3;
    public int Hours4;
    public int Hours5;
    public int Hours6;
    public int Hours7;
    public int Hours8;
    public int Hours9;
    public int Hours10;
    public int Hours11;
    public int Hours12;
    public int Hours13;
    public int Hours14;
    public int Hours15;
    public int Hours16;
    public int Hours17;
    public int Hours18;
    public int Hours19;
    public int Hours20;
    public int Hours21;
    public int Hours22;
    public int Hours23;
    public String emailChild;



    public void setEmailChild(String emailChild) {
        this.emailChild = emailChild;
    }

    public StepsChild(String emailChild, int hours0, int hours1, int hours2, int hours3, int hours4, int hours5, int hours6, int hours7, int hours8, int hours9, int hours10, int hours11, int hours12, int hours13, int hours14, int hours15, int hours16, int hours17, int hours18, int hours19, int hours20, int hours21, int hours22) {
        Hours0 = hours0;
        Hours1 = hours1;
        Hours2 = hours2;
        Hours3 = hours3;
        Hours4 = hours4;
        Hours5 = hours5;
        Hours6 = hours6;
        Hours7 = hours7;
        Hours8 = hours8;
        Hours9 = hours9;
        Hours10 = hours10;
        Hours11 = hours11;
        Hours12 = hours12;
        Hours13 = hours13;
        Hours14 = hours14;
        Hours15 = hours15;
        Hours16 = hours16;
        Hours17 = hours17;
        Hours18 = hours18;
        Hours19 = hours19;
        Hours20 = hours20;
        Hours21 = hours21;
        Hours22 = hours22;

        this.emailChild = emailChild;
    }

    public StepsChild() {
    }

    @Override
    public String toString() {
        return "StepsChild{" +
                "Hours0=" + Hours0 +
                ", Hours1=" + Hours1 +
                ", Hours2=" + Hours2 +
                ", Hours3=" + Hours3 +
                ", Hours4=" + Hours4 +
                ", Hours5=" + Hours5 +
                ", Hours6=" + Hours6 +
                ", Hours7=" + Hours7 +
                ", Hours8=" + Hours8 +
                ", Hours9=" + Hours9 +
                ", Hours10=" + Hours10 +
                ", Hours11=" + Hours11 +
                ", Hours12=" + Hours12 +
                ", Hours13=" + Hours13 +
                ", Hours14=" + Hours14 +
                ", Hours15=" + Hours15 +
                ", Hours16=" + Hours16 +
                ", Hours17=" + Hours17 +
                ", Hours18=" + Hours18 +
                ", Hours19=" + Hours19 +
                ", Hours20=" + Hours20 +
                ", Hours21=" + Hours21 +
                ", Hours22=" + Hours22 +
                ", Hours23=" + Hours23 +
                ", emailChild='" + emailChild + '\'' +
                '}';
    }
}
