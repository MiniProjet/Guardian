package com.example.ezzinemayssa.guardianfinale.activity;


import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Point;
import android.graphics.Typeface;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.view.Display;

import com.codemybrainsout.onboarder.AhoyOnboarderActivity;
import com.codemybrainsout.onboarder.AhoyOnboarderCard;
import com.example.ezzinemayssa.guardianfinale.R;

import java.util.ArrayList;
import java.util.List;

public class SettingDisableUninstallActivity extends AhoyOnboarderActivity {

    public static String PREFERENCE_FILENAME= "reporting_app";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.setColorBackground(R.color.white);
        AhoyOnboarderCard ahoyOnboarderCard1 = new AhoyOnboarderCard("", "Click manage device administrators", R.drawable.settings1);
        AhoyOnboarderCard ahoyOnboarderCard2 = new AhoyOnboarderCard("", "Uncheck Guardian application", R.drawable.settings1);
        AhoyOnboarderCard ahoyOnboarderCard3 = new AhoyOnboarderCard("", "The screen will get locked, to unlock it type in the password 1234", R.drawable.settings1);
        AhoyOnboarderCard ahoyOnboarderCard4 = new AhoyOnboarderCard("", "Click the home button, then click on dialog", R.drawable.settings1);
        AhoyOnboarderCard ahoyOnboarderCard5 = new AhoyOnboarderCard("", "Click Ok.", R.drawable.settings1);
        AhoyOnboarderCard ahoyOnboarderCard6 = new AhoyOnboarderCard("", "Congratulation, now you can uninstall the application.", R.drawable.settings1);

        Display display = getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        int width = size.x;
        int height = size.y;

        ahoyOnboarderCard1.setIconLayoutParams(width-(width/2),height-(height/2),0,0,0,0);
        ahoyOnboarderCard2.setIconLayoutParams(width-(width/2),height-(height/2),0,0,0,0);
        ahoyOnboarderCard3.setIconLayoutParams(width-(width/2),height-(height/2),0,0,0,0);
        ahoyOnboarderCard4.setIconLayoutParams(width-(width/2),height-(height/2),0,0,0,0);
        ahoyOnboarderCard5.setIconLayoutParams(width-(width/2),height-(height/2),0,0,0,0);
        ahoyOnboarderCard6.setIconLayoutParams(width-(width/2),height-(height/2),0,0,0,0);

        ahoyOnboarderCard1.setBackgroundColor(R.color.black_transparent);
        ahoyOnboarderCard2.setBackgroundColor(R.color.black_transparent);
        ahoyOnboarderCard3.setBackgroundColor(R.color.black_transparent);
        ahoyOnboarderCard4.setBackgroundColor(R.color.black_transparent);
        ahoyOnboarderCard5.setBackgroundColor(R.color.black_transparent);
        ahoyOnboarderCard6.setBackgroundColor(R.color.black_transparent);

        List<AhoyOnboarderCard> pages = new ArrayList<>();

        pages.add(ahoyOnboarderCard1);
        pages.add(ahoyOnboarderCard2);
        pages.add(ahoyOnboarderCard3);
        pages.add(ahoyOnboarderCard4);
        pages.add(ahoyOnboarderCard5);
        pages.add(ahoyOnboarderCard6);

        for (AhoyOnboarderCard page : pages) {

            page.setDescriptionColor(R.color.black);
            //page.setTitleTextSize(dpToPixels(12, this));
            //page.setDescriptionTextSize(dpToPixels(8, this));
            //page.setIconLayoutParams(width, height, marginTop, marginLeft, marginRight, marginBottom);
        }

        setFinishButtonTitle("Got it");
        showNavigationControls(true);
        setGradientBackground();

        //set the button style you created
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
            setFinishButtonDrawableStyle(ContextCompat.getDrawable(this, R.drawable.rounded_button));
        }

        Typeface face = Typeface.createFromAsset(getAssets(), "fonts/Roboto-Light.ttf");
        setFont(face);

        setOnboardPages(pages);
    }

    @Override
    public void onFinishButtonPressed() {
//        SharedPreferences reportingPref= getSharedPreferences(PREFERENCE_FILENAME,MODE_PRIVATE);
//        SharedPreferences.Editor prefEditor = reportingPref.edit();
//
//        prefEditor.putString("onBoarder","true");
//        prefEditor.commit();
//
//        Intent intent = new Intent(OnboarderActivity.this,LoginActivity.class);
//        startActivity(intent);


    }
}
