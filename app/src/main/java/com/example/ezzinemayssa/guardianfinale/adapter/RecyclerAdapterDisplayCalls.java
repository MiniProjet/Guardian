package com.example.ezzinemayssa.guardianfinale.adapter;

/**
 * Created by Shade on 5/9/2016.
 */

import android.content.Context;
import android.graphics.Color;
import android.support.design.widget.Snackbar;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.toolbox.ImageLoader;
import com.example.ezzinemayssa.guardianfinale.R;
import com.example.ezzinemayssa.guardianfinale.entities.Call;
import com.google.firebase.auth.FirebaseAuth;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;


public class RecyclerAdapterDisplayCalls extends RecyclerView.Adapter<RecyclerAdapterDisplayCalls.ViewHolder> {

    private Call current;
    private  Context context;
    List<Call> listCalls = new ArrayList<>();
    private FirebaseAuth firebaseAuth;
    private LayoutInflater inflater;
    private ImageLoader imageLoaderProfile;
    private ImageLoader imageLoaderQrCode;



    // create constructor to innitilize context and data sent from MainActivity
    public RecyclerAdapterDisplayCalls(Context context, List<Call> listCalls){
        this.context=context;
        this.listCalls=listCalls;
        inflater= LayoutInflater.from(context);
    }

    class ViewHolder extends RecyclerView.ViewHolder{

        private ImageView itemImageAvatar;
        private TextView itemContact_name_calls;
        private TextView itemDateTime_Calls;
        private TextView itemContactNumber_Calls;
        private ImageView item_iv_state_calls;



        private ViewHolder(View itemView) {
            super(itemView);
            itemImageAvatar =itemView.findViewById(R.id.anonymos_image_calls);
            itemContact_name_calls = itemView.findViewById(R.id.contact_name_calls);
            itemDateTime_Calls = itemView.findViewById(R.id.dateTime_Calls);
            itemContactNumber_Calls = itemView.findViewById(R.id.contactNumber_Calls);
            item_iv_state_calls = itemView.findViewById(R.id.iv_state_calls);


            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int position = getAdapterPosition();

                    Snackbar.make(v, "Click detected on item " + position,
                            Snackbar.LENGTH_LONG)
                            .setAction("Action", null).show();

                }
            });
        }
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_call, viewGroup, false);
        ViewHolder viewHolder = new ViewHolder(v);
        return viewHolder;


    }


    @Override
    public void onBindViewHolder(ViewHolder viewHolder, int i ) {
        ViewHolder myHolder= (ViewHolder) viewHolder;
//        firebaseAuth = FirebaseAuth.getInstance();
           current=listCalls.get(i);
        Log.e("listHolder",listCalls.toString());

        myHolder.itemContact_name_calls.setText(current.getContact_name());
        myHolder.itemContactNumber_Calls.setText(current.getNumber());
        Timestamp stamp=current.getStart_time();
        Date date = new Date(stamp.getTime());
        SimpleDateFormat sdf = new SimpleDateFormat("h:mm");
        sdf.setTimeZone(TimeZone.getTimeZone("GMT"));
        String formattedDate = sdf.format(date);
        myHolder.itemDateTime_Calls.setText(formattedDate);

        switch(current.getState())
        {
            case "MISSED":
                myHolder.item_iv_state_calls.setImageResource(R.drawable.ic_phone_missed);
                myHolder.itemImageAvatar.setImageResource(R.drawable.avatar_missed);
                myHolder.item_iv_state_calls.setColorFilter(Color.argb(255, 237, 113, 97));
                break;
            case "INCOMING":
                myHolder.item_iv_state_calls.setImageResource(R.drawable.ic_call_incoming);
                myHolder.item_iv_state_calls.setColorFilter(Color.argb(255, 72,160,120));
                myHolder.itemImageAvatar.setImageResource(R.drawable.avatar_incoming);


                break;
            case "OUTGOING":
                myHolder.item_iv_state_calls.setImageResource(R.drawable.ic_phone_outgoing);
                myHolder.item_iv_state_calls.setColorFilter(Color.argb(255,33,174,94));
                myHolder.itemImageAvatar.setImageResource(R.drawable.avatar_outgoing);


                break;

        }


    }


    @Override
    public int getItemCount() {
        return listCalls.size();
    }
}