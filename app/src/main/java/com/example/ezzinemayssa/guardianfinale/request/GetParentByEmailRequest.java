package com.example.ezzinemayssa.guardianfinale.request;

import com.android.volley.Response;
import com.android.volley.toolbox.StringRequest;

import java.util.HashMap;
import java.util.Map;

import static com.example.ezzinemayssa.guardianfinale.activity.MainActivity.ipAdress;

/**
 * Created by Ezzine Mayssa on 19/11/2017.
 */

public class GetParentByEmailRequest extends StringRequest {


    private static final String REGISTER_REQUEST_URL = "http://"+ipAdress+"/guardenScript/getParentByEmail.php";
    private Map<String, String> params;

    public GetParentByEmailRequest(String email, Response.Listener<String> listener) {
       super(Method.POST, REGISTER_REQUEST_URL,listener,null);
        params = new HashMap<>();
        params.put("email",email);

    }

    @Override
    public Map<String, String> getParams() {
        return params;
    }
}
