package com.example.ezzinemayssa.guardianfinale.request;

import com.android.volley.Response;
import com.android.volley.toolbox.StringRequest;

import java.util.HashMap;
import java.util.Map;

import static com.example.ezzinemayssa.guardianfinale.activity.MainActivity.ipAdress;

/**
 * Created by Ezzine Mayssa on 19/11/2017.
 */

public class GetCallsByEmailChildRequest extends StringRequest {


    private static final String REGISTER_REQUEST_URL = "http://"+ipAdress+"/guardenScript/getCallsByEmailChild.php";
    private Map<String, String> params;

    public GetCallsByEmailChildRequest(String email_child,String day,String month, Response.Listener<String> listener) {
       super(Method.POST, REGISTER_REQUEST_URL,listener,null);
        params = new HashMap<>();
        params.put("email_child",email_child);
        params.put("day",day);
        params.put("month",month);


    }

    @Override
    public Map<String, String> getParams() {
        return params;
    }
}
