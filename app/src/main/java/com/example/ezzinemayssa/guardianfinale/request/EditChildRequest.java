package com.example.ezzinemayssa.guardianfinale.request;

import android.util.Log;

import com.android.volley.Response;
import com.android.volley.toolbox.StringRequest;

import java.util.HashMap;
import java.util.Map;

import static com.example.ezzinemayssa.guardianfinale.activity.MainActivity.ipAdress;

/**
 * Created by Ezzine Mayssa on 13/11/2017.
 */

public class EditChildRequest extends StringRequest {


        private static final String REGISTER_REQUEST_URL = "http://"+ipAdress+"/guardenScript/editChildByEmailParent.php";
        private Map<String, String> params;

        public EditChildRequest(String image_name,  String code,  String username, String phone,
                                String email_parent , String birthdate , Response.Listener<String> listener,Response.ErrorListener errorListener) {
            super(Method.POST, REGISTER_REQUEST_URL, listener, errorListener);
            params = new HashMap<>();


            params.put("image_name",image_name);
            params.put("code",code);
            params.put("username", username);
            params.put("phone", phone);
            params.put("email_parent", email_parent);
            params.put("birthdate",birthdate);


        }

        @Override
        public Map<String, String> getParams() {
            return params;
        }
    }

