package com.example.ezzinemayssa.guardianfinale.receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.location.LocationManager;
import android.support.v7.app.AlertDialog;
import android.widget.Toast;

import com.example.ezzinemayssa.guardianfinale.activity.MainActivity;

import static android.content.Context.MODE_PRIVATE;
import static com.example.ezzinemayssa.guardianfinale.activity.MainActivity.role;

/**
 * Created by zasus on 13/01/2018.
 */

public class GpsReciever extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {

        SharedPreferences prefs = context.getSharedPreferences("USER", MODE_PRIVATE);
        String restoredText = prefs.getString("regId", null);
        if (restoredText != null) {
            String role = prefs.getString("role", "No name defined");//"No name defined" is the default value.
            if(role.equals("CHILD")){
                if (intent.getAction().matches("android.location.PROVIDERS_CHANGED")) {
                    final LocationManager manager = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);

                    if (!manager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
                        Intent i=new Intent(context.getApplicationContext(),MainActivity.class);
                        i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        context.startActivity(i);
                    }


                }

            }

        }



           }
    }

