package com.example.ezzinemayssa.guardianfinale.request;

import com.android.volley.Response;
import com.android.volley.toolbox.StringRequest;
import com.example.ezzinemayssa.guardianfinale.activity.MainActivity;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by mayss on 04/01/2018.
 */

public class DeleteZoneRequest extends StringRequest {


    private static final String REGISTER_REQUEST_URL = "http://"+ MainActivity.ipAdress+"/guardenScript/DeleteZone.php";
    private Map<String, String> params;

    public DeleteZoneRequest(String lat00,String datedebut,String datefin, Response.Listener<String> listener) {
        super(Method.POST, REGISTER_REQUEST_URL, listener, null);
        params = new HashMap<>();
        params.put("lat00",lat00);
        params.put("datedebut",datedebut);
        params.put("datefin",datefin);


    }

    @Override
    public Map<String, String> getParams() {
        return params;
    }
}



