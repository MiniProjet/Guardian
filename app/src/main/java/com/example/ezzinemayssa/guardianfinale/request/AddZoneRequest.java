package com.example.ezzinemayssa.guardianfinale.request;

import android.util.Log;

import com.android.volley.Response;
import com.android.volley.toolbox.StringRequest;
import com.example.ezzinemayssa.guardianfinale.activity.MainActivity;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by mayss on 21/11/2017.
 */

public class AddZoneRequest extends StringRequest {

    private static final String REGISTER_REQUEST_URL = "http://"+ MainActivity.ipAdress+"/guardenScript/AddZone.php";
    private Map<String, String> params;

    public AddZoneRequest(String email, String lat00, String lng00
            , String lat01, String lng01, String lat10, String lng10, String lat11, String lng11
            , String jour, String mardi, String mercredi, String jeudi, String vendredi, String samedi , String  dimanche
            , String datedebut, String datefin
            , Response.Listener<String> listener) {
        super(Method.POST, REGISTER_REQUEST_URL, listener, null);
        params = new HashMap<>();
        Log.e("lllll",email+lat00+lat10+lat01+lat11+lng00+lng01+lng10+lng11+jour+mardi+mercredi+jeudi+vendredi+samedi+dimanche+datedebut+datefin);
        params.put("email",email);
        params.put("lat00",lat00);
        params.put("lat01",lat01);
        params.put("lat10",lat10);
        params.put("lat11",lat11);
        params.put("lng00",lng00);
        params.put("lng01",lng01);
        params.put("lng10",lng10);
        params.put("lng11",lng11);
        params.put("jour",jour);
        params.put("mardi",mardi);
        params.put("mercredi",mercredi);
        params.put("jeudi",jeudi);
        params.put("vendredi",vendredi);
        params.put("samedi",samedi);
        params.put("dimanche",dimanche);
        params.put("datedebut",datedebut);
        params.put("datefin",datefin);
    }

    @Override
    public Map<String, String> getParams() {
        return params;
    }
}
