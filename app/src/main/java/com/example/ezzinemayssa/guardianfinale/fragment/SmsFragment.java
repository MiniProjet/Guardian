package com.example.ezzinemayssa.guardianfinale.fragment;

import android.app.Fragment;
import android.app.ProgressDialog;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.android.volley.Cache;
import com.android.volley.Network;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.BasicNetwork;
import com.android.volley.toolbox.DiskBasedCache;
import com.android.volley.toolbox.HurlStack;
import com.android.volley.toolbox.Volley;
import com.example.ezzinemayssa.guardianfinale.R;
import com.example.ezzinemayssa.guardianfinale.activity.map.AppercuChildMap;
import com.example.ezzinemayssa.guardianfinale.adapter.RecyclerAdapterDisplaySms;
import com.example.ezzinemayssa.guardianfinale.entities.Sms;
import com.example.ezzinemayssa.guardianfinale.request.GetSmsByEmailChildRequest;
import com.example.ezzinemayssa.guardianfinale.utils.MySingleton;
import com.example.ezzinemayssa.guardianfinale.utils.SimpleDividerItemDecoration;
import com.github.pwittchen.infinitescroll.library.InfiniteScrollListener;
import com.google.android.gms.vision.barcode.Barcode;
import com.google.firebase.auth.FirebaseAuth;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import static com.example.ezzinemayssa.guardianfinale.activity.MainActivity.btnChooseChild;
import static com.example.ezzinemayssa.guardianfinale.adapter.RecyclerAdapterChooseChildren.email_child_chosen;

public class SmsFragment extends Fragment {

    private FirebaseAuth firebaseAuth;
    RecyclerView recyclerView;
    RecyclerAdapterDisplaySms adapter;
    private List<Sms> smsList ;
    private static final int MAX_ITEMS_PER_REQUEST = 5;
    private static final int NUMBER_OF_ITEMS = 200;
    private static final int SIMULATED_LOADING_TIME_IN_MS = 1500;
    private int page;
    TextView searchSms;
    ProgressDialog progress;
    LinearLayout linearLayoutSMS;
    ImageView emptySMS;
    public SmsFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view= inflater.inflate(R.layout.fragment_sms, container, false);
        btnChooseChild.setVisibility(View.GONE);
        searchSms= view.findViewById(R.id.filterSMS);
        recyclerView = view.findViewById(R.id.recycler_view_sms);
        linearLayoutSMS = view.findViewById(R.id.linearLayout_sms);
        emptySMS = view.findViewById(R.id.emptySMS);
        firebaseAuth = FirebaseAuth.getInstance();
        progress = new ProgressDialog(getActivity());
        progress.setTitle("Please wait..");
        progress.setMessage("Processing   ");
        progress.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progress.setIndeterminate(true);

        searchSms.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                filter(editable.toString());
            }
        });
               getSmsDetails();

        return view;
    }



    public void getSmsDetails()
    {
        progress.show();

        RequestQueue mRequestQueue;
        Cache cache = new DiskBasedCache(getActivity().getCacheDir(), 1024 * 1024); // 1MB cap
        Network network = new BasicNetwork(new HurlStack());
        mRequestQueue = new RequestQueue(cache, network);
        mRequestQueue.start();
        smsList  = new LinkedList<>();
        Response.Listener<String> responseListener = new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    JSONObject jsonObject= new JSONObject(response);
                    JSONArray jsonArray= jsonObject.getJSONArray("sms");
                    int count =0;
                    while(count< jsonArray.length())
                    {
                        JSONObject jo= jsonArray.getJSONObject(count);

                        smsList.add(new Sms(jo.getString("state"),jo.getString("number"), Timestamp.valueOf(jo.getString("time"))
                                ,jo.getString("name"),jo.getString("content"),jo.getString("email_child")));
                        count++;

                    }
                    if(smsList.size()!=0)
                    {
                        searchSms.setVisibility(View.VISIBLE);
                        recyclerView.setVisibility(View.VISIBLE);

                        adapter=new RecyclerAdapterDisplaySms(getActivity(), smsList);
                        recyclerView.setHasFixedSize(true);
                        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
                        recyclerView.setAdapter(adapter);
                        recyclerView.addOnScrollListener(createInfiniteScrollListener());


                    }
                    else {

                        emptySMS.setVisibility(View.VISIBLE);
                        recyclerView.setVisibility(View.GONE);
                    }

                    progress.dismiss();

                } catch (JSONException e) {

                    e.printStackTrace();
                }
            }
        };
        Response.ErrorListener responseListener5 = new Response.ErrorListener() {


            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("Tag", error.toString());
                progress.dismiss();


            }
        };

        GetSmsByEmailChildRequest getCallsByEmailChildRequest = new GetSmsByEmailChildRequest(email_child_chosen,responseListener,responseListener5);
        RequestQueue queue = MySingleton.getInstance(getActivity()).getRequestQueue();
// Add a request (in this example, called stringRequest) to your RequestQueue.
        MySingleton.getInstance(getActivity()).addToRequestQueue(getCallsByEmailChildRequest);
    }




    private void filter(String text) {
        ArrayList<Sms> filtredSMS= new ArrayList<>();
        for(Sms item : smsList)
        {
            if(item.getName().toLowerCase().contains(text.toLowerCase()))
            {
                filtredSMS.add(item);
            }
        }

        if(filtredSMS.size()==0)
        {
            emptySMS.setVisibility(View.VISIBLE);
            recyclerView.setVisibility(View.GONE);
        }
        else
        {
            emptySMS.setVisibility(View.GONE);
            recyclerView.setVisibility(View.VISIBLE);
            adapter.filterList(filtredSMS);
        }


}

    @NonNull
    private InfiniteScrollListener createInfiniteScrollListener() {
        return new InfiniteScrollListener(MAX_ITEMS_PER_REQUEST, new LinearLayoutManager(getActivity())) {
            @Override public void onScrolledToEnd(final int firstVisibleItemPosition) {
               // progressBar.setVisibility(View.VISIBLE);
                int start = ++page * MAX_ITEMS_PER_REQUEST;
                final boolean allItemsLoaded = start >= smsList.size();
                if (allItemsLoaded) {
                    //progressBar.setVisibility(View.GONE);
                } else {
                    int end = start + MAX_ITEMS_PER_REQUEST;
                    final List<Sms> itemsLocal = getItemsToBeLoaded(start, end);
                    refreshView(recyclerView, new RecyclerAdapterDisplaySms(getActivity(),itemsLocal), firstVisibleItemPosition);


                }
            }
        };
    }

    @NonNull private List<Sms> getItemsToBeLoaded(int start, int end) {
        List<Sms> newItems = smsList.subList(start, end);
        final List<Sms> oldItems = ((RecyclerAdapterDisplaySms) recyclerView.getAdapter()).getItems();
        final List<Sms> itemsLocal = new LinkedList<>();
        itemsLocal.addAll(oldItems);
        itemsLocal.addAll(newItems);


        return itemsLocal;
    }
}
