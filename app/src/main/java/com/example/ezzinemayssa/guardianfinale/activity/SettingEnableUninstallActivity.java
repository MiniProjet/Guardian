package com.example.ezzinemayssa.guardianfinale.activity;


import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Point;
import android.graphics.Typeface;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.view.Display;

import com.codemybrainsout.onboarder.AhoyOnboarderActivity;
import com.codemybrainsout.onboarder.AhoyOnboarderCard;
import com.example.ezzinemayssa.guardianfinale.R;

import java.util.ArrayList;
import java.util.List;

public class SettingEnableUninstallActivity extends AhoyOnboarderActivity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.setColorBackground(R.color.white);
        AhoyOnboarderCard ahoyOnboarderCard1 = new AhoyOnboarderCard("", "Go to Settings -> Click Plus -> Pick Security", R.drawable.settings1);
        AhoyOnboarderCard ahoyOnboarderCard2 = new AhoyOnboarderCard("", "Choose device admin", R.drawable.settings2);
        AhoyOnboarderCard ahoyOnboarderCard3 = new AhoyOnboarderCard("", "Check Guardian application, then click Activate", R.drawable.settings3);
        //AhoyOnboarderCard ahoyOnboarderCard4 = new AhoyOnboarderCard("", "Click Activate", R.drawable.settings4);
        AhoyOnboarderCard ahoyOnboarderCard5 = new AhoyOnboarderCard("", "Congratulation, now you cannot uninstall the application.", R.drawable.settings5);

        Display display = getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        int width = size.x;
        int height = size.y;

        ahoyOnboarderCard1.setIconLayoutParams(width-(width/2),height-(height/2),0,0,0,0);
        ahoyOnboarderCard2.setIconLayoutParams(width-(width/2),height-(height/2),0,0,0,0);
        ahoyOnboarderCard3.setIconLayoutParams(width-(width/2),height-(height/2),0,0,0,0);
        //ahoyOnboarderCard4.setIconLayoutParams(width-(width/2),height-(height/2),0,0,0,0);
        ahoyOnboarderCard5.setIconLayoutParams(width-(width/2),height-(height/2),0,0,0,0);

        ahoyOnboarderCard1.setBackgroundColor(R.color.black_transparent);
        ahoyOnboarderCard2.setBackgroundColor(R.color.black_transparent);
        ahoyOnboarderCard3.setBackgroundColor(R.color.black_transparent);
       // ahoyOnboarderCard4.setBackgroundColor(R.color.black_transparent);
        ahoyOnboarderCard5.setBackgroundColor(R.color.black_transparent);

        List<AhoyOnboarderCard> pages = new ArrayList<>();

        pages.add(ahoyOnboarderCard1);
        pages.add(ahoyOnboarderCard2);
        pages.add(ahoyOnboarderCard3);
       // pages.add(ahoyOnboarderCard4);
        pages.add(ahoyOnboarderCard5);

        for (AhoyOnboarderCard page : pages) {

            page.setDescriptionColor(R.color.black);

        }

        setFinishButtonTitle("Configure");
        showNavigationControls(true);
        setGradientBackground();

        //set the button style you created
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
            setFinishButtonDrawableStyle(ContextCompat.getDrawable(this, R.drawable.rounded_button));
        }

        Typeface face = Typeface.createFromAsset(getAssets(), "fonts/Roboto-Light.ttf");
        setFont(face);

        setOnboardPages(pages);
    }

    @Override
    public void onFinishButtonPressed() {
        startActivityForResult(new Intent(android.provider.Settings.ACTION_SETTINGS), 0);

    }
}
