package com.example.ezzinemayssa.guardianfinale.fragment;

import android.app.ProgressDialog;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.app.DialogFragment;
import android.support.annotation.Nullable;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ImageView;

import com.android.volley.Cache;
import com.android.volley.Network;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.BasicNetwork;
import com.android.volley.toolbox.DiskBasedCache;
import com.android.volley.toolbox.HurlStack;
import com.android.volley.toolbox.Volley;
import com.example.ezzinemayssa.guardianfinale.R;
import com.example.ezzinemayssa.guardianfinale.activity.map.AppercuChildMap;
import com.example.ezzinemayssa.guardianfinale.adapter.RecyclerAdapterChooseChildren;
import com.example.ezzinemayssa.guardianfinale.entities.Child;
import com.example.ezzinemayssa.guardianfinale.request.GetChildrenByEmailParentRequest;
import com.example.ezzinemayssa.guardianfinale.utils.MySingleton;
import com.felipecsl.gifimageview.library.GifImageView;
import com.google.firebase.auth.FirebaseAuth;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import java.util.ArrayList;
import java.util.List;

import static android.content.Context.MODE_PRIVATE;

public class ChooseChildFragment extends DialogFragment {

    RecyclerView recyclerView;
    RecyclerView.Adapter adapter;
    private List<Child> chooseChildrenList ;
    private FirebaseAuth firebaseAuth;
    ProgressDialog progress;
    private GifImageView noChildToDisplay;
    ImageView emptyChild;
    String userConnected;
    public ChooseChildFragment() {
        // Required empty public constructor

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        SharedPreferences prefs = this.getActivity().getSharedPreferences("USER", MODE_PRIVATE);
        String restoredText = prefs.getString("userConnected", null);
        if (restoredText != null) {
            userConnected = prefs.getString("userConnected", "No name defined");//"No name defined" is the default value.

            Log.e("user",userConnected);
        }


        View view= inflater.inflate(R.layout.fragment_choose_child, container);
        getDialog().requestWindowFeature(Window.FEATURE_NO_TITLE);
        recyclerView = view.findViewById(R.id.recycler_view_chooseChildren);

        emptyChild = view.findViewById(R.id.emptyChooseChildDisplay);
        firebaseAuth = FirebaseAuth.getInstance();
        progress = new ProgressDialog(getActivity());
        progress.setTitle("Please wait..");
        progress.setMessage("Processing   ");
        progress.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progress.setIndeterminate(true);
        chooseChildRequest();

        return view;

    }


    public void chooseChildRequest()
{
    progress.show();
    RequestQueue mRequestQueue;

    chooseChildrenList = new ArrayList<>();
    Response.Listener<String> responseListener = new Response.Listener<String>() {
        @Override
        public void onResponse(String response) {
            try {
                JSONObject jsonObject= new JSONObject(response);
                JSONArray jsonArray= jsonObject.getJSONArray("child");
                int count =0;


                while(count< jsonArray.length())
                {
                    JSONObject jo= jsonArray.getJSONObject(count);
                    chooseChildrenList.add(new Child(jo.getString("code"),jo.getString("email"),jo.getString("username")
                            ,jo.getString("phone"),jo.getString("image_name"),jo.getString("email_parent"),jo.getString("birthdate"),jo.getString("image_qr")));
                    count++;
                }
                if(chooseChildrenList.size()!=0) {
                    adapter = new RecyclerAdapterChooseChildren(getActivity(), chooseChildrenList, getDialog());
                    recyclerView.setLayoutManager(new GridLayoutManager(getActivity(), 2));
                    recyclerView.setAdapter(adapter);
                }
                else
                {
                    emptyChild.setVisibility(View.VISIBLE);
                }
                progress.dismiss();


            } catch (JSONException e) {

                e.printStackTrace();
            }


        }
    };

    Response.ErrorListener responseListener5 = new Response.ErrorListener() {


        @Override
        public void onErrorResponse(VolleyError error) {
            Log.e("Tag", error.toString());
            progress.dismiss();

        }
    };
    GetChildrenByEmailParentRequest getChildrenByEmailParentRequest = new GetChildrenByEmailParentRequest(userConnected, responseListener,responseListener5);
    RequestQueue queue = MySingleton.getInstance(getActivity()).getRequestQueue();
// Add a request (in this example, called stringRequest) to your RequestQueue.
    MySingleton.getInstance(getActivity()).addToRequestQueue(getChildrenByEmailParentRequest);
}

}
