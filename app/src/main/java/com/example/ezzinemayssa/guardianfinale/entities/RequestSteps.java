package com.example.ezzinemayssa.guardianfinale.entities;

/**
 * Created by mayss on 04/12/2017.
 */

public class RequestSteps {
    String request;
    String emailParent;
    String emailChild;


    public RequestSteps(String emailParent, String childChild, String request) {
        this.request = request;
        this.emailParent = emailParent;
        this.emailChild = childChild;
    }

    public String getEmailParent() {
        return emailParent;
    }

    public void setEmailParent(String emailParent) {
        this.emailParent = emailParent;
    }

    public String getChildChild() {
        return emailChild;
    }

    public void setChildChild(String childChild) {
        this.emailChild = childChild;
    }

    @Override
    public String toString() {
        return "RequestSteps{" +
                "request='" + request + '\'' +
                ", emailParent='" + emailParent + '\'' +
                ", childChild='" + emailChild + '\'' +
                '}';
    }

    public RequestSteps() {
    }

    public String getRequest() {
        return request;
    }

    public RequestSteps(String request) {
        this.request = request;
    }

    public void setRequest(String request) {
        this.request = request;
    }
}
