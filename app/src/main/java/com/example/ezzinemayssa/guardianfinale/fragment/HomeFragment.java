package com.example.ezzinemayssa.guardianfinale.fragment;

import android.app.DialogFragment;
import android.app.Fragment;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.ProgressBar;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;
import com.example.ezzinemayssa.guardianfinale.R;
import com.example.ezzinemayssa.guardianfinale.activity.map.AppercuChildMap;
import com.example.ezzinemayssa.guardianfinale.activity.map.CreationZoneMap;
import com.example.ezzinemayssa.guardianfinale.activity.map.TrackingMapActivity;
import com.example.ezzinemayssa.guardianfinale.adapter.RecyclerAdapterChooseChildren;
import com.example.ezzinemayssa.guardianfinale.entities.Tracking;
import com.example.ezzinemayssa.guardianfinale.request.GetLocationOfOneUser;
import com.example.ezzinemayssa.guardianfinale.utils.MySingleton;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import static android.content.Context.MODE_PRIVATE;
import static com.example.ezzinemayssa.guardianfinale.activity.MainActivity.btnChooseChild;
import static com.example.ezzinemayssa.guardianfinale.adapter.RecyclerAdapterChooseChildren.email_child_chosen;

public class HomeFragment extends Fragment {
Button btnLocalisation;
Button btnHistory;
Button btnAllowedZone;
Button btnCalls;
Button btnSms;
Button btnChat;
Button btnChildren;
Button btnApplication;
Button btnSettings;
ProgressDialog progress;

    public static List<Tracking> listLocation;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_home, container, false);
        btnChooseChild.setVisibility(View.VISIBLE);

        listLocation=new ArrayList<>();
        btnLocalisation=view.findViewById(R.id.btn_home_location);
        btnHistory=view.findViewById(R.id.btn_home_history);
        btnAllowedZone=view.findViewById(R.id.btn_home_allowedZone);
        btnCalls=view.findViewById(R.id.btn_home_calls);
        btnSms=view.findViewById(R.id.btn_home_sms);
        btnApplication=view.findViewById(R.id.btn_home_chat);
        btnChildren=view.findViewById(R.id.btn_home_children);
        btnSettings=view.findViewById(R.id.btn_home_application);
        btnChat=view.findViewById(R.id.btn_home_settings);

        return view;
    }


    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        if(email_child_chosen==null)
        {
            showChooseChildrenDialog();
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        progress = new ProgressDialog(getActivity());
        progress.setTitle("Please wait..");
        progress.setMessage("Processing   ");
        progress.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progress.setIndeterminate(true);

        SharedPreferences prefs = getActivity().getSharedPreferences("USER", MODE_PRIVATE);
        String restoredText = prefs.getString("calls", null);
        if (restoredText != null) {
            String callsStatus = prefs.getString("calls", "no");//"No name defined" is the default value.
            if(callsStatus.equals("yes")){
            btnCalls.setVisibility(View.INVISIBLE);
            }

        }



        String restoredText2 = prefs.getString("sms", null);
        if (restoredText2 != null) {
            String callsStatus = prefs.getString("sms", "no");//"No name defined" is the default value.
            if(callsStatus.equals("yes")){
                btnSms.setVisibility(View.INVISIBLE);
            }

        }



        String restoredText3 = prefs.getString("zone", null);
        if (restoredText3 != null) {
            String callsStatus = prefs.getString("zone", "no");//"No name defined" is the default value.
            if(callsStatus.equals("yes")){
                btnAllowedZone.setVisibility(View.INVISIBLE);
            }

        }




        String restoredText4 = prefs.getString("app", null);
        if (restoredText4 != null) {
            String callsStatus = prefs.getString("app", "no");//"No name defined" is the default value.
            if(callsStatus.equals("yes")){
                btnApplication.setVisibility(View.INVISIBLE);
            }

        }


















        btnLocalisation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                    Intent intent = new Intent(getActivity(), TrackingMapActivity.class);
                    startActivity(intent);


            }
        });
        btnHistory.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(email_child_chosen==null)
                {
                    showChooseChildrenDialog();
                }
                else
                {
                    historyLocation();

                }

            }
        });
        btnAllowedZone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(email_child_chosen==null)
                {
                    showChooseChildrenDialog();
                }
                else
                {
                    Intent intent = new Intent(getActivity(), CreationZoneMap.class);
                    startActivity(intent);
                }

            }
        });
        btnCalls.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(email_child_chosen==null)
                {
                    showChooseChildrenDialog();
                }
                else
                {
                    getFragmentManager().beginTransaction().replace(R.id.displayContainer, new CallsChildFragment()).addToBackStack(null).commit();

                }


            }
        });
        btnSms.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(email_child_chosen==null)
                {
                    showChooseChildrenDialog();

                }
                else
                {
                    getFragmentManager().beginTransaction().replace(R.id.displayContainer, new SmsFragment()).addToBackStack(null).commit();
                }


            }
        });
        btnChildren.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                getFragmentManager().beginTransaction().replace(R.id.displayContainer, new DisplayChildFragment()).addToBackStack(null).commit();

            }
        });
        btnApplication.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(email_child_chosen==null)
                {
                    showChooseChildrenDialog();
                }
                else
                {
                    getFragmentManager().beginTransaction().replace(R.id.displayContainer, new ListAppChildFragment()).addToBackStack(null).commit();
                }


            }
        });

        btnChat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getFragmentManager().beginTransaction().replace(R.id.displayContainer, new ChatMessageFragment()).addToBackStack(null).commit();

            }
        });
        btnSettings.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getFragmentManager().beginTransaction().replace(R.id.displayContainer, new SettingsFragment()).addToBackStack(null).commit();

            }
        });


    }
    public void historyLocation(){
        progress.show();
        Response.Listener<String> responseListener3 = new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {


                try {

                    JSONObject jsonResponse = new JSONObject(response);
                    // boolean success = jsonResponse.getBoolean("success");

                    JSONArray jsonArray= jsonResponse.getJSONArray("location");
                    int count=0;
                    while(count<jsonArray.length())
                    {
                        JSONObject jo= jsonArray.getJSONObject(count);
                        listLocation.add(new Tracking( jo.getString("email"),
                                jo.getString("uid"), jo.getString("lat"), jo.getString("lng")));
                        count++;


                    }
                    Intent intent3 = new Intent(getActivity(), AppercuChildMap.class);
                    progress.dismiss();

                    startActivity(intent3);
                    Log.e("list",listLocation.toString());
                } catch (JSONException e) {
                    e.printStackTrace();
                }



            }
        };
        Response.ErrorListener responseListener5 = new Response.ErrorListener() {


            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("Tag", error.toString());
                progress.dismiss();


            }
        };
        GetLocationOfOneUser addlocation = new GetLocationOfOneUser(RecyclerAdapterChooseChildren.email_child_chosen,responseListener3,responseListener5);

         RequestQueue queue = MySingleton.getInstance(getActivity()).getRequestQueue();
// Add a request (in this example, called stringRequest) to your RequestQueue.
        MySingleton.getInstance(getActivity()).addToRequestQueue(addlocation);


    }
    public void showChooseChildrenDialog() {
        DialogFragment newFragment = new ChooseChildFragment();

        newFragment.show(getFragmentManager(), "chooseChildrenDialog");


    }
}
