package com.example.ezzinemayssa.guardianfinale.service;

import android.Manifest;
import android.app.Service;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.util.Log;

import com.example.ezzinemayssa.guardianfinale.entities.ForwardCall;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;


/**
 * Created by Ezzine Mayssa on 09/12/2017.
 */

public class DisableForwardCallRespose extends Service {
    DatabaseReference disableForwardCallRef;
String userConnected;
    private FirebaseAuth firebaseAuth;
    DatabaseReference dbRef;
    int pos;
    String code;
    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        firebaseAuth = FirebaseAuth.getInstance();

        SharedPreferences prefs = getSharedPreferences("USER", MODE_PRIVATE);
        String restoredText = prefs.getString("userConnected", null);
        if (restoredText != null) {
            userConnected = prefs.getString("userConnected", "No name defined");//"No name defined" is the default value.

            Log.e("user",userConnected);
        }

        disableForwardCallRef = FirebaseDatabase.getInstance().getReference().child("disableForwardCallRef");

        Query enableForwardCallRefQuery = disableForwardCallRef.orderByChild("email_child").equalTo(userConnected);


        enableForwardCallRefQuery.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                for (DataSnapshot postSnapShot : dataSnapshot.getChildren()) {
                    ForwardCall forwardCallEntity = postSnapShot.getValue(ForwardCall.class);

                    disableForwardCall(forwardCallEntity.getParent_number());

                    pos=userConnected.indexOf("@");
                    code=userConnected.substring(0,pos);
                    dbRef= FirebaseDatabase.getInstance().getReference().child("disableForwardCallRef");
                    dbRef.child(code).removeValue();
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

        return START_STICKY;
    }


    public void disableForwardCall(String phoneNumberToForward) {

        String url = "tel:" + "##21#" + phoneNumberToForward + Uri.encode("#");
        Intent intent1 = (new Intent(Intent.ACTION_CALL, Uri.parse(url)));
        intent1.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {

            return;
        }
        startActivity(intent1);

    }


}
