package com.example.ezzinemayssa.guardianfinale.request;

import com.android.volley.Response;
import com.android.volley.toolbox.StringRequest;

import java.sql.Timestamp;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import static com.example.ezzinemayssa.guardianfinale.activity.MainActivity.ipAdress;

/**
 * Created by Ezzine Mayssa on 13/11/2017.
 */

public class AddCallsByEmailChildRequest extends StringRequest {


        private static final String REGISTER_REQUEST_URL = "http://"+ipAdress+"/guardenScript/AddCallsByEmailChild.php";
        private Map<String, String> params;

        public AddCallsByEmailChildRequest(String state, String number, Timestamp start_time, String contact_name, String email_child, Response.Listener<String> listener) {
            super(Method.POST, REGISTER_REQUEST_URL, listener, null);
            params = new HashMap<>();

            params.put("state",state);
            params.put("number",number);
            params.put("start_time",start_time+"");
            params.put("contact_name",contact_name);
            params.put("email_child", email_child);

        }

        @Override
        public Map<String, String> getParams() {
            return params;
        }
    }

