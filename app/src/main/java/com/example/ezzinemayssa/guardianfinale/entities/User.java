package com.example.ezzinemayssa.guardianfinale.entities;

/**
 * Created by zasus on 02/01/2018.
 */

public class User {



    String email;
    String status;

    public User() {
    }

    public User(String email, String status) {
        this.email = email;
        this.status = status;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return "User{" +
                "email='" + email + '\'' +
                ", status='" + status + '\'' +
                '}';
    }
}
