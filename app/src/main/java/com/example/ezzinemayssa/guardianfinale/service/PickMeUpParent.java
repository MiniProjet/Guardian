package com.example.ezzinemayssa.guardianfinale.service;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.util.Log;

import com.example.ezzinemayssa.guardianfinale.R;
import com.example.ezzinemayssa.guardianfinale.activity.map.PickMeUpMapActivity;
import com.example.ezzinemayssa.guardianfinale.entities.PickMeUp;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import java.util.Random;

/**
 * Created by mayss on 24/11/2017.
 */

public class PickMeUpParent extends Service {
    DatabaseReference pickMeUp;
    private FirebaseAuth firebaseAuth;
    String userConnected;
    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }
    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {

        SharedPreferences prefs = getSharedPreferences("USER", MODE_PRIVATE);
        String restoredText = prefs.getString("userConnected", null);
        if (restoredText != null) {
            userConnected = prefs.getString("userConnected", "No name defined");//"No name defined" is the default value.

            Log.e("user",userConnected);
        }


        firebaseAuth = FirebaseAuth.getInstance();
        pickMeUp= FirebaseDatabase.getInstance().getReference().child("pickMeUp");
        Query childPickMe = pickMeUp.orderByChild("nameChild").equalTo(userConnected);
        childPickMe.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                for(DataSnapshot postSnapShot:dataSnapshot.getChildren())
                {Log.e("pick","me");
                    PickMeUp pickMe = postSnapShot.getValue(PickMeUp.class);
                    sendNotification("Pick me up","Papa je veux rentrer à"+pickMe.getTime());

                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
        //we have some options for service
        //start sticky means service will be explicity started and stopped
        return START_STICKY;
    }
    public void sendNotification (String title ,String content)
    {
        Notification.Builder builder = new Notification.Builder(getApplication())
                .setSmallIcon(R.mipmap.ic_launcher_round)
                .setContentTitle(title)
                .setContentText(content);
        NotificationManager manager = (NotificationManager) getApplication().getSystemService(NOTIFICATION_SERVICE);
        Intent intent = new Intent(getApplication(),PickMeUpMapActivity.class);
        PendingIntent contentInent = PendingIntent.getActivity(getApplication(),0,intent,PendingIntent.FLAG_IMMUTABLE);
        builder.setContentIntent(contentInent);
        Notification notification = null;
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.JELLY_BEAN) {
            notification = builder.build();
        }
        notification.flags|= Notification.FLAG_AUTO_CANCEL;
        notification.defaults|=Notification.DEFAULT_SOUND;

        manager.notify(new Random().nextInt(),notification);

    }
}
