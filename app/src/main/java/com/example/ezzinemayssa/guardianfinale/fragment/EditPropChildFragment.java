package com.example.ezzinemayssa.guardianfinale.fragment;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.Cache;
import com.android.volley.Network;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.BasicNetwork;
import com.android.volley.toolbox.DiskBasedCache;
import com.android.volley.toolbox.HurlStack;
import com.android.volley.toolbox.Volley;
import com.example.ezzinemayssa.guardianfinale.R;
import com.example.ezzinemayssa.guardianfinale.activity.MainActivity;
import com.example.ezzinemayssa.guardianfinale.adapter.RecyclerAdapterDisplayChildren;
import com.example.ezzinemayssa.guardianfinale.entities.Child;
import com.example.ezzinemayssa.guardianfinale.request.AddChildRequest;
import com.example.ezzinemayssa.guardianfinale.request.EditChildRequest;
import com.example.ezzinemayssa.guardianfinale.request.GetChildrenByEmailParentRequest;
import com.example.ezzinemayssa.guardianfinale.request.GetOneChildByEmailRequest;
import com.example.ezzinemayssa.guardianfinale.utils.MySingleton;
import com.google.firebase.auth.FirebaseAuth;
import com.squareup.picasso.Picasso;
import com.theartofdev.edmodo.cropper.CropImage;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

import static android.app.Activity.RESULT_OK;
import static android.content.Context.MODE_PRIVATE;
import static com.example.ezzinemayssa.guardianfinale.adapter.RecyclerAdapterDisplayChildren.emailChildSelected;
import static com.example.ezzinemayssa.guardianfinale.fragment.DisplayChildFragment.adapterDisplayChild;
import static com.example.ezzinemayssa.guardianfinale.fragment.DisplayChildFragment.recyclerViewDisplayChild;


public class EditPropChildFragment extends DialogFragment {


    private Child childObj;
    private List<Child> childrenList ;
    private List<Child> childrenListDisplay ;
    private EditText username;
    private EditText birthday;
    private EditText phone;
    private ImageView editProfilePicture;
    private FirebaseAuth firebaseAuth;
    Calendar mycal= Calendar.getInstance();
    Bitmap bitmapProfil;
    String userConnected;
    final int  CODE_GALLERY_REQUEST=203;

    ProgressDialog progress;

    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = getActivity().getLayoutInflater();
        final View dialogView =inflater.inflate(R.layout.fragment_edit_prop_child, null);

        SharedPreferences prefs = this.getActivity().getSharedPreferences("USER", MODE_PRIVATE);
        String restoredText = prefs.getString("userConnected", null);
        if (restoredText != null) {
            userConnected = prefs.getString("userConnected", "No name defined");//"No name defined" is the default value.

            Log.e("user",userConnected);
        }


        editProfilePicture = dialogView.findViewById(R.id.editProfilePicture);
        username = dialogView.findViewById(R.id.edit_username);
        birthday = dialogView.findViewById(R.id.edit_birthday);
        phone = dialogView.findViewById(R.id.edit_phone);
        firebaseAuth= FirebaseAuth.getInstance();
        progress = new ProgressDialog(getActivity());
        progress.setTitle("Please wait..");
        progress.setMessage("Processing   ");
        progress.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progress.setIndeterminate(true);

        username.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if(username.getText().length()==0){
                    Log.e("username","username");
                    username.setError("username is empty");
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        phone.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if(phone.getText().length()==0){
                    Log.e("username","username");
                    phone.setError("username is empty");
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        getChildDetails();
        editProfilePicture.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.M)
            @Override
            public void onClick(View view) {
                CropImage.activity()
                        .start(getActivity(),EditPropChildFragment.this);
                Intent intent = new Intent(Intent.ACTION_PICK);
                intent.setType("image/*");
                startActivityForResult(Intent.createChooser(intent,"Select Image"),CODE_GALLERY_REQUEST);

            }
        });
        builder.setView(dialogView)

                .setPositiveButton("Edit", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {

                    editChild();
                    displayChildafterEdit();

                    }
                })
                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        EditPropChildFragment.this.getDialog().cancel();
                    }
                });




        return builder.create();
    }



    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(requestCode== CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE && resultCode == RESULT_OK && data!= null )
        {
            CropImage.ActivityResult result = CropImage.getActivityResult(data);
            Uri filePath= result.getUri();
            Context applicationContext = MainActivity.getContextOfApplication();
            try {
                InputStream inputStream= applicationContext.getContentResolver().openInputStream(filePath);
                bitmapProfil = BitmapFactory.decodeStream(inputStream);

                editProfilePicture.setImageBitmap(bitmapProfil);

            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
        }
        super.onActivityResult(requestCode, resultCode, data);
    }



    public void getChildDetails()
    {

        progress.show();

        RequestQueue mRequestQueue;

        childrenList = new ArrayList<>();

        Response.Listener<String> responseListener = new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    JSONObject jsonObject= new JSONObject(response);
                    JSONArray jsonArray= jsonObject.getJSONArray("oneChild");
                    int count =0;


                    while(count< jsonArray.length())
                    {
                        JSONObject jo= jsonArray.getJSONObject(count);
                        childrenList.add(new Child(jo.getString("code"),jo.getString("email"),jo.getString("username")
                                ,jo.getString("phone"),jo.getString("image_name"),jo.getString("email_parent"),jo.getString("birthdate"),jo.getString("image_qr")));
                        count++;

                    }
                if(childrenList.size()!=0) {
                    childObj = childrenList.get(0);
                    username.setText(childObj.getUsername());
                    birthday.setText(childObj.getBirthdate());
                    phone.setText(childObj.getPhone());
                    birthday.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            new DatePickerDialog(getActivity(), date2, mycal.get(Calendar.YEAR), mycal.get(Calendar.MONTH), mycal.get(Calendar.DAY_OF_MONTH)).show();
                        }
                    });


                    Picasso.with(getActivity()).load(childObj.getImage_name()).into(editProfilePicture);
                    }
                    progress.dismiss();



                } catch (JSONException e) {

                    e.printStackTrace();
                }
            }
        };
        Response.ErrorListener responseListener5 = new Response.ErrorListener() {


            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("Tag", error.toString());
                progress.dismiss();


            }
        };
        GetOneChildByEmailRequest getOneChildByEmailRequest = new GetOneChildByEmailRequest(emailChildSelected, responseListener,responseListener5);
        MySingleton.getInstance(getActivity()).addToRequestQueue(getOneChildByEmailRequest);


    }
    public  void displayChildafterEdit()
    {
        progress.show();
        RequestQueue mRequestQueue;

        childrenListDisplay = new ArrayList<>();
        Response.Listener<String> responseListener = new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    JSONObject jsonObject= new JSONObject(response);
                    JSONArray jsonArray= jsonObject.getJSONArray("child");
                    int count =0;


                    while(count< jsonArray.length())
                    {
                        JSONObject jo= jsonArray.getJSONObject(count);
                        childrenListDisplay.add(new Child(jo.getString("code"),jo.getString("email"),jo.getString("username")
                                ,jo.getString("phone"),jo.getString("image_name"),jo.getString("email_parent"),jo.getString("birthdate"),jo.getString("image_qr")));
                        count++;


                    }

                    adapterDisplayChild=new RecyclerAdapterDisplayChildren(getActivity(), childrenListDisplay );
                    recyclerViewDisplayChild.setLayoutManager(new LinearLayoutManager(getActivity()));
                    recyclerViewDisplayChild.setAdapter(adapterDisplayChild);
                    adapterDisplayChild.notifyDataSetChanged();

                    progress.dismiss();


                } catch (JSONException e) {

                    e.printStackTrace();
                }
            }
        };
        Response.ErrorListener responseListener5 = new Response.ErrorListener() {


            @Override
            public void onErrorResponse(VolleyError error) {

                progress.dismiss();

            }
        };
        GetChildrenByEmailParentRequest getChildrenByEmailParentRequest = new GetChildrenByEmailParentRequest(userConnected, responseListener,responseListener5);
        MySingleton.getInstance(getActivity()).addToRequestQueue(getChildrenByEmailParentRequest);
    }

    public String getStringImage(Bitmap bmp){
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bmp.compress(Bitmap.CompressFormat.JPEG, 100, baos);
        byte[] imageBytes = baos.toByteArray();
        String encodedImage = Base64.encodeToString(imageBytes, Base64.DEFAULT);
        return encodedImage;
    }
    public void editChild()
    {





        if((username.getText().length()!=0)&&(phone.getText().length()==8))
        { progress.show();

            Response.Listener<String> responseListener = new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    try {
                        JSONObject jsonResponse = new JSONObject(response);
                        String activated = jsonResponse.getString("editChild");

                        if (activated.equals("true")) {
                            progress.dismiss();
                        }


                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            };

            Response.ErrorListener responseListener5 = new Response.ErrorListener() {


                @Override
                public void onErrorResponse(VolleyError error) {
                    progress.setTitle("Error");
                    progress.setMessage("An error has occurred while ");
                    progress.setProgressStyle(ProgressDialog.STYLE_SPINNER);
                    progress.setIndeterminate(true);
                    progress.dismiss();

                }
            };

            EditChildRequest editChildRequest = new EditChildRequest(getStringImage(((BitmapDrawable)editProfilePicture.getDrawable()).getBitmap()),childObj.getCode(),username.getText().toString(),phone.getText().toString(),userConnected,birthday.getText().toString(),responseListener,responseListener5);
            MySingleton.getInstance(getActivity()).addToRequestQueue(editChildRequest);

        }


    }

    final DatePickerDialog.OnDateSetListener date2=new DatePickerDialog.OnDateSetListener() {
        @Override
        public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
            mycal.set(Calendar.YEAR,year);
            mycal.set(Calendar.MONTH,month);
            mycal.set(Calendar.DAY_OF_MONTH,dayOfMonth);
            String format="MM/dd/yy";
            SimpleDateFormat simpleDateFormat=new SimpleDateFormat(format, Locale.FRANCE);
            birthday.setText(simpleDateFormat.format(mycal.getTime()));


        }
    };
}
