package com.example.ezzinemayssa.guardianfinale.activity.map;

import android.app.DatePickerDialog;
import android.content.DialogInterface;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.SeekBar;

import com.android.volley.Cache;
import com.android.volley.Network;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.BasicNetwork;
import com.android.volley.toolbox.DiskBasedCache;
import com.android.volley.toolbox.HurlStack;
import com.android.volley.toolbox.Volley;
import com.example.ezzinemayssa.guardianfinale.R;
import com.example.ezzinemayssa.guardianfinale.adapter.RecyclerAdapterDisplayChildren;
import com.example.ezzinemayssa.guardianfinale.entities.Child;
import com.example.ezzinemayssa.guardianfinale.entities.Tracking;

import com.example.ezzinemayssa.guardianfinale.request.GetChildrenByEmailParentRequest;
import com.example.ezzinemayssa.guardianfinale.request.GetOneChildByEmailRequest;
import com.example.ezzinemayssa.guardianfinale.request.SendingNotifRequest;
import com.example.ezzinemayssa.guardianfinale.service.GestureDetection;
import com.example.ezzinemayssa.guardianfinale.utils.MySingleton;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import static com.example.ezzinemayssa.guardianfinale.activity.MainActivity.childrenListMainActivity;
import static com.example.ezzinemayssa.guardianfinale.adapter.RecyclerAdapterDisplayChildren.emailChildSelected;


public class TrackingMapActivity extends FragmentActivity implements OnMapReadyCallback {

    private GoogleMap mMap;


    String email2="";
    DatabaseReference locations;
    Double lat,lng;
    SeekBar seekBar;
    private List<Child> childrenList ;
    LatLng latLngOfPolygone,friendLocation;
    private Child childObj;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tracking_map);


        seekBar =(SeekBar) findViewById(R.id.seekBar);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            getWindow().setStatusBarColor(Color.LTGRAY);
        }
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        locations = FirebaseDatabase.getInstance().getReference("locations");





    }

    private void loadLocationForThisUser(final String email) {
        Query user_Location = locations.orderByChild("email").equalTo(email);

        user_Location.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if(!dataSnapshot.getChildren().iterator().hasNext()){
buildAlertMessageNoGps(email);

                }
                for(DataSnapshot postSnapShot:dataSnapshot.getChildren())
                {
                    Tracking tracking = postSnapShot.getValue(Tracking.class);


                    friendLocation=new LatLng(Double.parseDouble(tracking.getLat()),
                            Double.parseDouble(tracking.getLng()));
                    email2  =tracking.getEmail();
                    Log.e("location",email2);
                    mMap.clear();

                    mMap.addMarker(new MarkerOptions()
                            .position(friendLocation).title(email)
                            .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_BLUE)));


                    CameraPosition cameraPosition = new CameraPosition.Builder().target(friendLocation).zoom(15.0f).build();
                    CameraUpdate cameraUpdate = CameraUpdateFactory.newCameraPosition(cameraPosition);
                    mMap.moveCamera(cameraUpdate);





                }

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Log.e("error","canceled");
            }
        });
    }


    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        int size=childrenListMainActivity.size();
       for(int i=0;i<size;i++){
           loadLocationForThisUser(childrenListMainActivity.get(i).getEmail().trim());

       }




    }

    private void buildAlertMessageNoGps(String email) {
        final AlertDialog.Builder builder = new AlertDialog.Builder(this);

        builder.setMessage("Your child's "+email+" GPS is disabled ")
                .setCancelable(false)
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(@SuppressWarnings("unused")
                                        final DialogInterface dialog, @SuppressWarnings("unused") final int id) {

                    }
                });
        final AlertDialog alert = builder.create();

        alert.show();


    }

    public void getChildDetails(String email)
    {



        childrenList = new ArrayList<>();

        Response.Listener<String> responseListener = new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    JSONObject jsonObject= new JSONObject(response);
                    JSONArray jsonArray= jsonObject.getJSONArray("oneChild");
                    int count =0;


                    while(count< jsonArray.length())
                    {
                        JSONObject jo= jsonArray.getJSONObject(count);
                        childrenList.add(new Child(jo.getString("code"),jo.getString("email"),jo.getString("username")
                                ,jo.getString("phone"),jo.getString("image_name"),jo.getString("email_parent"),jo.getString("birthdate"),jo.getString("image_qr")));
                        count++;

                    }

                        childObj = childrenList.get(0);
                    buildAlertMessageNoGps( childObj.getUsername());





                } catch (JSONException e) {

                    e.printStackTrace();
                }
            }
        };
        Response.ErrorListener responseListener5 = new Response.ErrorListener() {


            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("Tag", error.toString());


            }
        };
        GetOneChildByEmailRequest getOneChildByEmailRequest = new GetOneChildByEmailRequest(email, responseListener,responseListener5);
        MySingleton.getInstance(TrackingMapActivity.this).addToRequestQueue(getOneChildByEmailRequest);


    }

}
