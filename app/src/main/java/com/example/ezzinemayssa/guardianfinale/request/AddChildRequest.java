package com.example.ezzinemayssa.guardianfinale.request;

import com.android.volley.Response;
import com.android.volley.toolbox.StringRequest;

import java.util.HashMap;
import java.util.Map;

import static com.example.ezzinemayssa.guardianfinale.activity.MainActivity.ipAdress;

/**
 * Created by Ezzine Mayssa on 13/11/2017.
 */

public class AddChildRequest extends StringRequest {


        private static final String REGISTER_REQUEST_URL = "http://"+ipAdress+"/guardenScript/addChild.php";
        private Map<String, String> params;

        public AddChildRequest(String image_name,String image_qr, String code, String email, String username,String phone,
                               String email_parent ,String birthdate ,Response.Listener<String> listener) {
            super(Method.POST, REGISTER_REQUEST_URL, listener, null);
            params = new HashMap<>();

            params.put("image_name",image_name);
            params.put("image_qr",image_qr);
            params.put("code",code);
            params.put("email",email);
            params.put("username", username);
            params.put("phone", phone);
            params.put("email_parent", email_parent);
            params.put("birthdate",birthdate);

        }

        @Override
        public Map<String, String> getParams() {
            return params;
        }
    }

