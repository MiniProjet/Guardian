package com.example.ezzinemayssa.guardianfinale.fragment;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TimePicker;

import com.example.ezzinemayssa.guardianfinale.R;
import com.example.ezzinemayssa.guardianfinale.entities.PickMeUp;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

public class PickMeUpParentDialog extends DialogFragment {
    TimePicker time ;
    DatabaseReference pickMeUp;
    DatabaseReference pickMeUpResponse;
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        pickMeUp= FirebaseDatabase.getInstance().getReference().child("pickMeUp");
        pickMeUpResponse= FirebaseDatabase.getInstance().getReference().child("pickMeUpResponse");
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        // Get the layout inflater
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View dialogView =inflater.inflate(R.layout.fragment_pick_me_up_parent_dialog, null);
        time=(TimePicker) dialogView.findViewById(R.id.timePicker2);
        // Inflate and set the layout for the dialog
        // Pass null as the parent view because its going in the dialog layout
        builder.setView(dialogView)
                // Add action buttons
                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {
                        pickMeUp.child("pickMeUp").removeValue();
                        pickMeUpResponse.child("pickmeUp").setValue(new PickMeUp("halim@jerbi.com","halim@jerbi.com",time.getCurrentHour()+":"+time.getCurrentMinute()));


                    }
                })
                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        PickMeUpParentDialog.this.getDialog().cancel();
                    }
                });

        return builder.create();
    }

}
