package com.example.ezzinemayssa.guardianfinale.receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.BatteryManager;
import android.util.Log;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;
import com.example.ezzinemayssa.guardianfinale.activity.Home_Child_Activity;
import com.example.ezzinemayssa.guardianfinale.entities.Child;
import com.example.ezzinemayssa.guardianfinale.request.GetOneChildByEmailRequest;
import com.example.ezzinemayssa.guardianfinale.request.GetRefreshTokken;
import com.example.ezzinemayssa.guardianfinale.request.SendingNotifRequest;
import com.example.ezzinemayssa.guardianfinale.utils.MySingleton;
import com.google.firebase.auth.FirebaseAuth;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by mayss on 04/01/2018.
 */

public class PowerLevelReciver extends BroadcastReceiver {
    FirebaseAuth firebaseAuth;
    String tokken;

    @Override
    public void onReceive(final Context context, Intent intent) {
        firebaseAuth= FirebaseAuth.getInstance();
        IntentFilter ifilter = new IntentFilter(Intent.ACTION_BATTERY_CHANGED);
        Intent batteryStatus = context.registerReceiver(null, ifilter);

        int level = batteryStatus.getIntExtra(BatteryManager.EXTRA_LEVEL, -1);
        int scale = batteryStatus.getIntExtra(BatteryManager.EXTRA_SCALE, -1);

        float batteryPct = level / (float)scale;
        if(batteryPct==5f){



            Log.e("3","3");

            Response.Listener<String> responseListener2 = new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {

                    try {
                        JSONObject jsonResponse = new JSONObject(response);

                        JSONArray jsonArray= jsonResponse.getJSONArray("tokken");
                        int count= 0;
                        while(count<jsonArray.length())
                        {
                            JSONObject jo= jsonArray.getJSONObject(count);

                            tokken= jo.getString("tokken");
                            count++;

                            Log.e("Response",tokken);
                            Response.Listener<String> responseListener = new Response.Listener<String>() {
                                @Override
                                public void onResponse(String response) {


                                }
                            };

                            SendingNotifRequest getChildrenByEmailParentRequest = new SendingNotifRequest(
                                    "Low energy",  tokken,"Votre Enfant"+firebaseAuth.getCurrentUser().getEmail()+"a depassé une zone", responseListener);
                            RequestQueue queue = MySingleton.getInstance(context).getRequestQueue();
// Add a request (in this example, called stringRequest) to your RequestQueue.
                            MySingleton.getInstance(context).addToRequestQueue(getChildrenByEmailParentRequest);
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }




                }
            };

            GetRefreshTokken tokkenParent = new GetRefreshTokken(firebaseAuth.getCurrentUser().getEmail(), responseListener2);
            RequestQueue queue = MySingleton.getInstance(context).getRequestQueue();
// Add a request (in this example, called stringRequest) to your RequestQueue.
            MySingleton.getInstance(context).addToRequestQueue(tokkenParent);

        }
    }


}
