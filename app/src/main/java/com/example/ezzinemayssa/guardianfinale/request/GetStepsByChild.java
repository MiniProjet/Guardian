package com.example.ezzinemayssa.guardianfinale.request;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.toolbox.StringRequest;
import com.example.ezzinemayssa.guardianfinale.activity.MainActivity;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by mayss on 18/12/2017.
 */

public class GetStepsByChild extends StringRequest {

    private static final String LOGIN_REQUEST_URL = "http://"+ MainActivity.ipAdress+"/guardenScript/GetStepsByChild.php";
    private Map<String, String> params;

    public GetStepsByChild(String email, Response.Listener<String> listener) {
        super(Request.Method.POST, LOGIN_REQUEST_URL, listener, null);
        params = new HashMap<>();
        params.put("email", email);


    }

    @Override
    public Map<String, String> getParams() {
        return params;
    }

}
