package com.example.ezzinemayssa.guardianfinale.service;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Intent;
import android.util.Log;

import com.example.ezzinemayssa.guardianfinale.R;
import com.example.ezzinemayssa.guardianfinale.activity.map.TrackingMapActivity;
import com.example.ezzinemayssa.guardianfinale.fragment.PickMeUpParentDialog;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import java.util.Random;

/**
 * Created by mayss on 04/01/2018.
 */

public class MyFirebaseMessagingService extends FirebaseMessagingService {

    private static final String TAG = "message"; //MyFirebaseMessagingService.class.getSimpleName();



    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        Log.e(TAG, "From: " + remoteMessage.getFrom());

        if (remoteMessage == null)
            return;

        // Check if message contains a notification payload.
        if (remoteMessage.getNotification() != null) {
            Log.e(TAG, "Notification Body: " + remoteMessage.getNotification().getBody());
            if( remoteMessage.getNotification().getTitle().equals("Emergency")) {
                sendNotificationEmergency(remoteMessage.getNotification().getTitle(), remoteMessage.getNotification().getBody());
            }
        }

        if (remoteMessage.getNotification() != null) {
            Log.e(TAG, "Notification Body: " + remoteMessage.getNotification().getBody());
            if( remoteMessage.getNotification().getTitle().equals("Pick Me Up")) {
                sendNotifPickMe(remoteMessage.getNotification().getTitle(), remoteMessage.getNotification().getBody());
            }
        }

        if (remoteMessage.getNotification() != null) {
            Log.e(TAG, "Notification Body: " + remoteMessage.getNotification().getBody());
            if( remoteMessage.getNotification().getTitle().equals("Zone")) {
                sendNotifZone(remoteMessage.getNotification().getTitle(), remoteMessage.getNotification().getBody());
            }
        }
    }





    /**
     * Showing notification with text only
     */
    public void sendNotificationEmergency (String title ,String content)
    {
        Notification.Builder builder = new Notification.Builder(getApplication())
                .setSmallIcon(R.mipmap.ic_launcher_round)
                .setContentTitle(title)
                .setContentText(content);

            NotificationManager manager = (NotificationManager) getApplication().getSystemService(NOTIFICATION_SERVICE);
            Intent intent = new Intent(getApplication(), TrackingMapActivity.class);
            PendingIntent contentInent = PendingIntent.getActivity(getApplication(), 0, intent, PendingIntent.FLAG_IMMUTABLE);
            builder.setContentIntent(contentInent);
            Notification notification = null;
            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.JELLY_BEAN) {
                notification = builder.build();
            }
            notification.flags |= Notification.FLAG_AUTO_CANCEL;
            notification.defaults |= Notification.DEFAULT_SOUND;

            manager.notify(new Random().nextInt(), notification);


        }


    public void sendNotifPickMe (String title ,String content)
    {
        Notification.Builder builder = new Notification.Builder(getApplication())
                .setSmallIcon(R.mipmap.ic_launcher_round)
                .setContentTitle(title)
                .setContentText(content);

        NotificationManager manager = (NotificationManager) getApplication().getSystemService(NOTIFICATION_SERVICE);
        Intent intent = new Intent(getApplication(), TrackingMapActivity.class);
        PendingIntent contentInent = PendingIntent.getActivity(getApplication(), 0, intent, PendingIntent.FLAG_IMMUTABLE);
        builder.setContentIntent(contentInent);
        Notification notification = null;
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.JELLY_BEAN) {
            notification = builder.build();
        }
        notification.flags |= Notification.FLAG_AUTO_CANCEL;
        notification.defaults |= Notification.DEFAULT_SOUND;

        manager.notify(new Random().nextInt(), notification);


    }



    public void sendNotifZone (String title ,String content)
    {
        Notification.Builder builder = new Notification.Builder(getApplication())
                .setSmallIcon(R.mipmap.ic_launcher_round)
                .setContentTitle(title)
                .setContentText(content);

        NotificationManager manager = (NotificationManager) getApplication().getSystemService(NOTIFICATION_SERVICE);
        Intent intent = new Intent(getApplication(), TrackingMapActivity.class);
        PendingIntent contentInent = PendingIntent.getActivity(getApplication(), 0, intent, PendingIntent.FLAG_IMMUTABLE);
        builder.setContentIntent(contentInent);
        Notification notification = null;
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.JELLY_BEAN) {
            notification = builder.build();
        }
        notification.flags |= Notification.FLAG_AUTO_CANCEL;
        notification.defaults |= Notification.DEFAULT_SOUND;

        manager.notify(new Random().nextInt(), notification);


    }
    }

