package com.example.ezzinemayssa.guardianfinale.adapter;

/**
 * Created by Shade on 5/9/2016.
 */

import android.content.Context;
import android.support.design.widget.Snackbar;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.example.ezzinemayssa.guardianfinale.R;
import com.example.ezzinemayssa.guardianfinale.entities.Child;
import com.example.ezzinemayssa.guardianfinale.entities.User;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.squareup.picasso.Callback;
import com.squareup.picasso.MemoryPolicy;
import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;




public class RecyclerAdapterChooseChildrenChat extends RecyclerView.Adapter<RecyclerAdapterChooseChildrenChat.ViewHolder> {
    public static String email_child_choosen_chat;
    private Child current;
    private  Context context;
    List<Child> listChooseChildren = new ArrayList<>();
    private LayoutInflater inflater;



    // create constructor to innitilize context and data sent from MainActivity
    public RecyclerAdapterChooseChildrenChat(Context context, List<Child> listChooseChildren){
        this.context=context;
        this.listChooseChildren=listChooseChildren;
        inflater= LayoutInflater.from(context);


    }

    class ViewHolder extends RecyclerView.ViewHolder{

        private ImageView itemImage;
        private TextView itemUsername;
        private ImageView itemStatus;
        ProgressBar progressBarImageProfile;
        public ImageView getItemStatus() {
            return itemStatus;
        }

        public void setItemStatus(ImageView itemStatus) {
            this.itemStatus = itemStatus;
        }

        private ViewHolder(View itemView) {
            super(itemView);
            itemImage =itemView.findViewById(R.id.item_image_chooseChild_chat);

            itemStatus = itemView.findViewById(R.id.statusChooseChildChat);
            progressBarImageProfile = itemView.findViewById(R.id.progressBarImageProfileChoose);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int position = getAdapterPosition();

                    email_child_choosen_chat=listChooseChildren.get(position).getEmail();



                }
            });
        }
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_choose_child_chat, viewGroup, false);
        ViewHolder viewHolder = new ViewHolder(v);




        return viewHolder;


    }


    @Override
    public void onBindViewHolder(ViewHolder viewHolder, int i ) {
        final ViewHolder myHolder= (ViewHolder) viewHolder;
        current=listChooseChildren.get(i);


        Picasso.with(context).load(current.getImage_name()).into(myHolder.itemImage, new Callback() {
            @Override
            public void onSuccess() {
                myHolder.progressBarImageProfile.setVisibility(View.GONE);
                myHolder.itemImage.setVisibility(View.VISIBLE);
                myHolder.itemStatus.setVisibility(View.VISIBLE);
                myHolder.itemStatus.setImageResource(android.R.drawable.presence_online);
                Log.e("listChooseChat",listChooseChildren.toString()+"");
            }

            @Override
            public void onError() {

            }
        });


    }


    @Override
    public int getItemCount() {
        return listChooseChildren.size();
    }


}