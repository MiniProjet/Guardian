package com.example.ezzinemayssa.guardianfinale.request;

import com.android.volley.Response;
import com.android.volley.toolbox.StringRequest;
import com.example.ezzinemayssa.guardianfinale.activity.MainActivity;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by mayss on 12/12/2017.
 */

public class AddLocationOfOneUser extends StringRequest {

    private static final String LOGIN_REQUEST_URL = "http://"+MainActivity.ipAdress+"/guardenScript/AddLocation.php";
    private Map<String, String> params;

    public AddLocationOfOneUser(String email, String uid , String lat, String lng , Response.Listener<String> listener) {
        super(Method.POST, LOGIN_REQUEST_URL, listener, null);
        params = new HashMap<>();
        params.put("email", email);
        params.put("uid", uid);
        params.put("lat", lat);
        params.put("lng", lng);

    }

    @Override
    public Map<String, String> getParams() {
        return params;
    }

}