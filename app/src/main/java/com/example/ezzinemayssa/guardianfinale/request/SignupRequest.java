package com.example.ezzinemayssa.guardianfinale.request;

import com.android.volley.Response;
import com.android.volley.toolbox.StringRequest;

import java.util.HashMap;
import java.util.Map;

import static com.example.ezzinemayssa.guardianfinale.activity.MainActivity.ipAdress;

/**
 * Created by Ezzine Mayssa on 13/11/2017.
 */

public class SignupRequest extends StringRequest {


        private static final String REGISTER_REQUEST_URL = "http://"+ipAdress+"/guardenScript/signup.php";
        private Map<String, String> params;

        public SignupRequest(String email, String password, String phone, Response.Listener<String> listener,Response.ErrorListener errorListener) {
            super(Method.POST, REGISTER_REQUEST_URL, listener, errorListener);
            params = new HashMap<>();
            params.put("password",password);
            params.put("email", email);
            params.put("phone", phone);

        }

        @Override
        public Map<String, String> getParams() {
            return params;
        }
    }

