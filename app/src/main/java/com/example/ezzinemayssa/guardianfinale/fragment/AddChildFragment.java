package com.example.ezzinemayssa.guardianfinale.fragment;


import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.app.FragmentTransaction;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.RequiresApi;
import android.support.v7.widget.LinearLayoutManager;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.toolbox.Volley;
import com.example.ezzinemayssa.guardianfinale.R;
import com.example.ezzinemayssa.guardianfinale.activity.MainActivity;
import com.example.ezzinemayssa.guardianfinale.activity.RegisterActivity;
import com.example.ezzinemayssa.guardianfinale.activity.map.DiagonalLayoutMainActivity;
import com.example.ezzinemayssa.guardianfinale.adapter.RecyclerAdapterDisplayChildren;
import com.example.ezzinemayssa.guardianfinale.entities.Child;
import com.example.ezzinemayssa.guardianfinale.request.AddChildRequest;
import com.example.ezzinemayssa.guardianfinale.utils.MySingleton;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.FirebaseApp;
import com.google.firebase.FirebaseOptions;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.zxing.BarcodeFormat;
import com.google.zxing.MultiFormatWriter;
import com.google.zxing.WriterException;
import com.google.zxing.common.BitMatrix;
import com.theartofdev.edmodo.cropper.CropImage;
import org.json.JSONException;
import org.json.JSONObject;
import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;
import java.util.Random;

import static android.app.Activity.RESULT_OK;
import static android.content.Context.MODE_PRIVATE;
import static com.example.ezzinemayssa.guardianfinale.fragment.DisplayChildFragment.adapterDisplayChild;
import static com.example.ezzinemayssa.guardianfinale.fragment.DisplayChildFragment.childrenListDisplayChild;
import static com.example.ezzinemayssa.guardianfinale.fragment.DisplayChildFragment.recyclerViewDisplayChild;


public  class  AddChildFragment extends DialogFragment {
    private FirebaseAuth firebaseAuth;
    private FirebaseAuth firebaseAuthChild;

    ImageView PictureProfileAddChild;
    String userConnected;
    final int  CODE_GALLERY_REQUEST=203;
    EditText phoneAddChild;
    EditText usernameAddChild;
    EditText birthdateAddChild;
    Calendar mycal= Calendar.getInstance();
    String code;
    String email;
    Bitmap bitmapProfil;
    ImageView ivQRCodeAddChild;
    TextView btnQRCodeAddChild;
    public final static int QRcodeWidth = 500 ;
    Bitmap bitmapQR ;
    ImageView btnAddChild;
    ProgressDialog progressDialog = null;

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View dialogView =inflater.inflate(R.layout.fragment_add_child, null);
        btnAddChild= dialogView.findViewById(R.id.btnAddChildDialog);

        SharedPreferences prefs = this.getActivity().getSharedPreferences("USER", MODE_PRIVATE);
        String restoredText = prefs.getString("userConnected", null);
        if (restoredText != null) {
            userConnected = prefs.getString("userConnected", "No name defined");//"No name defined" is the default value.

            Log.e("user",userConnected);
        }


        builder.setView(dialogView);
        btnAddChild.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                firebaseAuth= FirebaseAuth.getInstance();
                Log.e("username",usernameAddChild.getText().length()+"");
                Log.e("birth",birthdateAddChild.getText().toString()+"");
                Log.e("phone",phoneAddChild.getText().length()+"");
                if  (  ivQRCodeAddChild.getVisibility()!=View.VISIBLE){
                    Toast.makeText(getActivity(),"You must generate a QR",
                            Toast.LENGTH_LONG).show();}


                if((usernameAddChild.getText().length()>0)&&(birthdateAddChild.getText().toString().contains("/"))&&
                        (phoneAddChild.getText().length()==8)&&( ivQRCodeAddChild.getVisibility()==View.VISIBLE)) {

                    Log.e("username",usernameAddChild.getText().length()+"");
                    Log.e("birth",birthdateAddChild.getText().toString().toLowerCase());
                    Log.e("phone",phoneAddChild.getText().length()+"");
                    FirebaseOptions firebaseOptions = new FirebaseOptions.Builder()
                            .setDatabaseUrl("https://guardianfinale-a89bf.firebaseio.com")
                            .setApiKey("AIzaSyCJwnJOrzBCIkkW0_LTUSwyqdMIPefOlBs")
                            .setApplicationId("guardianfinale-a89bf").build();

                    FirebaseApp myApp = FirebaseApp.initializeApp(getActivity(),firebaseOptions,
                            code+"");

                    firebaseAuthChild= FirebaseAuth.getInstance(myApp);
                    addChildRequest();

                }


            }
        });



        btnQRCodeAddChild=dialogView.findViewById(R.id.btnQRCodeAddChild);
        ivQRCodeAddChild=dialogView.findViewById(R.id.ivQRcodeAddChild);

        PictureProfileAddChild= dialogView.findViewById(R.id.CVPictureProfileAddChild);

        phoneAddChild= dialogView.findViewById(R.id.addChildPhone);
        usernameAddChild= dialogView.findViewById(R.id.addChildUsername);
        birthdateAddChild= dialogView.findViewById(R.id.addChildBirthdate);
        if  ( !birthdateAddChild.getText().toString().contains("/")){
            birthdateAddChild.setError("you must pick a birthdate");}
        if (usernameAddChild.getText().length()==0){

            usernameAddChild.setError("username is empty");
        }
        if (birthdateAddChild.getText().toString().toLowerCase().startsWith("b")){

            birthdateAddChild.setError("you must choose a birthdate");
        }
        if (phoneAddChild.getText().length()!=8){

            phoneAddChild.setError("8 number are required");
        }
        usernameAddChild.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (usernameAddChild.getText().length()==0){

                    usernameAddChild.setError("username is empty");
                }
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (usernameAddChild.getText().length()==0){

                    usernameAddChild.setError("username is empty");
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {
                if (usernameAddChild.getText().length()==0){

                    usernameAddChild.setError("username is empty");
                }
            }
        });
        birthdateAddChild.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (birthdateAddChild.getText().toString().toLowerCase().startsWith("b")){

                    birthdateAddChild.setError("you must choose a birthdate");
                }
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (birthdateAddChild.getText().toString().toLowerCase().startsWith("b")){

                    birthdateAddChild.setError("you must choose a birthdate");
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {
                if (birthdateAddChild.getText().toString().toLowerCase().startsWith("b")){

                    birthdateAddChild.setError("you must choose a birthdate");
                }
            }
        });

        phoneAddChild.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (phoneAddChild.getText().length()!=8){

                    phoneAddChild.setError("8 number are required");
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
        birthdateAddChild.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new DatePickerDialog(getActivity(),date2,mycal.get(Calendar.YEAR),mycal.get(Calendar.MONTH),mycal.get(Calendar.DAY_OF_MONTH)).show();
            }
        });
        PictureProfileAddChild.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.M)
            @Override
            public void onClick(View view) {
                CropImage.activity()
                        .start(getActivity(),AddChildFragment.this);
                Intent intent = new Intent(Intent.ACTION_PICK);
                intent.setType("image/*");
                startActivityForResult(Intent.createChooser(intent,"Select Image"),CODE_GALLERY_REQUEST);
            }
        });

        btnQRCodeAddChild.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Random r = new Random();
                code = r.nextInt(999999999 - 999999 + 1) + 9999+"";
                email=code+"@guardian.com";
                try {
                    bitmapQR = TextToImageEncode(code);
                    ivQRCodeAddChild.setImageBitmap(bitmapQR);
                    ivQRCodeAddChild.setVisibility(View.VISIBLE);
                    btnQRCodeAddChild.setVisibility(View.INVISIBLE);

                } catch (WriterException e) {
                    e.printStackTrace();
                }
            }
        });
        return builder.create();
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(requestCode== CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE && resultCode == RESULT_OK && data!= null )
        {
            CropImage.ActivityResult result = CropImage.getActivityResult(data);
            Uri filePath= result.getUri();
            Context applicationContext = MainActivity.getContextOfApplication();
            try {
                InputStream inputStream= applicationContext.getContentResolver().openInputStream(filePath);
                bitmapProfil = BitmapFactory.decodeStream(inputStream);
                PictureProfileAddChild.setImageBitmap(bitmapProfil);

            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
        }
        super.onActivityResult(requestCode, resultCode, data);
    }
    public String getStringImage(Bitmap bmp){
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bmp.compress(Bitmap.CompressFormat.JPEG, 100, baos);
        byte[] imageBytes = baos.toByteArray();
        String encodedImage = Base64.encodeToString(imageBytes, Base64.DEFAULT);
        return encodedImage;
    }
    final DatePickerDialog.OnDateSetListener date2=new DatePickerDialog.OnDateSetListener() {
        @Override
        public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
            mycal.set(Calendar.YEAR,year);
            mycal.set(Calendar.MONTH,month);
            mycal.set(Calendar.DAY_OF_MONTH,dayOfMonth);
            String format="MM/dd/yy";
            SimpleDateFormat simpleDateFormat=new SimpleDateFormat(format, Locale.FRANCE);
            birthdateAddChild.setText(simpleDateFormat.format(mycal.getTime()));


        }
    };
    public void addChildRequest()
    {


        Response.Listener<String> responseListener = new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    JSONObject jsonResponse = new JSONObject(response);
                    String activated = jsonResponse.getString("addChild");


                    if (activated.equals("true")) {
                        addChildFirebase();


                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        };



    progressDialog = ProgressDialog.show(getActivity(), "Please wait..", "Processing", true);
    Child child = new Child();
    String image = getStringImage(((BitmapDrawable) PictureProfileAddChild.getDrawable()).getBitmap());
    String imageQR = getStringImage(bitmapQR);
    child.setCode(code);
    child.setEmail(email);
    child.setPhone(phoneAddChild.getText().toString());
    child.setUsername(usernameAddChild.getText().toString());
    child.setEmail_parent(userConnected);
    child.setImage_name(image);
    child.setImage_qr(imageQR);
    child.setBirthdate(birthdateAddChild.getText().toString());
    childrenListDisplayChild.add(child);
    adapterDisplayChild=new RecyclerAdapterDisplayChildren(getActivity(), childrenListDisplayChild);
    recyclerViewDisplayChild.setLayoutManager(new LinearLayoutManager(getActivity()));
    recyclerViewDisplayChild.setAdapter(adapterDisplayChild);
    adapterDisplayChild.notifyDataSetChanged();
    AddChildRequest addChildRequest = new AddChildRequest(child.getImage_name(), child.getImage_qr(), child.getCode(), child.getEmail(), child.getUsername(), child.getPhone(), child.getEmail_parent(), child.getBirthdate(), responseListener);
    MySingleton.getInstance(getActivity()).addToRequestQueue(addChildRequest);
}

    Bitmap TextToImageEncode(String Value) throws WriterException {
        BitMatrix bitMatrix;
        try {
            bitMatrix = new MultiFormatWriter().encode(
                    Value,
                    BarcodeFormat.DATA_MATRIX.QR_CODE,
                    QRcodeWidth, QRcodeWidth, null
            );

        } catch (IllegalArgumentException Illegalargumentexception) {

            return null;
        }
        int bitMatrixWidth = bitMatrix.getWidth();

        int bitMatrixHeight = bitMatrix.getHeight();

        int[] pixels = new int[bitMatrixWidth * bitMatrixHeight];

        for (int y = 0; y < bitMatrixHeight; y++) {
            int offset = y * bitMatrixWidth;

            for (int x = 0; x < bitMatrixWidth; x++) {

                pixels[offset + x] = bitMatrix.get(x, y) ?
                        getResources().getColor(R.color.black):getResources().getColor(R.color.white);
            }
        }
        Bitmap bitmap = Bitmap.createBitmap(bitMatrixWidth, bitMatrixHeight, Bitmap.Config.ARGB_4444);

        bitmap.setPixels(pixels, 0, 500, 0, 0, bitMatrixWidth, bitMatrixHeight);
        return bitmap;
    }
    void  addChildFirebase()
    {
        (firebaseAuthChild.createUserWithEmailAndPassword(email,code))
                .addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            progressDialog.dismiss();
                            firebaseAuthChild.signOut();
                            AddChildFragment.this.getDialog().cancel();
                        }
                        else {
                            progressDialog.dismiss();
                            Log.e("errorFirebase",task.getException().getMessage());
                            Toast.makeText(getActivity(), task.getException().getMessage(), Toast.LENGTH_SHORT).show();
                        }


                    }
                });
    }

}
