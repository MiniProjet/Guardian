package com.example.ezzinemayssa.guardianfinale.adapter;

import android.support.v7.widget.RecyclerView;
import android.text.format.DateUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.example.ezzinemayssa.guardianfinale.R;
import com.example.ezzinemayssa.guardianfinale.entities.ChatModel;
import com.example.ezzinemayssa.guardianfinale.utils.Util;
import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;


import hani.momanii.supernova_emoji_library.Helper.EmojiconTextView;

import static com.example.ezzinemayssa.guardianfinale.activity.MainActivity.EMAIL_PARENT_OF_CONNECTED_CHILD;
import static com.example.ezzinemayssa.guardianfinale.adapter.RecyclerAdapterChooseChildrenChat.email_child_choosen_chat;

/**
 * Created by Alessandro Barreto on 23/06/2016.
 */
public class ChatFirebaseAdapter extends FirebaseRecyclerAdapter<ChatModel,ChatFirebaseAdapter.MyChatViewHolder> {

    private static final int RIGHT_MSG = 0;
    private static final int LEFT_MSG = 1;
    private static final int RIGHT_MSG_IMG = 2;
    private static final int LEFT_MSG_IMG = 3;
    private static final int NO_MESSAGE = 4;

    private ClickListenerChatFirebase mClickListenerChatFirebase;

    private String nameUser;
    String usernameUser;
    String urlPhoto;
    String idFamily;


    public ChatFirebaseAdapter(DatabaseReference ref, String nameUser,String usernameUser,String urlPhoto,String idFamily,ClickListenerChatFirebase mClickListenerChatFirebase) {
        super(ChatModel.class, R.layout.item_message_left, ChatFirebaseAdapter.MyChatViewHolder.class, ref);
        this.nameUser = nameUser;
        this.usernameUser = usernameUser;
        this.urlPhoto = urlPhoto;
        this.idFamily = idFamily;

        this.mClickListenerChatFirebase = mClickListenerChatFirebase;



    }


    @Override
    public MyChatViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view;
        if (viewType == RIGHT_MSG){
            view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_message_right,parent,false);
            return new MyChatViewHolder(view);
        }else if (viewType == LEFT_MSG){
            view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_message_left,parent,false);
            return new MyChatViewHolder(view);
        }else if (viewType == RIGHT_MSG_IMG){
            view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_message_right_img,parent,false);
            return new MyChatViewHolder(view);
        }else if (viewType == LEFT_MSG_IMG){
            view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_message_left_img,parent,false);
            return new MyChatViewHolder(view);
        }
        else{
            view = LayoutInflater.from(parent.getContext()).inflate(R.layout.no_message,parent,false);
            return new MyChatViewHolder(view);
        }
    }

    @Override
    public int getItemViewType(int position) {
        ChatModel model = getItem(position);


        if(model.getUserModel().getIdFamilly().equals(EMAIL_PARENT_OF_CONNECTED_CHILD)) {
            if (model.getMapModel() != null) {
                if (model.getUserModel().getName().equals(nameUser)) {
                    return RIGHT_MSG_IMG;
                } else {
                    return LEFT_MSG_IMG;
                }
            }
            if (model.getFile() != null) {
                if (model.getFile().getType().equals("img") && model.getUserModel().getName().equals(nameUser)) {
                    return RIGHT_MSG_IMG;
                } else {
                    return LEFT_MSG_IMG;
                }
            }
            if (model.getUserModel().getName().equals(nameUser)) {

                return RIGHT_MSG;
            } else return LEFT_MSG;
        }
        return NO_MESSAGE;


    }

    @Override
    protected void populateViewHolder(MyChatViewHolder viewHolder, ChatModel model, int position) {
        viewHolder.setIvUser(model.getUserModel().getUrlPhoto());
        viewHolder.setTxtMessage(model.getMessage());
        viewHolder.setTvTimestamp(model.getTimeStamp());
        viewHolder.setTxtUsername(model.getUserModel().getUsername());
        viewHolder.tvIsLocation(View.GONE);
        if (model.getFile() != null){
            viewHolder.tvIsLocation(View.GONE);
            viewHolder.setIvChatPhoto(model.getFile().getUrl_file());
        }else if(model.getMapModel() != null){
            viewHolder.setIvChatPhoto(Util.local(model.getMapModel().getLatitude(),model.getMapModel().getLongitude()));
            viewHolder.tvIsLocation(View.VISIBLE);
        }
    }

    public class MyChatViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        TextView tvTimestamp,tvLocation,usernameMessageChat;
        EmojiconTextView txtMessage;
        ImageView ivUser,ivChatPhoto;

        public MyChatViewHolder(View itemView) {
            super(itemView);
            tvTimestamp =itemView.findViewById(R.id.timestamp);
            txtMessage = itemView.findViewById(R.id.txtMessage);
            tvLocation = itemView.findViewById(R.id.tvLocation);
            ivChatPhoto =itemView.findViewById(R.id.img_chat);
            ivUser = itemView.findViewById(R.id.ivUserChat);
            usernameMessageChat = itemView.findViewById(R.id.usernameMessageChat);
        }

        @Override
        public void onClick(View view) {
            int position = getAdapterPosition();
            ChatModel model = getItem(position);
            if (model.getMapModel() != null){
               // mClickListenerChatFirebase.clickImageMapChat(view,position,model.getMapModel().getLatitude(),model.getMapModel().getLongitude());
            }else{
               // mClickListenerChatFirebase.clickImageChat(view,position,model.getUserModel().getName(),model.getUserModel().getPhoto_profile(),model.getFile().getUrl_file());
            }
        }

        public void setTxtMessage(String message){
            if (txtMessage == null)return;
            txtMessage.setText(message);
        }
        public void setTxtUsername(String username){
            if (usernameMessageChat == null)return;
            usernameMessageChat.setText(username);
        }


        public void setIvUser(String urlPhotoUser){
            if (ivUser == null)return;
            Glide.with(ivUser.getContext()).load(urlPhotoUser).centerCrop().transform(new CircleTransform(ivUser.getContext())).override(40,40).into(ivUser);
        }

        public void setTvTimestamp(String timestamp){
            if (tvTimestamp == null)return;
            tvTimestamp.setText(converteTimestamp(timestamp));
        }

        public void setIvChatPhoto(String url){
            if (ivChatPhoto == null)return;
            Glide.with(ivChatPhoto.getContext()).load(url)
                    .override(100, 100)
                    .fitCenter()
                    .into(ivChatPhoto);
            ivChatPhoto.setOnClickListener(this);
        }

        public void tvIsLocation(int visible){
            if (tvLocation == null)return;
            tvLocation.setVisibility(visible);
        }

    }

    private CharSequence converteTimestamp(String mileSegundos){
        return DateUtils.getRelativeTimeSpanString(Long.parseLong(mileSegundos), System.currentTimeMillis(), DateUtils.SECOND_IN_MILLIS);
    }

}
