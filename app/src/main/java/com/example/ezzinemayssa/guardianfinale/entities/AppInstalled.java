package com.example.ezzinemayssa.guardianfinale.entities;

import android.graphics.drawable.Drawable;

/**
 * Created by mayss on 26/11/2017.
 */

public class AppInstalled {
    String nameApp;
    String imageApp;
    String email;
    public AppInstalled(String nameApp, String imageApp) {
        this.nameApp = nameApp;
        this.imageApp = imageApp;
    }

    public String getNameApp() {
        return nameApp;
    }

    public void setNameApp(String nameApp) {
        this.nameApp = nameApp;
    }

    public String getImageApp() {
        return imageApp;
    }

    public void setImageApp(String imageApp) {
        this.imageApp = imageApp;
    }

    @Override
    public String toString() {
        return "AppInstalled{" +
                "nameApp='" + nameApp + '\'' +
                ", imageApp=" + imageApp +
                '}';
    }
}
