package com.example.ezzinemayssa.guardianfinale.fragment;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TimePicker;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;
import com.example.ezzinemayssa.guardianfinale.R;
import com.example.ezzinemayssa.guardianfinale.activity.map.AppercuChildMap;
import com.example.ezzinemayssa.guardianfinale.activity.map.TrackingMapActivity;
import com.example.ezzinemayssa.guardianfinale.entities.Child;
import com.example.ezzinemayssa.guardianfinale.request.GetOneChildByEmailRequest;
import com.example.ezzinemayssa.guardianfinale.request.GetRefreshTokken;
import com.example.ezzinemayssa.guardianfinale.request.SendingNotifRequest;
import com.example.ezzinemayssa.guardianfinale.utils.MySingleton;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import static android.content.Context.MODE_PRIVATE;

public class PickMeUpDialog extends DialogFragment {
TimePicker time ;
    DatabaseReference pickMeUp;
    String tokken;
    FirebaseAuth firebaseAuth;
    RequestQueue queue;
    private Child childObj;
    private List<Child> childrenList ;
    String userConnected;
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        SharedPreferences prefs = this.getActivity().getSharedPreferences("USER", MODE_PRIVATE);
        String restoredText = prefs.getString("userConnected", null);
        if (restoredText != null) {
            userConnected = prefs.getString("userConnected", "No name defined");//"No name defined" is the default value.

            Log.e("user",userConnected);
        }


        firebaseAuth = FirebaseAuth.getInstance();
        pickMeUp= FirebaseDatabase.getInstance().getReference().child("pickMeUp");
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        // Get the layout inflater
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View dialogView =inflater.inflate(R.layout.dialog_time, null);
        time=(TimePicker) dialogView.findViewById(R.id.timePicker);
        // Inflate and set the layout for the dialog
        // Pass null as the parent view because its going in the dialog layout
        builder.setView(dialogView)
                // Add action buttons
                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {



                        Log.e("3","3");

                        Response.Listener<String> responseListener2 = new Response.Listener<String>() {
                            @Override
                            public void onResponse(String response) {

                                try {
                                    JSONObject jsonResponse = new JSONObject(response);
                                    Log.e("tokkenrefresh",response);
                                    JSONArray jsonArray= jsonResponse.getJSONArray("tokken");
                                    int count= 0;
                                    while(count<jsonArray.length())
                                    {
                                        JSONObject jo= jsonArray.getJSONObject(count);

                                        tokken= jo.getString("tokken");
                                        count++;




                                        getChildDetails(userConnected);
                                    }

                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }




                            }
                        };

                        GetRefreshTokken tokkenParent = new GetRefreshTokken(userConnected, responseListener2);
                        RequestQueue queue = MySingleton.getInstance(getActivity()).getRequestQueue();
// Add a request (in this example, called stringRequest) to your RequestQueue.
                        Log.e("etape1","etape1");
                        MySingleton.getInstance(getActivity()).addToRequestQueue(tokkenParent);











                    }
                })
                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        PickMeUpDialog.this.getDialog().cancel();
                    }
                });

        return builder.create();
    }


    public void getChildDetails(String email)
    {



        childrenList = new ArrayList<>();

        Response.Listener<String> responseListener = new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    JSONObject jsonObject= new JSONObject(response);
                    JSONArray jsonArray= jsonObject.getJSONArray("oneChild");
                    int count =0;


                    while(count< jsonArray.length())
                    {
                        JSONObject jo= jsonArray.getJSONObject(count);
                        childrenList.add(new Child(jo.getString("code"),jo.getString("email"),jo.getString("username")
                                ,jo.getString("phone"),jo.getString("image_name"),jo.getString("email_parent"),jo.getString("birthdate"),jo.getString("image_qr")));
                        count++;

                    }

                    childObj = childrenList.get(0);



                    Response.Listener<String> responseListener = new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {


                        }
                    };
                    final String timeString= time.getCurrentHour()+":"+time.getCurrentMinute();
                    Log.e("tokennnnnn",tokken+"");
                    SendingNotifRequest getChildrenByEmailParentRequest = new SendingNotifRequest(
                            "Pick Me Up",tokken,childObj.getUsername()+"i want to go home at "+timeString, responseListener);
                    RequestQueue queue = MySingleton.getInstance(getActivity()).getRequestQueue();
// Add a request (in this example, called stringRequest) to your RequestQueue.
                    MySingleton.getInstance(getActivity()).addToRequestQueue(getChildrenByEmailParentRequest);


                } catch (JSONException e) {

                    e.printStackTrace();
                }
            }
        };
        Response.ErrorListener responseListener5 = new Response.ErrorListener() {


            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("Tag", error.toString());


            }
        };
        GetOneChildByEmailRequest getOneChildByEmailRequest = new GetOneChildByEmailRequest(email, responseListener,responseListener5);
        MySingleton.getInstance(getActivity()).addToRequestQueue(getOneChildByEmailRequest);


    }


}


