package com.example.ezzinemayssa.guardianfinale.service;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Intent;
import android.os.Build;
import android.os.Handler;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.util.Log;

import com.example.ezzinemayssa.guardianfinale.R;
import com.example.ezzinemayssa.guardianfinale.activity.map.PickMeUpMapActivity;
import com.example.ezzinemayssa.guardianfinale.entities.PickMeUp;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Random;

/**
 * Created by mayss on 25/11/2017.
 */

public class PickMeChildResponse extends Service {
    DatabaseReference pickMeUpResponse;
    PickMeUp pickMe;
    Runnable runnable;
    Handler h;
    int delay = 15000;
    int missedResponse;

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }
    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        pickMe=new PickMeUp();
        Date currentTime = Calendar.getInstance().getTime();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd_HHmmss");
        String currentDateandTime = sdf.format(new Date());
        pickMeUpResponse= FirebaseDatabase.getInstance().getReference().child("pickMeUpResponse");
        Query childPickMe = pickMeUpResponse.orderByChild("nameChild").equalTo("309403586@guardian.com");
        childPickMe.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                for(DataSnapshot postSnapShot:dataSnapshot.getChildren())
                {

                     pickMe = postSnapShot.getValue(PickMeUp.class);
                    Log.e("pick",pickMe.toString());
                     sendNotification("Pick Me up","je viens à"+pickMe.getTime());

                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

        //we have some options for service
        //start sticky means service will be explicity started and stopped
        return START_STICKY;
    }
    public void sendNotification (String title ,String content)
    {
        Notification.Builder builder = new Notification.Builder(getApplication())
                .setSmallIcon(R.mipmap.ic_launcher_round)
                .setContentTitle(title)
                .setContentText(content);
        NotificationManager manager = (NotificationManager) getApplication().getSystemService(NOTIFICATION_SERVICE);
        Intent intent = new Intent(getApplication(),PickMeUpMapActivity.class);
        PendingIntent contentInent = PendingIntent.getActivity(getApplication(),0,intent,PendingIntent.FLAG_IMMUTABLE);
        builder.setContentIntent(contentInent);
        Notification notification = null;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
            notification = builder.build();
        }
        notification.flags|= Notification.FLAG_AUTO_CANCEL;
        notification.defaults|=Notification.DEFAULT_SOUND;

        manager.notify(new Random().nextInt(),notification);

    }

}
