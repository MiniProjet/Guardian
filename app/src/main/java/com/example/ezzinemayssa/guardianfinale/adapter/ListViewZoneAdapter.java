package com.example.ezzinemayssa.guardianfinale.adapter;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.ezzinemayssa.guardianfinale.R;
import com.example.ezzinemayssa.guardianfinale.entities.AppInstalled;
import com.example.ezzinemayssa.guardianfinale.entities.ZoneChild;
import com.squareup.picasso.Picasso;

import java.util.List;

/**
 * Created by mayss on 22/12/2017.
 */

public class ListViewZoneAdapter extends ArrayAdapter {
    Context context;
    int ressource;
    List<ZoneChild> list = null;
        public ListViewZoneAdapter(Context context, int resource, List<ZoneChild> list ) {
        super(context, resource, list);
        this.context=context;
        this.ressource=resource;
        this.list=list;

    }


    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View row = convertView;
        UserHolder holder = null;

        if(row == null)
        {
            LayoutInflater inflater = ((Activity)context).getLayoutInflater();
            row = inflater.inflate(ressource, parent, false);

            holder = new UserHolder();
            holder.txtDateDebut = (TextView)row.findViewById(R.id.txtDateDebut);
            holder.txtDateFin = (TextView)row.findViewById(R.id.txtDateFin);
            holder.txtLundi = (TextView)row.findViewById(R.id.txtlundi);
            holder.txtMardi = (TextView)row.findViewById(R.id.txtMardi);
            holder.txtMercredi = (TextView)row.findViewById(R.id.txtMercredi);
            holder.txtJeudi = (TextView)row.findViewById(R.id.txtJeudi);
            holder.txtVendredi = (TextView)row.findViewById(R.id.txtVendredi);
            holder.txtSamedi = (TextView)row.findViewById(R.id.txtSamedi);
            holder.txtDimanche = (TextView)row.findViewById(R.id.txtDiamanche);
            holder.image = (ImageView)row.findViewById(R.id.image);
            row.setTag(holder);
        }
        else
        {
            holder = (UserHolder)row.getTag();
        }
        ZoneChild zone = list.get(position);
        holder.txtDateDebut.setText(zone.getDateDeubt());
        holder.txtDateFin.setText(zone.getDateFin());
        if(zone.getJour().equals("oui")){
            holder.txtLundi.setTextColor((Color.parseColor("#008000")));
            holder.txtLundi.setTypeface(null, Typeface.BOLD);
        }
        if(zone.getMardi().equals("oui")){
            holder.txtMardi.setTextColor((Color.parseColor("#008000")));
            holder.txtMardi.setTypeface(null, Typeface.BOLD);
        }
        if(zone.getMercredi().equals("oui")){
            holder.txtMercredi.setTextColor((Color.parseColor("#008000")));
            holder.txtMercredi.setTypeface(null, Typeface.BOLD);
        }
        if(zone.getJeudi().equals("oui")){
            holder.txtJeudi.setTextColor((Color.parseColor("#008000")));
            holder.txtJeudi.setTypeface(null, Typeface.BOLD);

        }
        if(zone.getVendredi().equals("oui")){
            holder.txtVendredi.setTextColor((Color.parseColor("#008000")));
            holder.txtVendredi.setTypeface(null, Typeface.BOLD);
        }
        if(zone.getSamedi().equals("oui")){
            holder.txtSamedi.setTextColor((Color.parseColor("#008000")));
            holder.txtSamedi.setTypeface(null, Typeface.BOLD);
        }
        if(zone.getDimanche().equals("oui")){
            holder.txtDimanche.setTextColor((Color.parseColor("#008000")));
            holder.txtDimanche.setTypeface(null, Typeface.BOLD);
        }
        return row;

    }

    static class UserHolder
    {

        TextView txtDateDebut;
        TextView txtDateFin;
        TextView txtLundi;
        TextView txtMardi;
        TextView txtMercredi;
        TextView txtJeudi;
        TextView txtVendredi;
        TextView txtSamedi;
        TextView txtDimanche;

        ImageView image;
    }
}

