package com.example.ezzinemayssa.guardianfinale.activity.map;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.SeekBar;
import android.widget.TimePicker;
import android.widget.Toast;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;
import com.example.ezzinemayssa.guardianfinale.R;
import com.example.ezzinemayssa.guardianfinale.adapter.RecyclerAdapterChooseChildren;
import com.example.ezzinemayssa.guardianfinale.request.AddZoneRequest;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.PolygonOptions;

import org.json.JSONException;
import org.json.JSONObject;

public class CreationZoneMap extends FragmentActivity implements OnMapReadyCallback {

    private GoogleMap mMap;
    EditText edit1;
    EditText edit2;
    SeekBar seekBar;
    TimePicker time;
    LatLng lat00;
    Intent iin;
    Bundle b;
    LatLng lat01;
    LatLng lat10;
    LatLng lat11;
    double coef;
    LatLng latLngOfPolygone;
    Button btnLundi,btnMardi,btnMercredi,btnJeudi,btnVendredi,btnSamedi,btnDimanche,btnvalider;
    String lundi="non",mardi="non",mercredi="non",jeudi="non",vendredi="non",samedi="non",dimanche="non";
    boolean lundiSelectd,mardiSelected,mercrediSelected,jeudiSelected,vendrediSelected,samediSelected,dimancheSelected=false;
    ProgressDialog progress;
    String hourDebut;
    CheckBox repeat;

    String hourFin ;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_creation_zone_map);
        progress = new ProgressDialog(CreationZoneMap.this);
        progress.setTitle("Please wait..");
        progress.setMessage("Processing   ");
        progress.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progress.setIndeterminate(true);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            getWindow().setStatusBarColor(Color.LTGRAY);
        }
        edit1=(EditText)findViewById(R.id.editText);
        edit2=(EditText)findViewById(R.id.editText2);
        repeat=(CheckBox)findViewById(R.id.checkBox);
        iin= getIntent();
        b = iin.getBundleExtra("bundle");
        seekBar =(SeekBar) findViewById(R.id.seekBarZone);
        btnLundi=(Button)findViewById(R.id.button7);
        btnMardi=(Button)findViewById(R.id.button8);
        btnMercredi =(Button)findViewById(R.id.button9);
        btnJeudi=(Button)findViewById(R.id.btn_home_calls);
        btnVendredi =(Button)findViewById(R.id.btn_home_sms);
        btnSamedi=(Button)findViewById(R.id.btn_home_chat);
        btnDimanche =(Button)findViewById(R.id.btn_home_children);
        btnvalider=(Button)findViewById(R.id.button3);
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        if(b!=null)
        {
            String dateDebut =(String) b.get("DateDebut");
            edit1.setText(dateDebut);
            Log.e("ddd",dateDebut);
            edit2.setText((String) b.get("DateFin"));

            if(b.getString("jour").equals("oui")){
                lundiSelectd=true;
                btnLundi.setBackgroundColor(Color.GRAY);
            }
            if(b.getString("mardi").equals("oui")){
                mardiSelected=true;
                btnMardi.setBackgroundColor(Color.GRAY);
            }
            if(b.getString("mercredi").equals("oui")){
                mercrediSelected=true;
                btnMercredi.setBackgroundColor(Color.GRAY);
            }
            if(b.getString("jeudi").equals("oui")){
                jeudiSelected=true;
                btnJeudi.setBackgroundColor(Color.GRAY);
            }
            if(b.getString("vendredi").equals("oui")){
                vendrediSelected=true;
                btnVendredi.setBackgroundColor(Color.GRAY);
            }
            if(b.getString("samedi").equals("oui")){
                samediSelected=true;
                btnSamedi.setBackgroundColor(Color.GRAY);
            }
            if(b.getString("dimanche").equals("oui")){
                dimancheSelected=true;
                btnDimanche.setBackgroundColor(Color.GRAY);
                Log.e("color","wiiiw222");

            }


        }

        ///////////////////////////////////////////////////////////////////
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
        edit1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.e("click","click");
                AlertDialog.Builder dialog = new AlertDialog.Builder(
                        CreationZoneMap.this);
                final  View pickers=getLayoutInflater().inflate(R.layout.dialog_time,
                        null);
                dialog.setView(pickers);
                dialog.setPositiveButton("ok",
                        new DialogInterface.OnClickListener() {

                            @Override
                            public void onClick(DialogInterface dialog,
                                                int which) {
                                // TODO Auto-generated method stub
                                time=(TimePicker)pickers.findViewById(R.id.timePicker);
                                edit1.setText(time.getCurrentHour()+":"+time.getCurrentMinute());
                                //  edit1.setText();
                            }
                        });
                dialog.setNegativeButton("cancel",
                        new DialogInterface.OnClickListener() {

                            @Override
                            public void onClick(DialogInterface dialog,
                                                int which) {
                                // TODO Auto-generated method stub
                                dialog.dismiss();
                            }
                        });


                dialog.show();

            }


        });



        edit2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.e("click","click");
                AlertDialog.Builder dialog = new AlertDialog.Builder(
                        CreationZoneMap.this);
                AlertDialog dlg = dialog.create();

                final View pickers=getLayoutInflater().inflate(R.layout.dialog_time,
                        null);
                dlg.setView(pickers);


                dlg.setButton(Dialog.BUTTON_POSITIVE, "Ok",
                        new DialogInterface.OnClickListener() {

                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                // TODO Auto-generated method stub
                                time=(TimePicker)pickers.findViewById(R.id.timePicker);
                                edit2.setText(time.getCurrentHour()+":"+time.getCurrentMinute());
                            }


                        });

                dlg.setButton(Dialog.BUTTON_NEGATIVE, "Cancel",
                        new DialogInterface.OnClickListener() {

                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                // TODO Auto-generated method stub


                            }
                        });


                dlg.show();

            }


        });

        btnvalider.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                 hourDebut=edit2.getText().toString();


                 hourFin =edit1.getText().toString();//timePickerFin.getCurrentHour()+"";

                final Response.Listener<String> responseListener = new Response.Listener<String>() {

                    @Override
                    public void onResponse(String response) {
progress.dismiss();

                        try {

                            JSONObject jsonResponse = new JSONObject(response);
                            // boolean success = jsonResponse.getBoolean("success");

                            String activated = jsonResponse.getString("add");

                            if (activated.equals("true")) {

                                Log.e("updateAjout","true");

                            } else {
                                Log.e("updateAjout","false");
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }



                    }

                    Response.ErrorListener responseListener5 = new Response.ErrorListener() {


                        @Override
                        public void onErrorResponse(VolleyError error) {
                            Log.e("Tag", error.toString());
                            progress.dismiss();


                        }
                    };
                };if(hourDebut.startsWith("D")&&hourFin.startsWith("D")){

                    Toast.makeText(getApplication(),"Vous n'avez pas defini la marge horaire",
                            Toast.LENGTH_LONG).show();
                }
                else{
                    if((lundiSelectd||mardiSelected||mercrediSelected||jeudiSelected||vendrediSelected||samediSelected||dimancheSelected)==false)
                    { Toast.makeText(getApplication(),"Vous devez selectionner minimum un jour",
                            Toast.LENGTH_LONG).show();}
                    else{
                        if(lat00==null){
                            Toast.makeText(getApplication(),"Vous n'avez pas selectionnez une zone",
                                    Toast.LENGTH_LONG).show();
                        }else{

                            AlertDialog.Builder builder = new AlertDialog.Builder(CreationZoneMap.this);
                            builder.setTitle("Confirm to Add Zone ");
                            builder.setMessage("You are about to add a zone . Do you really want to proceed ?");
                            builder.setCancelable(false);
                            builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {

                                    progress.show();


                                    AddZoneRequest addlocation = new AddZoneRequest(RecyclerAdapterChooseChildren.email_child_chosen,lat00.latitude+"",lat00.longitude+""
                                            ,lat01.latitude+"",lat01.longitude+"",lat10.latitude+"",lat10.longitude+"",lat11.latitude+"",lat11.longitude+"",
                                            lundi,mardi,mercredi,jeudi,vendredi,samedi,dimanche,hourDebut,hourFin,responseListener);
                                    RequestQueue queue = Volley.newRequestQueue(getApplicationContext());
                                    queue.add(addlocation);
                                    Log.e("wiiiiw","ssss");
                                }
                            });

                            builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.cancel();
                                }
                            });

                            builder.show();

                        }}

                }}

        });


        btnLundi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(lundiSelectd==false){
                    lundiSelectd=true;
                    lundi="oui";
                    btnLundi.setBackgroundColor(Color.GRAY);
                }
                else{
                    lundiSelectd=false;
                    btnLundi.setBackgroundColor((Color.parseColor("#008000")));
                    lundi="non";
                }


            }
        });
        btnMardi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(mardiSelected==false){
                    mardiSelected=true;
                    mardi="oui";
                    btnMardi.setBackgroundColor(Color.GRAY);
                }
                else{
                    mardiSelected=false;
                    btnMardi.setBackgroundColor((Color.parseColor("#008000")));
                    mardi="non";
                }
            }
        });
        btnMercredi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(mercrediSelected==false){
                    mercrediSelected=true;
                    mercredi="oui";
                    btnMercredi.setBackgroundColor(Color.GRAY);
                }
                else{
                    mercrediSelected=false;
                    btnMercredi.setBackgroundColor((Color.parseColor("#008000")));
                    mercredi="non";
                }
            }
        });
        btnJeudi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(jeudiSelected==false){
                    jeudiSelected=true;
                    jeudi="oui";
                    btnJeudi.setBackgroundColor(Color.GRAY);
                }
                else{
                    jeudiSelected=false;
                    btnJeudi.setBackgroundColor((Color.parseColor("#008000")));
                    jeudi="non";
                }
            }
        });
        btnVendredi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(vendrediSelected==false){
                    vendrediSelected=true;
                    vendredi="oui";
                    btnVendredi.setBackgroundColor(Color.GRAY);
                }
                else{
                    vendrediSelected=false;
                    btnVendredi.setBackgroundColor((Color.parseColor("#008000")));
                    vendredi="non";
                }
            }
        });
        btnSamedi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(samediSelected==false){
                    samediSelected=true;
                    samedi="oui";
                    btnSamedi.setBackgroundColor(Color.GRAY);
                }
                else{
                    samediSelected=false;
                    btnSamedi.setBackgroundColor(Color.rgb(00,80,00));
                    samedi="non";
                }
            }
        });
        btnDimanche.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if(dimancheSelected==false){
                    dimancheSelected=true;
                    dimanche="oui";
                    btnDimanche.setBackgroundColor(Color.GRAY);
                }
                else{
                    dimancheSelected=false;
                    btnDimanche.setBackgroundColor((Color.parseColor("#008000")));
                    dimanche="non";
                }
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        if(repeat.isChecked()){
            dimancheSelected=true;
            dimanche="oui";
            btnDimanche.setBackgroundColor(Color.GRAY);

            samediSelected=true;
            samedi="oui";
            btnSamedi.setBackgroundColor(Color.GRAY);

            vendrediSelected=true;
            vendredi="oui";
            btnVendredi.setBackgroundColor(Color.GRAY);

            jeudiSelected=true;
            jeudi="oui";
            btnJeudi.setBackgroundColor(Color.GRAY);

            mercrediSelected=true;
            mercredi="oui";
            btnMercredi.setBackgroundColor(Color.GRAY);

            mardiSelected=true;
            mardi="oui";
            btnMardi.setBackgroundColor(Color.GRAY);

            lundiSelectd=true;
            lundi="oui";
            btnLundi.setBackgroundColor(Color.GRAY);
        }

        else{
            dimanche="non";
            dimancheSelected=false;
            btnDimanche.setBackgroundColor((Color.parseColor("#008000")));

            samediSelected=false;
            btnSamedi.setBackgroundColor((Color.parseColor("#008000")));
            samedi="non";

            vendrediSelected=false;
            btnVendredi.setBackgroundColor((Color.parseColor("#008000")));
            vendredi="non";

            jeudiSelected=false;
            btnJeudi.setBackgroundColor((Color.parseColor("#008000")));
            jeudi="non";

            mercrediSelected=false;
            btnMercredi.setBackgroundColor((Color.parseColor("#008000")));
            mercredi="non";

            mardiSelected=false;
            btnMardi.setBackgroundColor((Color.parseColor("#008000")));
            mardi="non";

            lundiSelectd=false;
            btnLundi.setBackgroundColor((Color.parseColor("#008000")));
            lundi="non";
        }
        repeat.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if(repeat.isChecked()){
                    dimancheSelected=true;
                    dimanche="oui";
                    btnDimanche.setBackgroundColor(Color.GRAY);

                    samediSelected=true;
                    samedi="oui";
                    btnSamedi.setBackgroundColor(Color.GRAY);

                    vendrediSelected=true;
                    vendredi="oui";
                    btnVendredi.setBackgroundColor(Color.GRAY);

                    jeudiSelected=true;
                    jeudi="oui";
                    btnJeudi.setBackgroundColor(Color.GRAY);

                    mercrediSelected=true;
                    mercredi="oui";
                    btnMercredi.setBackgroundColor(Color.GRAY);

                    mardiSelected=true;
                    mardi="oui";
                    btnMardi.setBackgroundColor(Color.GRAY);

                    lundiSelectd=true;
                    lundi="oui";
                    btnLundi.setBackgroundColor(Color.GRAY);
                }

                else{
                    dimanche="non";
                    dimancheSelected=false;
                    btnDimanche.setBackgroundColor((Color.parseColor("#008000")));

                    samediSelected=false;
                    btnSamedi.setBackgroundColor((Color.parseColor("#008000")));
                    samedi="non";

                    vendrediSelected=false;
                    btnVendredi.setBackgroundColor((Color.parseColor("#008000")));
                    vendredi="non";

                    jeudiSelected=false;
                    btnJeudi.setBackgroundColor((Color.parseColor("#008000")));
                    jeudi="non";

                    mercrediSelected=false;
                    btnMercredi.setBackgroundColor((Color.parseColor("#008000")));
                    mercredi="non";

                    mardiSelected=false;
                    btnMardi.setBackgroundColor((Color.parseColor("#008000")));
                    mardi="non";

                    lundiSelectd=false;
                    btnLundi.setBackgroundColor((Color.parseColor("#008000")));
                    lundi="non";
                }
            }
        });

    }

    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        LatLng tunis = new LatLng(35, 10);

        CameraPosition cameraPosition = new CameraPosition.Builder()
                .target(tunis).zoom(15.0f).build();
        CameraUpdate cameraUpdate = CameraUpdateFactory.newCameraPosition(cameraPosition);
        mMap.moveCamera(cameraUpdate);

        seekBar.setVisibility(View.INVISIBLE);

        if(b!=null){
            CameraPosition cameraPosition2 = new CameraPosition.Builder()
                    .target(new LatLng(Double.parseDouble(b.getString("lng00")), Double.parseDouble(b.getString("lat00")))).zoom(15.0f).build();
            CameraUpdate cameraUpdate2 = CameraUpdateFactory.newCameraPosition(cameraPosition2);
            mMap.moveCamera(cameraUpdate2);

            mMap.addPolygon(new PolygonOptions().add(
                    lat00=    new LatLng(Double.parseDouble(b.getString("lng00")), Double.parseDouble(b.getString("lat00")))
                    ,lat01=new LatLng(Double.parseDouble(b.getString("lng01")), Double.parseDouble(b.getString("lat01")))
                    ,lat10 =new LatLng(Double.parseDouble(b.getString("lng10")), Double.parseDouble(b.getString("lat10")))
                    ,lat11=new LatLng(Double.parseDouble(b.getString("lng11")), Double.parseDouble(b.getString("lat11"))))
                    .strokeColor(Color.BLUE).fillColor(0x220000ff).strokeWidth(5.0f));

        }
        mMap.setOnMapClickListener(new GoogleMap.OnMapClickListener() {

            @Override
            public void onMapClick(LatLng latLng) {
                latLngOfPolygone=latLng;

                mMap.clear();

                seekBar.setVisibility(View.VISIBLE);
                double meters = 50;
                double coef = meters * 0.0000089;
                //  double new_lat = my_lat + coef;
                //double new_long = my_long + coef / Math.cos(latLng.longitude * 0.018);

                mMap.addPolygon(new PolygonOptions().add(
                        lat00=    new LatLng(latLng.latitude-coef,latLng.longitude-coef / Math.cos(latLng.longitude * 0.018))
                        ,lat01=new LatLng(latLng.latitude-coef,latLng.longitude+coef / Math.cos(latLng.longitude * 0.018))
                        ,lat10 =new LatLng(latLng.latitude+coef,latLng.longitude+coef / Math.cos(latLng.longitude * 0.018))
                        ,lat11=new LatLng(latLng.latitude+coef,latLng.longitude-coef / Math.cos(latLng.longitude * 0.018)))
                        .strokeColor(Color.BLUE).fillColor(0x220000ff).strokeWidth(5.0f));



            }
        });


        seekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
                double meters = i;
                coef = meters * 0.0000089;
                //  double new_lat = my_lat + coef;
                //double new_long = my_long + coef / Math.cos(latLng.longitude * 0.018);
                mMap.clear();
                mMap.addPolygon(new PolygonOptions().add(
                        lat00=  new LatLng(latLngOfPolygone.latitude-coef,latLngOfPolygone.longitude-coef / Math.cos(latLngOfPolygone.longitude * 0.018))
                        , lat01=new LatLng(latLngOfPolygone.latitude-coef,latLngOfPolygone.longitude+coef / Math.cos(latLngOfPolygone.longitude * 0.018))
                        , lat11=new LatLng(latLngOfPolygone.latitude+coef,latLngOfPolygone.longitude+coef / Math.cos(latLngOfPolygone.longitude * 0.018))
                        , lat10=new LatLng(latLngOfPolygone.latitude+coef,latLngOfPolygone.longitude-coef / Math.cos(latLngOfPolygone.longitude * 0.018)))
                        .strokeColor(Color.BLUE).fillColor(0x220000ff).strokeWidth(5.0f));


            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });
    }
}

