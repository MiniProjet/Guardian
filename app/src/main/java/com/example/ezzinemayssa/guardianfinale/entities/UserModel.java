package com.example.ezzinemayssa.guardianfinale.entities;

import com.google.firebase.database.Exclude;

/**
 * Created by Alessandro Barreto on 22/06/2016.
 */
public class UserModel {

    private String id;
    private String name;
    private String username;
    private String urlPhoto;
    private String idFamilly;


    public UserModel() {
    }

    public UserModel(String id, String name, String username,String urlPhoto) {
        this.id = id;
        this.name = name;
        this.username = username;
        this.urlPhoto = urlPhoto;
    }

    public UserModel(String id, String name, String username, String urlPhoto, String idFamilly) {
        this.id = id;
        this.name = name;
        this.username = username;
        this.urlPhoto = urlPhoto;
        this.idFamilly = idFamilly;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    @Exclude
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUrlPhoto() {
        return urlPhoto;
    }

    public void setUrlPhoto(String urlPhoto) {
        this.urlPhoto = urlPhoto;
    }

    public String getIdFamilly() {
        return idFamilly;
    }

    public void setIdFamilly(String idFamilly) {
        this.idFamilly = idFamilly;
    }

    @Override
    public String toString() {
        return "UserModel{" +
                "id='" + id + '\'' +
                ", name='" + name + '\'' +
                ", username='" + username + '\'' +
                '}';
    }
}
