package com.example.ezzinemayssa.guardianfinale.receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.util.Base64;
import android.util.Log;
import android.widget.Toast;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.toolbox.Volley;
import com.example.ezzinemayssa.guardianfinale.request.AddAppRequest;
import com.example.ezzinemayssa.guardianfinale.request.DeleteAppRequest;
import com.example.ezzinemayssa.guardianfinale.utils.MySingleton;
import com.google.firebase.auth.FirebaseAuth;

import java.io.ByteArrayOutputStream;
import java.util.HashMap;
import java.util.List;

/**
 * Created by mayss on 13/12/2017.
 */

public class AppInstalledReciver extends BroadcastReceiver {
    RequestQueue queue;
    FirebaseAuth firebaseAuth;
    @Override
    public void onReceive(final Context context, Intent intent) {

        firebaseAuth = FirebaseAuth.getInstance();
        List<PackageInfo> packs = context.getPackageManager().getInstalledPackages(0);
         HashMap<String, String> map;
        for (int i = 0; i < packs.size(); i++) {
            final PackageInfo p = packs.get(i);

            if ((isSystemPackage(p) == false)) {
                Response.Listener<String> responseListener5 = new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.e("delete",response);
                        Response.Listener<String> responseListener3 = new Response.Listener<String>() {
                            @Override
                            public void onResponse(String response) {



                            }
                        };
                        Log.e("app",p.applicationInfo.loadLabel(context.getPackageManager()).toString());
                        AddAppRequest addlocation = new AddAppRequest(firebaseAuth.getCurrentUser().getEmail(),p.applicationInfo.loadLabel(context.getPackageManager()).toString().replaceAll("\\s",""),
                                getStringImage(drawableToBitmap( p.applicationInfo.loadIcon(context.getPackageManager()))),responseListener3);
                        RequestQueue queue = MySingleton.getInstance(context).getRequestQueue();
// Add a request (in this example, called stringRequest) to your RequestQueue.
                        MySingleton.getInstance(context).addToRequestQueue(addlocation);

                    }
                };
                Log.e("app",p.applicationInfo.loadLabel(context.getPackageManager()).toString());
                DeleteAppRequest delete = new DeleteAppRequest(firebaseAuth.getCurrentUser().getEmail(),responseListener5);
                RequestQueue queue = MySingleton.getInstance(context).getRequestQueue();
// Add a request (in this example, called stringRequest) to your RequestQueue.
                MySingleton.getInstance(context).addToRequestQueue(delete);


            }
        }



    }



    private boolean isSystemPackage(PackageInfo pInfo) {
        return ((pInfo.applicationInfo.flags & ApplicationInfo.FLAG_SYSTEM) != 0) ? true : false;
    }
    public static Bitmap drawableToBitmap (Drawable drawable) {
        if (drawable instanceof BitmapDrawable) {
            return ((BitmapDrawable)drawable).getBitmap();
        }

        int width = drawable.getIntrinsicWidth();
        width = width > 0 ? width : 1;
        int height = drawable.getIntrinsicHeight();
        height = height > 0 ? height : 1;

        Bitmap bitmap = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(bitmap);
        drawable.setBounds(0, 0, canvas.getWidth(), canvas.getHeight());
        drawable.draw(canvas);

        return bitmap;
    }
    public String getStringImage(Bitmap bmp){
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bmp.compress(Bitmap.CompressFormat.PNG, 100, baos);
        byte[] imageBytes = baos.toByteArray();
        String encodedImage = Base64.encodeToString(imageBytes, Base64.DEFAULT);
        return encodedImage;
    }
}
