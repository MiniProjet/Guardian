package com.example.ezzinemayssa.guardianfinale.adapter;

import android.app.Activity;
import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.example.ezzinemayssa.guardianfinale.R;
import com.example.ezzinemayssa.guardianfinale.entities.AppInstalled;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import java.util.List;

/**
 * Created by mayss on 26/11/2017.
 */



public class AppInstalledAdapter extends ArrayAdapter {
    Context context;
    int ressource;
    List<AppInstalled> list = null;
    public AppInstalledAdapter(Context context, int resource, List<AppInstalled> list ) {
        super(context, resource, list);
        this.context=context;
        this.ressource=resource;
        this.list=list;

    }


    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View row = convertView;
        UserHolder holder = null;

        if(row == null)
        {
            LayoutInflater inflater = ((Activity)context).getLayoutInflater();
            row = inflater.inflate(ressource, parent, false);

            holder = new UserHolder();
            holder.progressBar=(ProgressBar)row.findViewById(R.id.progress);
            holder.txtName = (TextView)row.findViewById(R.id.textView2);
            holder.image = (ImageView)row.findViewById(R.id.image);
            row.setTag(holder);
        }
        else
        {
            holder = (UserHolder)row.getTag();
        }
        AppInstalled app = list.get(position);
        holder.txtName.setText(app.getNameApp());
        final UserHolder finalHolder = holder;
        Picasso.with(context).load(app.getImageApp()).into(holder.image, new Callback() {
            @Override
            public void onSuccess() {
                finalHolder.progressBar.setVisibility(View.GONE);
                finalHolder.image.setVisibility(View.VISIBLE);
            }

            @Override
            public void onError() {

            }
        });
        Log.e("imagess",app.getImageApp());

        return row;

    }

    static class UserHolder
    {
        ProgressBar progressBar ;
        TextView txtName;
        ImageView image;
    }
}

