package com.example.ezzinemayssa.guardianfinale.request;

import com.android.volley.Response;
import com.android.volley.toolbox.StringRequest;
import com.example.ezzinemayssa.guardianfinale.activity.MainActivity;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by mayss on 13/12/2017.
 */

public class GetAppInstalled extends StringRequest {


    private static final String REGISTER_REQUEST_URL = "http://"+ MainActivity.ipAdress+"/guardenScript/GetListApp.php";
    private Map<String, String> params;

    public GetAppInstalled(String email_child, Response.Listener<String> listener, Response.ErrorListener eroor) {
        super(Method.POST, REGISTER_REQUEST_URL,listener,eroor);
        params = new HashMap<>();
        params.put("email",email_child);



    }

    @Override
    public Map<String, String> getParams() {
        return params;
    }
}
