package com.example.ezzinemayssa.guardianfinale.activity;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.util.SparseArray;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.toolbox.Volley;
import com.daimajia.androidanimations.library.Techniques;
import com.daimajia.androidanimations.library.YoYo;
import com.example.ezzinemayssa.guardianfinale.R;
import com.example.ezzinemayssa.guardianfinale.request.RefreshTokkenRequest;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.gms.vision.CameraSource;
import com.google.android.gms.vision.Detector;
import com.google.android.gms.vision.barcode.Barcode;
import com.google.android.gms.vision.barcode.BarcodeDetector;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.GoogleAuthProvider;

import java.io.IOException;

public class LoginActivity extends AppCompatActivity {
    public static String PREFERENCE_FILENAME = "reporting_app";
    String resultQRSignIN;
    TextView tvGuardian;
    Button btnSignin;
    int i = 0;
    ImageButton ibParent;
    ImageButton ibChild;
    EditText emailSignin;
    EditText passwordSignin;
    TextView forgotPsw;
    TextView status;
    TextView tvDonthaveaccount;
    TextView tvSignup;

    TextInputLayout emailSigninLayout;
    TextInputLayout passwordSigninLayout;
    private FirebaseAuth firebaseAuth;
    private FirebaseAuth.AuthStateListener firebaseAuthListener;
    private GoogleApiClient mGoogleApiClient;
    private static final int RC_SIGN_IN = 1;
    SurfaceView surfaceQR;
    TextView resultQR;
    BarcodeDetector barcodeDetector;
    CameraSource cameraSource;

    final int RequestCameraPermissionID = 1001;
    String QrDetected = "false";
    public TextView tvClickHere;

    @Override
    protected void onStart() {
        super.onStart();
        firebaseAuth.addAuthStateListener(firebaseAuthListener);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        SharedPreferences reportingPref = getSharedPreferences(PREFERENCE_FILENAME, MODE_PRIVATE);
        if ((!reportingPref.getString("onBoarder", "").equals("true"))) {
            Intent intent = new Intent(LoginActivity.this, OnboarderActivity.class);
            startActivity(intent);
        }
        //Declaration
        tvGuardian = findViewById(R.id.tvGuardian);
        btnSignin = findViewById(R.id.btnSignin);
        ibParent = findViewById(R.id.btnParent);
        ibChild = findViewById(R.id.btnChild);
        emailSignin = findViewById(R.id.emailSignin);
        passwordSignin = findViewById(R.id.passwordSignin);
        forgotPsw = findViewById(R.id.tvForgotPassword);

        emailSigninLayout = findViewById(R.id.emailSigninLayout);
        passwordSigninLayout = findViewById(R.id.PasswordSigninLayout);
        status = findViewById(R.id.tvStatus);
        tvDonthaveaccount = findViewById(R.id.tvDonthaveaccount);
        tvSignup = findViewById(R.id.tvSignup);
        surfaceQR = findViewById(R.id.surfaceQR);
        resultQR = findViewById(R.id.resultQR);

        tvClickHere = findViewById(R.id.tvClickHere);


        emailSignin.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                if (emailSignin.getText().length() == 0) {
                    emailSignin.setError("Email is empty");
                }

            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        passwordSignin.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                if (passwordSignin.getText().length() == 0) {
                    passwordSignin.setError("Password is empty");
                }
                if (passwordSignin.getText().length() < 6) {
                    passwordSignin.setError("6 characters or more");
                }

            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });


        firebaseAuth = FirebaseAuth.getInstance();
        //OnStateChangedGoogleAuth
        firebaseAuthListener = new FirebaseAuth.AuthStateListener() {

            @Override
            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
                if (firebaseAuth.getCurrentUser() != null) {

                    Intent intent = new Intent(LoginActivity.this, MainActivity.class);
                    SharedPreferences pref = getApplicationContext().getSharedPreferences("USER", 0);
                    SharedPreferences.Editor editor = pref.edit();
                    editor.putString("userConnected", firebaseAuth.getCurrentUser().getEmail());
                    editor.commit();

                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent);
                    finish();

                }
            }
        };
        configureGoogleSignin();


        barcodeDetector = new BarcodeDetector.Builder(this)
                .setBarcodeFormats(Barcode.QR_CODE)
                .build();
        cameraSource = new CameraSource
                .Builder(this, barcodeDetector)
                .setRequestedPreviewSize(640, 480)
                .build();

        surfaceQR.getHolder().addCallback(new SurfaceHolder.Callback() {
            @Override
            public void surfaceCreated(SurfaceHolder surfaceHolder) {
                if (ActivityCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
                    //Request Permission
                    ActivityCompat.requestPermissions(LoginActivity.this,
                            new String[]{Manifest.permission.CAMERA},
                            RequestCameraPermissionID);
                    return;
                }
                try {
                    cameraSource.start(surfaceQR.getHolder());
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void surfaceChanged(SurfaceHolder surfaceHolder, int i, int i1, int i2) {

            }

            @Override
            public void surfaceDestroyed(SurfaceHolder surfaceHolder) {
                cameraSource.stop();
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        Typeface type = Typeface.createFromAsset(getAssets(), "fonts/Dopestyle.ttf");
        tvGuardian.setTypeface(type);

        YoYo.with(Techniques.Tada)
                .duration(700)
                .repeat(100)
                .playOn(tvClickHere);
        ibChild.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                switchParentChild();
                qrDetection();

            }
        });

        ibParent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                switchChildParent();
                btnSignin.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        if ((emailSignin.getText().length() != 0) && (passwordSignin.getText().length() != 0) && passwordSignin.getText().length() >= 6) {
                            parentSignInFirebase();
                        }

                    }
                });
            }
        });
        btnSignin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if ((emailSignin.getText().length() != 0) && (passwordSignin.getText().length() != 0) && passwordSignin.getText().length() >= 6) {
                    parentSignInFirebase();
                }
            }
        });


        tvSignup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(LoginActivity.this, RegisterActivity.class);
                startActivity(intent);
            }
        });

        forgotPsw.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                firebaseAuth.sendPasswordResetEmail("mayssa.ezzine@esprit.tn");
            }
        });


    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case RequestCameraPermissionID: {
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    if (ActivityCompat.checkSelfPermission(this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {

                        return;
                    }
                    try {
                        cameraSource.start(surfaceQR.getHolder());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }
        }
    }


    private void signInGoogle() {
        Intent signInIntent = Auth.GoogleSignInApi.getSignInIntent(mGoogleApiClient);
        startActivityForResult(signInIntent, RC_SIGN_IN);


    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == RC_SIGN_IN) {
            GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
            if (result.isSuccess()) {
                GoogleSignInAccount account = result.getSignInAccount();
                firebaseAuthWithGoogle(account);
            }

        }
    }

    private void firebaseAuthWithGoogle(GoogleSignInAccount account) {
        AuthCredential credential = GoogleAuthProvider.getCredential(account.getIdToken()
                , null);
        firebaseAuth.signInWithCredential(credential)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        Log.e("google", "signInWithCredential:onComplete" + task.isSuccessful());


                        if (!task.isSuccessful()) {
                            Log.e("no_google", "signInWithCredential:onComplete" + task.getException());
                            Toast.makeText(LoginActivity.this, "Authentification failed :(", Toast.LENGTH_SHORT).show();
                        }
                    }
                });

    }

    public void childSignInFirebase(String resultCode) {

        (firebaseAuth.signInWithEmailAndPassword(resultCode + "@guardian.com", resultCode))
                .addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {

                        if (task.isSuccessful()) {

                            SharedPreferences pref = getApplicationContext().getSharedPreferences("USER", 0);
                            SharedPreferences.Editor editor = pref.edit();
                            editor.putString("userConnected", firebaseAuth.getCurrentUser().getEmail());
                            editor.commit();

                            Intent intent = new Intent(LoginActivity.this, MainActivity.class);

                            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            startActivity(intent);
                            finish();

                        } else {
                            Toast.makeText(LoginActivity.this, "Failed to connect", Toast.LENGTH_SHORT).show();
                        }
                    }
                });

    }

    public void switchParentChild() {
        emailSigninLayout.setVisibility(View.INVISIBLE);
        passwordSigninLayout.setVisibility(View.INVISIBLE);
        forgotPsw.setVisibility(View.INVISIBLE);

        ibChild.setVisibility(View.INVISIBLE);
        tvDonthaveaccount.setVisibility(View.INVISIBLE);
        tvSignup.setVisibility(View.INVISIBLE);
        surfaceQR.setVisibility(View.VISIBLE);
        resultQR.setVisibility(View.VISIBLE);
        btnSignin.setVisibility(View.INVISIBLE);
        ibParent.setVisibility(View.VISIBLE);

        status.setText("Child");
    }

    public void parentSignInFirebase() {
        final ProgressDialog progressDialog = ProgressDialog.show(LoginActivity.this, "Please wait..", "Processing", true);

        (firebaseAuth.signInWithEmailAndPassword(emailSignin.getText().toString(), passwordSignin.getText().toString()))
                .addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        progressDialog.dismiss();
                        if (task.isSuccessful()) {
                            Intent intent = new Intent(LoginActivity.this, MainActivity.class);
                            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            startActivity(intent);
                            finish();

                        } else {

                            Toast.makeText(LoginActivity.this, "Check your internet connexion", Toast.LENGTH_SHORT).show();

                        }
                    }
                });
    }

    public void switchChildParent() {

        emailSigninLayout.setVisibility(View.VISIBLE);
        passwordSigninLayout.setVisibility(View.VISIBLE);
        forgotPsw.setVisibility(View.VISIBLE);

        ibChild.setVisibility(View.VISIBLE);
        tvDonthaveaccount.setVisibility(View.VISIBLE);
        tvSignup.setVisibility(View.VISIBLE);
        surfaceQR.setVisibility(View.INVISIBLE);
        resultQR.setVisibility(View.INVISIBLE);
        ibParent.setVisibility(View.INVISIBLE);
        btnSignin.setVisibility(View.VISIBLE);

        status.setText("Parent");

    }

    public void configureGoogleSignin() {
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestIdToken(getString(R.string.default_web_client_id))
                .requestEmail()
                .build();
        mGoogleApiClient = new GoogleApiClient.Builder(getApplicationContext())
                .enableAutoManage(this, new GoogleApiClient.OnConnectionFailedListener() {
                    @Override
                    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
                        Toast.makeText(LoginActivity.this, "You got an Error", Toast.LENGTH_SHORT).show();
                    }
                })
                .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                .build();

    }

    public void qrDetection() {
        barcodeDetector.setProcessor(new Detector.Processor<Barcode>() {
            @Override
            public void release() {
            }

            @Override
            public void receiveDetections(Detector.Detections<Barcode> detections) {

                final SparseArray<Barcode> qrcodes = detections.getDetectedItems();
                if (qrcodes.size() != 0) {
                    i++;
                    resultQRSignIN = qrcodes.valueAt(0).displayValue + "";
                    QrDetected = "true";

                    if ((QrDetected == "true") && (i == 1)) {
                        childSignInFirebase(resultQRSignIN);

                    }
                }
            }
        });

    }

}

