package com.example.ezzinemayssa.guardianfinale.service;

import android.app.Notification;
import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.IntentFilter;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Handler;
import android.os.IBinder;
import android.os.PowerManager;
import android.os.Process;
import android.util.Log;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;
import com.example.ezzinemayssa.guardianfinale.entities.Child;
import com.example.ezzinemayssa.guardianfinale.request.GetOneChildByEmailRequest;
import com.example.ezzinemayssa.guardianfinale.request.GetRefreshTokken;
import com.example.ezzinemayssa.guardianfinale.request.SendingNotifRequest;
import com.example.ezzinemayssa.guardianfinale.utils.MySingleton;
import com.google.firebase.auth.FirebaseAuth;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by zasus on 09/01/2018.
 */

public class GestureDetection extends Service implements SensorEventListener {

String userConnected;
    public static final String TAG = GestureDetection.class.getName();
    public static final int SCREEN_OFF_RECEIVER_DELAY = 500;
    FirebaseAuth firebaseAuth;
    String tokken;
    private SensorManager mSensorManager = null;
    private PowerManager.WakeLock mWakeLock = null;
    private long lastUpdate = 0;
    private float last_x, last_y, last_z;
    private static final int SHAKE_THRESHOLD = 3500;
    List<Child> childrenList;
    Child objChild;
    /*
     * Register this as a sensor event listener.
     */
    private void registerListener() {
        mSensorManager.registerListener(this,
                mSensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER),
                SensorManager.SENSOR_DELAY_NORMAL);
    }

    /*
     * Un-register this as a sensor event listener.
     */
    private void unregisterListener() {
        mSensorManager.unregisterListener(this);
    }

    public BroadcastReceiver mReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            Log.e(TAG, "onReceive("+intent+")");

            if (!intent.getAction().equals(Intent.ACTION_SCREEN_OFF)) {
                return;
            }

            Runnable runnable = new Runnable() {
                public void run() {
                    Log.e(TAG, "Runnable executing.");
                    unregisterListener();
                    registerListener();
                }
            };

            new Handler().postDelayed(runnable, SCREEN_OFF_RECEIVER_DELAY);
        }
    };

    public void onAccuracyChanged(Sensor sensor, int accuracy) {


        Log.e(TAG, "onAccuracyChanged().");
    }

    public void onSensorChanged(SensorEvent event) {
        firebaseAuth=FirebaseAuth.getInstance();
        Sensor mySensor = event.sensor;

        if (mySensor.getType() == Sensor.TYPE_ACCELEROMETER) {
            float x = event.values[0];
            float y = event.values[1];
            float z = event.values[2];

            long curTime = System.currentTimeMillis();

            if ((curTime - lastUpdate) > 100) {
                long diffTime = (curTime - lastUpdate);
                lastUpdate = curTime;

                float speed = Math.abs(x + y + z - last_x - last_y - last_z)/ diffTime * 10000;

                if (speed > SHAKE_THRESHOLD) {

                    getChildDetails();

                }

                last_x = x;
                last_y = y;
                last_z = z;
            }
        }

    }

    @Override
    public void onCreate() {
        super.onCreate();

        mSensorManager = (SensorManager) getSystemService(SENSOR_SERVICE);

        PowerManager manager =
                (PowerManager) getSystemService(Context.POWER_SERVICE);
        mWakeLock = manager.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK, TAG);

        registerReceiver(mReceiver, new IntentFilter(Intent.ACTION_SCREEN_OFF));
    }

    @Override
    public void onDestroy() {
        unregisterReceiver(mReceiver);
        unregisterListener();
        mWakeLock.release();
        stopForeground(true);
    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        super.onStartCommand(intent, flags, startId);

        startForeground(Process.myPid(), new Notification());
        registerListener();
        mWakeLock.acquire();

        return START_STICKY;
    }


    public void getChildDetails()
    {
        SharedPreferences prefs = getSharedPreferences("USER", MODE_PRIVATE);
        String restoredText = prefs.getString("userConnected", null);
        if (restoredText != null) {
            userConnected = prefs.getString("userConnected", "No name defined");//"No name defined" is the default value.

            Log.e("user",userConnected);
        }


        RequestQueue mRequestQueue;

        childrenList = new ArrayList<>();

        Response.Listener<String> responseListener = new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    JSONObject jsonObject= new JSONObject(response);
                    JSONArray jsonArray= jsonObject.getJSONArray("oneChild");
                    int count =0;


                    while(count< jsonArray.length())
                    {
                        JSONObject jo= jsonArray.getJSONObject(count);
                        childrenList.add(new Child(jo.getString("code"),jo.getString("email"),jo.getString("username")
                                ,jo.getString("phone"),jo.getString("image_name"),jo.getString("email_parent"),jo.getString("birthdate"),jo.getString("image_qr")));
                        count++;

                    }
                    objChild = childrenList.get(0);
                    Response.Listener<String> responseListener2 = new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {

                            try {
                                JSONObject jsonResponse = new JSONObject(response);

                                JSONArray jsonArray= jsonResponse.getJSONArray("tokken");
                                int count= 0;
                                while(count<jsonArray.length())
                                {
                                    JSONObject jo= jsonArray.getJSONObject(count);

                                    tokken= jo.getString("tokken");
                                    count++;

                                    Log.e("Response",tokken);
                                    Response.Listener<String> responseListener = new Response.Listener<String>() {
                                        @Override
                                        public void onResponse(String response) {
                                            getChildDetails();
                                        }
                                    };

                                    SendingNotifRequest getChildrenByEmailParentRequest = new SendingNotifRequest(
                                            "Emergency",  tokken,"Your child "+ objChild.getUsername()+" has been kidnapped!! please check his localisation ", responseListener);
                                    MySingleton.getInstance(GestureDetection.this).addToRequestQueue(getChildrenByEmailParentRequest);

                                }

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    };

                    GetRefreshTokken tokkenParent = new GetRefreshTokken(
                            userConnected, responseListener2);
                    MySingleton.getInstance(GestureDetection.this).addToRequestQueue(tokkenParent);

                } catch (JSONException e) {

                    e.printStackTrace();
                }
            }
        };
        Response.ErrorListener responseListener5 = new Response.ErrorListener() {


            @Override
            public void onErrorResponse(VolleyError error) {
            }
        };
        GetOneChildByEmailRequest getOneChildByEmailRequest = new GetOneChildByEmailRequest(userConnected, responseListener,responseListener5);
        MySingleton.getInstance(GestureDetection.this).addToRequestQueue(getOneChildByEmailRequest);


    }
}
