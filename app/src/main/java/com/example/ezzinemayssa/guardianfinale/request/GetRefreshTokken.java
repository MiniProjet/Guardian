package com.example.ezzinemayssa.guardianfinale.request;

import android.util.Log;

import com.android.volley.Response;
import com.android.volley.toolbox.StringRequest;
import com.example.ezzinemayssa.guardianfinale.activity.MainActivity;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by mayss on 09/01/2018.
 */

public class GetRefreshTokken extends StringRequest {


    private static final String REGISTER_REQUEST_URL = "http://"+ MainActivity.ipAdress+"/guardenScript/getTokkenByEmailParent.php";
    private Map<String, String> params;

    public GetRefreshTokken(String email_child, Response.Listener<String> listener) {
        super(Method.POST, REGISTER_REQUEST_URL,listener,null);
        params = new HashMap<>();
        params.put("email_child",email_child);

        Log.e("Response","tokken");

    }

    @Override
    public Map<String, String> getParams() {
        return params;
    }
}
