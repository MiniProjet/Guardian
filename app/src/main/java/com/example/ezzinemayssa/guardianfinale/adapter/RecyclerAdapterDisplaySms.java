package com.example.ezzinemayssa.guardianfinale.adapter;

/**
 * Created by Shade on 5/9/2016.
 */

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.toolbox.ImageLoader;
import com.example.ezzinemayssa.guardianfinale.R;
import com.example.ezzinemayssa.guardianfinale.entities.Sms;
import com.google.firebase.auth.FirebaseAuth;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;


public class RecyclerAdapterDisplaySms extends RecyclerView.Adapter<RecyclerAdapterDisplaySms.ViewHolder> {

    private Sms current;
    private  Context context;
    List<Sms> listSms = new ArrayList<>();
    private FirebaseAuth firebaseAuth;
    private LayoutInflater inflater;
    private ImageLoader imageLoaderProfile;
    private ImageLoader imageLoaderQrCode;


    // create constructor to innitilize context and data sent from MainActivity
    public RecyclerAdapterDisplaySms(Context context, List<Sms> listSms){
        this.context=context;
        this.listSms=listSms;
        inflater= LayoutInflater.from(context);


    }

    class ViewHolder extends RecyclerView.ViewHolder{

        private TextView itemImageAvatar;
        private TextView itemName_sms;
        private TextView itemNumber_sms;
        private TextView itemTime_sms;
        private TextView itemContent_sms;




        private ViewHolder(View itemView) {
            super(itemView);
            itemImageAvatar =itemView.findViewById(R.id.lettreSms);
            itemContent_sms = itemView.findViewById(R.id.content_sms);
            itemNumber_sms = itemView.findViewById(R.id.number_sms);
            itemTime_sms = itemView.findViewById(R.id.time_sms);
            itemName_sms = itemView.findViewById(R.id.from_name_sms);


        }
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_sms, viewGroup, false);
        ViewHolder viewHolder = new ViewHolder(v);
        return viewHolder;


    }


    @Override
    public void onBindViewHolder(ViewHolder viewHolder, int i ) {
        ViewHolder myHolder= (ViewHolder) viewHolder;

        current=listSms.get(i);
        Log.e("listHolder",listSms.toString());

        myHolder.itemName_sms.setText(current.getName()+": ");
        myHolder.itemNumber_sms.setText(current.getNumber());
        Timestamp stamp=current.getTime();
        Date date = new Date(stamp.getTime());
        SimpleDateFormat houre = new SimpleDateFormat("h:mm");
        houre.setTimeZone(TimeZone.getTimeZone("GMT"));
        String formattedHoure = houre.format(date);

        SimpleDateFormat date2 = new SimpleDateFormat("d/M/y");
        date2.setTimeZone(TimeZone.getTimeZone("GMT"));
        String formattedDate2 = date2.format(date);

        myHolder.itemTime_sms.setText(formattedDate2+", "+formattedHoure+" PM");
        myHolder.itemContent_sms.setText(current.getContent());
        myHolder.itemImageAvatar.setText(current.getName().substring(0,1));



    }
    public List<Sms> getItems() {
        return listSms;
    }

    @Override
    public int getItemCount() {
        return listSms.size();
    }


    public void filterList(ArrayList<Sms> filteredList) {
        listSms = filteredList;
        notifyDataSetChanged();

    }

}