package com.example.ezzinemayssa.guardianfinale.fragment;

import android.app.Fragment;
import android.app.ProgressDialog;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.GridView;
import android.widget.Toast;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;
import com.example.ezzinemayssa.guardianfinale.R;
import com.example.ezzinemayssa.guardianfinale.activity.map.AppercuChildMap;
import com.example.ezzinemayssa.guardianfinale.adapter.AppInstalledAdapter;
import com.example.ezzinemayssa.guardianfinale.adapter.RecyclerAdapterChooseChildren;
import com.example.ezzinemayssa.guardianfinale.entities.AppInstalled;
import com.example.ezzinemayssa.guardianfinale.request.GetAppInstalled;
import com.example.ezzinemayssa.guardianfinale.utils.MySingleton;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;


public class ListAppChildFragment extends Fragment {
    String TAG="testAppInstalled";
    ProgressDialog progress;
    GridView gridView;
    ArrayList<AppInstalled>listApp;
    public ListAppChildFragment() {
        // Required empty public constructor
    }




    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_list_app_child, container, false);
        listApp=new ArrayList();
        progress=new ProgressDialog(getActivity());
        gridView=(GridView)view.findViewById(R.id.gridView1);









        Response.Listener<String> responseListener3 = new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {


                try {

                    JSONObject jsonResponse = new JSONObject(response);
                    // boolean success = jsonResponse.getBoolean("success");
                    Log.e("wiiw",response);
                    JSONArray jsonArray= jsonResponse.getJSONArray("app");
                    int count=0;
                    while(count<jsonArray.length())
                    {
                        JSONObject jo= jsonArray.getJSONObject(count);
                        listApp.add(new AppInstalled(jo.getString("nameapp"),
                                jo.getString("path")));
                        count++;


                        Log.e("App",listApp.toString());

                    }
                    progress.dismiss();
                    AppInstalledAdapter adapter = new AppInstalledAdapter(getActivity(), R.layout.app_installed_item,listApp);
                    gridView.setAdapter(adapter );
                } catch (JSONException e) {
                    e.printStackTrace();
                }



            }
        };
        Response.ErrorListener responseListener5 = new Response.ErrorListener() {


            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("Tag", error.toString());
                Toast.makeText(getActivity(), error.toString(),
                        Toast.LENGTH_LONG).show();
                progress.dismiss();
            }
        };
        progress.setMessage("Downloading  :) ");
        progress.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progress.setIndeterminate(true);
        progress.show();
        GetAppInstalled addlocation = new GetAppInstalled(RecyclerAdapterChooseChildren.email_child_chosen,responseListener3,responseListener5);
        RequestQueue queue = MySingleton.getInstance(getActivity()).getRequestQueue();
// Add a request (in this example, called stringRequest) to your RequestQueue.
        MySingleton.getInstance(getActivity()).addToRequestQueue(addlocation);








        return view;
    }


//
}
