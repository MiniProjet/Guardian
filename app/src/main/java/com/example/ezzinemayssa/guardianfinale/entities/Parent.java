package com.example.ezzinemayssa.guardianfinale.entities;

/**
 * Created by Ezzine Mayssa on 13/11/2017.
 */

public class Parent {
    private String email;
    private String password;
    private String username;
    private String picture;
    private String phone;


    public Parent() {
    }

    public Parent(String email, String password, String username, String picture, String phone) {
        this.email = email;
        this.password = password;
        this.username = username;
        this.picture = picture;
        this.phone = phone;
    }

    public Parent(String phone) {
        this.phone = phone;
    }


    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPicture() {
        return picture;
    }

    public void setPicture(String picture) {
        this.picture = picture;
    }

    public String getPhone() {return phone;}

    public void setPhone(String phone) {
        this.phone = phone;
    }



    @Override
    public String toString() {
        return "Parent{" +
                "email='" + email + '\'' +
                ", password='" + password + '\'' +
                ", username='" + username + '\'' +
                ", picture='" + picture + '\'' +
                ", phone='" + phone + '\'' +
                '}';
    }
}
