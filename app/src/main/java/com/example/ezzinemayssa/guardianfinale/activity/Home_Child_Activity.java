package com.example.ezzinemayssa.guardianfinale.activity;

import android.Manifest;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.DialogFragment;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;
import com.example.ezzinemayssa.guardianfinale.R;
import com.example.ezzinemayssa.guardianfinale.activity.map.AppercuChildMap;
import com.example.ezzinemayssa.guardianfinale.entities.Child;
import com.example.ezzinemayssa.guardianfinale.fragment.ChatMessageFragment;
import com.example.ezzinemayssa.guardianfinale.fragment.PickMeUpDialog;
import com.example.ezzinemayssa.guardianfinale.request.GetOneChildByEmailRequest;
import com.example.ezzinemayssa.guardianfinale.utils.MySingleton;
import com.google.firebase.auth.FirebaseAuth;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import static com.example.ezzinemayssa.guardianfinale.adapter.RecyclerAdapterDisplayChildren.emailChildSelected;

public class Home_Child_Activity extends AppCompatActivity {
    Button pickMe;
    Button btnChatHomeChild;
    private Child childObj;
    ImageView imageProfile_activityHomeChild;
    TextView username_activityHomeChild;
    ImageView btnProtectionCivil;
    ImageView btnPolice;
    ImageView btnAmbulance;
    private FirebaseAuth firebaseAuth;
    private List<Child> childrenList ;
    LinearLayout chatContainer;
    LinearLayout childContainer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_home__child_);
        firebaseAuth = FirebaseAuth.getInstance();
        pickMe = findViewById(R.id.btn_pick_me);
        btnProtectionCivil = findViewById(R.id.civilProtectionCall);
        btnPolice = findViewById(R.id.PoliceCall);
        btnAmbulance = findViewById(R.id.AmbulanceCall);
        imageProfile_activityHomeChild = findViewById(R.id.imageProfile_activityHomeChild);
        username_activityHomeChild = findViewById(R.id.username_activityHomeChild);
        btnChatHomeChild = findViewById(R.id.btnChatHomeChild);
        chatContainer = findViewById(R.id.dispalyChat);
        childContainer = findViewById(R.id.displayChild);

    }


    @Override
    protected void onResume() {
        super.onResume();
        activateGps();
        pickMe.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                DialogFragment newFragment = new PickMeUpDialog();

                newFragment.show(getFragmentManager(), "chooseChildrenDialog");
            }
        });
        getChildDetails();
        callServices();
        btnChatHomeChild.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                chatContainer.setVisibility(View.VISIBLE);
                childContainer.setVisibility(View.GONE);
                getFragmentManager().beginTransaction().add(R.id.dispalyChat, new ChatMessageFragment()).addToBackStack(null).commit();
            }
        });







    }

    private void buildAlertMessageNoGps() {
        final android.support.v7.app.AlertDialog.Builder builder = new android.support.v7.app.AlertDialog.Builder(this);
        builder.setMessage("Your GPS seems to be disabled,  you must  enable it?")
                .setCancelable(false)
                .setPositiveButton("RIGHT NOW", new DialogInterface.OnClickListener() {
                    public void onClick(@SuppressWarnings("unused") final DialogInterface dialog, @SuppressWarnings("unused") final int id) {
                        //  buildAlertMessageNoGps();
                    }
                });

        final android.support.v7.app.AlertDialog alert = builder.create();
        alert.show();
    }

    public void activateGps() {
        final LocationManager manager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);

        if (!manager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
            buildAlertMessageNoGps();
        }

    }

    public void activateGps2() {
        final LocationManager manager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);

        if (!manager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
            Intent intent= new Intent(Home_Child_Activity.this, Home_Child_Activity.class);

            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(intent);
            finish();
        }

    }



    @Override
    protected void onStop() {
        super.onStop();

        activateGps2();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        activateGps2();
        Log.e("destroy","destroy");
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
//        Intent intent = new Intent(Intent.ACTION_MAIN);
//        intent.addCategory(Intent.CATEGORY_HOME);
//        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
//        startActivity(intent);
//        finish();
//        System.exit(0);
        chatContainer.setVisibility(View.GONE);
        childContainer.setVisibility(View.VISIBLE);
        startActivity(new Intent(Home_Child_Activity.this,Home_Child_Activity.class));
    }

    public void getChildDetails()
    {
        RequestQueue mRequestQueue;

        childrenList = new ArrayList<>();

        Response.Listener<String> responseListener = new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    JSONObject jsonObject= new JSONObject(response);
                    JSONArray jsonArray= jsonObject.getJSONArray("oneChild");
                    int count =0;


                    while(count< jsonArray.length())
                    {
                        JSONObject jo= jsonArray.getJSONObject(count);
                        childrenList.add(new Child(jo.getString("code"),jo.getString("email"),jo.getString("username")
                                ,jo.getString("phone"),jo.getString("image_name"),jo.getString("email_parent"),jo.getString("birthdate"),jo.getString("image_qr")));
                        count++;

                    }
                       childObj = childrenList.get(0);
                       Picasso.with(Home_Child_Activity.this).load(childObj.getImage_name()).into( imageProfile_activityHomeChild);
                       username_activityHomeChild.setText(childObj.getUsername());

                } catch (JSONException e) {

                    e.printStackTrace();
                }
            }
        };
        Response.ErrorListener responseListener5 = new Response.ErrorListener() {


            @Override
            public void onErrorResponse(VolleyError error) {




            }
        };
        GetOneChildByEmailRequest getOneChildByEmailRequest = new GetOneChildByEmailRequest(firebaseAuth.getCurrentUser().getEmail(), responseListener,responseListener5);
        RequestQueue queue = MySingleton.getInstance(Home_Child_Activity.this).getRequestQueue();
// Add a request (in this example, called stringRequest) to your RequestQueue.
        MySingleton.getInstance(this).addToRequestQueue(getOneChildByEmailRequest);


    }
    public void callServices()
    {
        btnAmbulance.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                AlertDialog.Builder builder = new AlertDialog.Builder(Home_Child_Activity.this);
                builder.setTitle("Confirm call to 190 ");
                builder.setMessage("You are about to call the ambulance service. Do you really want to proceed ?");
                builder.setCancelable(false);
                builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {


                        Intent dialIntent = new Intent(Intent.ACTION_CALL);
                        dialIntent.setData(Uri.parse("tel:" + "190"));
                        if (ActivityCompat.checkSelfPermission(getBaseContext(), Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {

                            return;
                        }
                        startActivity(dialIntent);




                    }
                });

                builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });

                builder.show();




            }
        });

        btnPolice.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                AlertDialog.Builder builder = new AlertDialog.Builder(Home_Child_Activity.this);
                builder.setTitle("Confirm call to 197 ");
                builder.setMessage("You are about to call the police service. Do you really want to proceed ?");
                builder.setCancelable(false);
                builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {


                        Intent dialIntent = new Intent(Intent.ACTION_CALL);
                        dialIntent.setData(Uri.parse("tel:" + "197"));
                        if (ActivityCompat.checkSelfPermission(getBaseContext(), Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                            // TODO: Consider calling
                            //    ActivityCompat#requestPermissions
                            // here to request the missing permissions, and then overriding
                            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                            //                                          int[] grantResults)
                            // to handle the case where the user grants the permission. See the documentation
                            // for ActivityCompat#requestPermissions for more details.
                            return;
                        }
                        startActivity(dialIntent);




                    }
                });

                builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });

                builder.show();




            }
        });
        btnProtectionCivil.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                AlertDialog.Builder builder = new AlertDialog.Builder(Home_Child_Activity.this);
                builder.setTitle("Confirm call to 198 ");
                builder.setMessage("You are about to call the protection civil service. Do you really want to proceed ?");
                builder.setCancelable(false);
                builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {


                        Intent dialIntent = new Intent(Intent.ACTION_CALL);
                        dialIntent.setData(Uri.parse("tel:" + "198"));
                        if (ActivityCompat.checkSelfPermission(getBaseContext(), Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                            // TODO: Consider calling
                            //    ActivityCompat#requestPermissions
                            // here to request the missing permissions, and then overriding
                            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                            //                                          int[] grantResults)
                            // to handle the case where the user grants the permission. See the documentation
                            // for ActivityCompat#requestPermissions for more details.
                            return;
                        }
                        startActivity(dialIntent);




                    }
                });

                builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });

                builder.show();




            }
        });

    }
}
