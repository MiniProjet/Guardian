package com.example.ezzinemayssa.guardianfinale.service;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.util.Log;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.toolbox.Volley;
import com.example.ezzinemayssa.guardianfinale.entities.RequestSteps;
import com.example.ezzinemayssa.guardianfinale.entities.StepsChild;
import com.example.ezzinemayssa.guardianfinale.request.AddStepsChild;
import com.example.ezzinemayssa.guardianfinale.request.GetStepsByChild;
import com.example.ezzinemayssa.guardianfinale.request.UpdateStepsChild;
import com.example.ezzinemayssa.guardianfinale.utils.MySingleton;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Date;

/**
 * Created by mayss on 27/11/2017.
 */

public class getStepService extends Service implements SensorEventListener{
    DatabaseReference statStepsRequest;
    private FirebaseAuth firebaseAuth;
    StepsChild steps;
    DatabaseReference statStepsResponse;
    SensorManager sensorManager;
    Date time ;
    String userConnected;
    Boolean firsttime=false;
    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }
    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        firebaseAuth = FirebaseAuth.getInstance();
        sensorManager=(SensorManager) getSystemService(Context.SENSOR_SERVICE);
        Sensor conterSensor=sensorManager.getDefaultSensor(Sensor.TYPE_STEP_COUNTER);

        SharedPreferences prefs = getSharedPreferences("USER", MODE_PRIVATE);
        String restoredText = prefs.getString("userConnected", null);
        if (restoredText != null) {
            userConnected = prefs.getString("userConnected", "No name defined");//"No name defined" is the default value.

            Log.e("user",userConnected);
        }



        Log.e("yeah","yeah");

        if(conterSensor!=null){
            sensorManager.registerListener(this,conterSensor,SensorManager.SENSOR_DELAY_UI);
        }
        time = java.util.Calendar.getInstance().getTime();
        Response.Listener<String> responseListener = new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {



            }};

        AddStepsChild getChildrenByEmailParentRequest = new AddStepsChild(userConnected, responseListener);
        RequestQueue queue = MySingleton.getInstance(getStepService.this).getRequestQueue();
// Add a request (in this example, called stringRequest) to your RequestQueue.
        MySingleton.getInstance(getStepService.this).addToRequestQueue(getChildrenByEmailParentRequest);


        //  requestParentSteps();
        return START_STICKY;
    }

    @Override
    public void onSensorChanged(SensorEvent sensorEvent) {
        String steps="0";
        Log.e("ss",sensorEvent.values[0]+"");

        if(time.getHours()==00) {
            updateSteps("hour0",Math.round(sensorEvent.values[0]));

        }
        if(time.getHours()==01) {
            updateSteps("hour1",Math.round(sensorEvent.values[0]));
        }
        if(time.getHours()==02) {
            updateSteps("hour2",Math.round(sensorEvent.values[0]));
        }
        if(time.getHours()==03) {
            updateSteps("hour3",Math.round(sensorEvent.values[0]));
        }
        if(time.getHours()==04) {
            updateSteps("hour4",Math.round(sensorEvent.values[0]));
        }
        if(time.getHours()==05) {
            updateSteps("hour5",Math.round(sensorEvent.values[0]));
        }
        if(time.getHours()==06) {
            updateSteps("hour6",Math.round(sensorEvent.values[0]));
        }
        if(time.getHours()==07) {
            updateSteps("hour7",Math.round(sensorEvent.values[0]));
        }
        if(time.getHours()==08f) {
            updateSteps("hour8",Math.round(sensorEvent.values[0]));
        }if(time.getHours()==09f) {
            updateSteps("hour9",Math.round(sensorEvent.values[0]));
        }
        if(time.getHours()==10) {
            updateSteps("hour10",Math.round(sensorEvent.values[0]));
        }
        if(time.getHours()==11) {
            updateSteps("hour11",Math.round(sensorEvent.values[0]));
        }
        if(time.getHours()==12) {
            updateSteps("hour12",Math.round(sensorEvent.values[0]));
        }
        if(time.getHours()==13) {
            updateSteps("hour13",Math.round(sensorEvent.values[0]));
        }if(time.getHours()==14) {
            Log.e("update","true");
            updateSteps("hour14",Math.round(sensorEvent.values[0]));
        }if(time.getHours()==15) {
            updateSteps("hour15",Math.round(sensorEvent.values[0]));
        }if(time.getHours()==16) {
            updateSteps("hour16",Math.round(sensorEvent.values[0]));
        }if(time.getHours()==17) {
            updateSteps("hour17",Math.round(sensorEvent.values[0]));
        }if(time.getHours()==18) {
            updateSteps("hour18",Math.round(sensorEvent.values[0]));
        }if(time.getHours()==19) {
            updateSteps("hour19",Math.round(sensorEvent.values[0]));
        }if(time.getHours()==20) {
            updateSteps("hour20",Math.round(sensorEvent.values[0]));
        }
        if(time.getHours()==21) {
            updateSteps("hour21",Math.round(sensorEvent.values[0]));
        }
        if(time.getHours()==22) {
            updateSteps("hour22",Math.round(sensorEvent.values[0]));
        }
        if(time.getHours()==23) {
            updateSteps("hour23",Math.round(sensorEvent.values[0]));
        }







    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int i) {

    }

    public void updateSteps(String hour,int steps){
        Response.Listener<String> responseListener = new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {



            }};
        Log.e("update","wiiiiw");
        UpdateStepsChild getChildrenByEmailParentRequest = new UpdateStepsChild(userConnected,hour,steps, responseListener);
        RequestQueue queue = MySingleton.getInstance(getStepService.this).getRequestQueue();
// Add a request (in this example, called stringRequest) to your RequestQueue.
        MySingleton.getInstance(getStepService.this).addToRequestQueue(getChildrenByEmailParentRequest);
    }







    }
