package com.example.ezzinemayssa.guardianfinale.receiver;

import android.app.admin.DeviceAdminReceiver;
import android.app.admin.DevicePolicyManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;

/**
 * Created by mayss on 09/12/2017.
 */

public class LockedPhoneReceiver extends DeviceAdminReceiver {
    @Override
    public CharSequence onDisableRequested(final Context context, Intent intent) {

        Intent startMain = new Intent(Intent.ACTION_MAIN);
        startMain.addCategory(Intent.CATEGORY_HOME);
        startMain.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        context.startActivity(startMain); //switch to the home screen, not totally necessary
        lockPhone(context, "1234");
        //Log.i(TAG, "DEVICE ADMINISTRATION DISABLE REQUESTED & LOCKED PHONE");

        return "haha. i locked your phone.";
    }
    public static boolean lockPhone(Context context, String password){
     ComponentName   devAdminReceiver = new ComponentName(context, LockedPhoneReceiver.class);
      DevicePolicyManager  dpm = (DevicePolicyManager)context.getSystemService(Context.DEVICE_POLICY_SERVICE);
        boolean pwChange = dpm.resetPassword(password, 0);
        dpm.lockNow();
        return pwChange;
    }
}