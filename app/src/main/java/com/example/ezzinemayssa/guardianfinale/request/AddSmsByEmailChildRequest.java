package com.example.ezzinemayssa.guardianfinale.request;

import com.android.volley.Response;
import com.android.volley.toolbox.StringRequest;

import java.sql.Timestamp;
import java.util.HashMap;
import java.util.Map;

import static com.example.ezzinemayssa.guardianfinale.activity.MainActivity.ipAdress;

/**
 * Created by Ezzine Mayssa on 13/11/2017.
 */

public class AddSmsByEmailChildRequest extends StringRequest {


        private static final String REGISTER_REQUEST_URL = "http://"+ipAdress+"/guardenScript/AddSmsByEmailChild.php";
        private Map<String, String> params;

        public AddSmsByEmailChildRequest(String state, String number, Timestamp time, String name, String email_child,String content, Response.Listener<String> listener) {
            super(Method.POST, REGISTER_REQUEST_URL, listener, null);
            params = new HashMap<>();

            params.put("state",state);
            params.put("number",number);
            params.put("time",time+"");
            params.put("name",name);
            params.put("email_child", email_child);
            params.put("content", content);

        }

        @Override
        public Map<String, String> getParams() {
            return params;
        }
    }

