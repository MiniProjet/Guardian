package com.example.ezzinemayssa.guardianfinale.receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.telephony.SmsManager;
import android.telephony.SmsMessage;
import android.util.Log;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.example.ezzinemayssa.guardianfinale.entities.Sms;
import com.example.ezzinemayssa.guardianfinale.request.AddSmsByEmailChildRequest;
import com.example.ezzinemayssa.guardianfinale.utils.MySingleton;
import com.google.firebase.auth.FirebaseAuth;

import org.json.JSONException;
import org.json.JSONObject;

import java.sql.Timestamp;

import static com.example.ezzinemayssa.guardianfinale.activity.MainActivity.role;


/**
 * Created by Ezzine Mayssa on 14/11/2017.
 */

public class SMSReceiver extends BroadcastReceiver {
    // SmsManager class is responsible for all SMS related actions
    final SmsManager sms = SmsManager.getDefault();
    String phoneNumber;
    String name;
    String number;
    String content;
    String state;
    Timestamp  time;
    private FirebaseAuth firebaseAuth;
    private static final String TAG = "IncomingSms";

    public void onReceive(Context context, Intent intent) {




  // Get the SMS message received
        final Bundle bundle = intent.getExtras();
        try {
            if (bundle != null) {
                // A PDU is a "protocol data unit". This is the industrial standard for SMS message
                final Object[] pdusObj = (Object[]) bundle.get("pdus");
                for (int i = 0; i < pdusObj.length; i++) {

                    SmsMessage sms = SmsMessage.createFromPdu((byte[]) pdusObj[i]);
                    number = sms.getDisplayOriginatingAddress();
                    name = getContactName(number, context);
                    content = sms.getDisplayMessageBody();
                    state="RECEIVED";
                    time=new Timestamp(sms.getTimestampMillis());

                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }


        if(role.equals("CHILD"))
        {
            addSms(context);
        }



    }
    private String getContactName(String number, Context context) {

        String name = "Unknown";
        String[] projection = new String[] {
                ContactsContract.PhoneLookup.DISPLAY_NAME,
                ContactsContract.PhoneLookup._ID};
        Uri contactUri = Uri.withAppendedPath(ContactsContract.PhoneLookup.CONTENT_FILTER_URI, Uri.encode(number));
        Cursor cursor = context.getContentResolver().query(contactUri, projection, null, null, null);

        if(cursor != null) {
            if (cursor.moveToFirst()) {
                name =cursor.getString(cursor.getColumnIndex(ContactsContract.PhoneLookup.DISPLAY_NAME));

            }
            cursor.close();
        }
        return name;
    }

    public void addSms(Context context)
    {
        firebaseAuth = FirebaseAuth.getInstance();
        Response.Listener<String> responseListener = new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    JSONObject jsonResponse = new JSONObject(response);
                    String activated = jsonResponse.getString("addSms");
                    if (activated.equals("true")) {
                        Log.e("addSMS", "add ");
                    } else {
                        Log.e("addSMS", "no ");
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        };
        Sms sms = new Sms();
        sms.setNumber(number);
        sms.setName(name);
        sms.setState(state);
        sms.setEmail_child(firebaseAuth.getCurrentUser().getEmail());
        sms.setTime(time);
        sms.setContent(content);
        AddSmsByEmailChildRequest register = new AddSmsByEmailChildRequest(sms.getState(),sms.getNumber(),sms.getTime(),sms.getName(),sms.getEmail_child(),sms.getContent(), responseListener);
        RequestQueue queue = MySingleton.getInstance(context).getRequestQueue();
// Add a request (in this example, called stringRequest) to your RequestQueue.
        MySingleton.getInstance(context).addToRequestQueue(register);
    }

}
