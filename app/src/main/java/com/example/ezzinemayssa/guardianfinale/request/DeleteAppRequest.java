package com.example.ezzinemayssa.guardianfinale.request;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.toolbox.StringRequest;
import com.example.ezzinemayssa.guardianfinale.activity.MainActivity;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by mayss on 04/01/2018.
 */

public class DeleteAppRequest extends StringRequest {


    private static final String REGISTER_REQUEST_URL = "http://"+ MainActivity.ipAdress+"/guardenScript/DeleteApp.php";
    private Map<String, String> params;

    public DeleteAppRequest(String email, Response.Listener<String> listener) {
        super(Request.Method.POST, REGISTER_REQUEST_URL, listener, null);
        params = new HashMap<>();
        params.put("email_child",email);


    }

    @Override
    public Map<String, String> getParams() {
        return params;
    }
}



