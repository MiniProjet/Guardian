package com.example.ezzinemayssa.guardianfinale.entities;

/**
 * Created by Ezzine Mayssa on 09/12/2017.
 */

public class ForwardCall {
    String email_child;
    String email_parent;
    String parent_number;


    public ForwardCall() {
    }

    public ForwardCall(String email_child, String email_parent, String parent_number) {
        this.email_child = email_child;
        this.email_parent = email_parent;
        this.parent_number = parent_number;
    }

    public String getEmail_child() {
        return email_child;
    }

    public void setEmail_child(String email_child) {
        this.email_child = email_child;
    }

    public String getEmail_parent() {
        return email_parent;
    }

    public void setEmail_parent(String email_parent) {
        this.email_parent = email_parent;
    }

    public String getParent_number() {
        return parent_number;
    }

    public void setParent_number(String parent_number) {
        this.parent_number = parent_number;
    }

    @Override
    public String toString() {
        return "ForwardCall{" +
                "email_child='" + email_child + '\'' +
                ", email_parent='" + email_parent + '\'' +
                ", parent_number='" + parent_number + '\'' +
                '}';
    }


}
