package com.example.ezzinemayssa.guardianfinale.activity;

import android.Manifest;
import android.app.ActivityManager;
import android.app.DialogFragment;
import android.app.admin.DevicePolicyManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.annotation.RequiresApi;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageButton;
import android.widget.ImageView;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;
import com.daimajia.androidanimations.library.Techniques;
import com.daimajia.androidanimations.library.YoYo;
import com.example.ezzinemayssa.guardianfinale.activity.map.CreationZoneMap;
import com.example.ezzinemayssa.guardianfinale.activity.map.DiagonalLayoutMainActivity;
import com.example.ezzinemayssa.guardianfinale.activity.map.TrackingMapActivity;
import com.example.ezzinemayssa.guardianfinale.entities.Child;
import com.example.ezzinemayssa.guardianfinale.entities.Parent;
import com.example.ezzinemayssa.guardianfinale.entities.Tracking;
import com.example.ezzinemayssa.guardianfinale.entities.User;
import com.example.ezzinemayssa.guardianfinale.fragment.ChooseChildFragment;
import com.example.ezzinemayssa.guardianfinale.fragment.DisplayChildFragment;
import com.example.ezzinemayssa.guardianfinale.fragment.HomeFragment;
import com.example.ezzinemayssa.guardianfinale.fragment.PickMeUpDialog;
import com.example.ezzinemayssa.guardianfinale.receiver.LockedPhoneReceiver;
import com.example.ezzinemayssa.guardianfinale.request.GetChildrenByEmailParentRequest;
import com.example.ezzinemayssa.guardianfinale.request.GetOneChildByEmailRequest;
import com.example.ezzinemayssa.guardianfinale.request.GetParentByEmailRequest;
import com.example.ezzinemayssa.guardianfinale.request.RefreshTokkenRequest;
import com.example.ezzinemayssa.guardianfinale.service.DisableForwardCallRespose;
import com.example.ezzinemayssa.guardianfinale.service.ForwardCallRespose;
import com.example.ezzinemayssa.guardianfinale.service.GestureDetection;
import com.example.ezzinemayssa.guardianfinale.service.PickMeChildResponse;
import com.example.ezzinemayssa.guardianfinale.service.PickMeUpParent;
import com.example.ezzinemayssa.guardianfinale.service.SendingLocation;
import com.example.ezzinemayssa.guardianfinale.service.getStepService;
import com.example.ezzinemayssa.guardianfinale.utils.MySingleton;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.luseen.spacenavigation.SpaceItem;
import com.luseen.spacenavigation.SpaceNavigationView;
import com.luseen.spacenavigation.SpaceOnClickListener;
import com.luseen.spacenavigation.SpaceOnLongClickListener;
import com.example.ezzinemayssa.guardianfinale.R;


import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;


public class MainActivity extends AppCompatActivity  /* implementsAddChildFragment.NoticeDialogListener*/ {
    private String userConnected;
    private static final int MY_PERMISSION_REQUEST_CODE = 7171;
    private static final int PLAY_SERVICES_RES_REQUEST = 7172;
    private static final int MY_PERMISSIONS_REQUEST_CALL_PHONE = 1234;
    private static final int MY_PERMISSION_SMS = 1;
    final int CODE_GALLERY_REQUEST = 203;
    public static List<Tracking> listLocation;
    public static String PREFERENCE_FILENAME = "reporting_app";
    public static SpaceNavigationView spaceNavigationView;
    private FirebaseAuth firebaseAuth;
    private FirebaseAuth.AuthStateListener firebaseAuthListener;
    public static Context contextOfApplication;
    DatabaseReference statStepsResponse;
    private Toolbar toolbar;
    public static ImageButton btnChooseChild;
    ChooseChildFragment frg;
    Context context;
    ImageView btnChooseProfile;
    ImageView btnChooseLight;
    List<Child> childrenList;
    public static String EMAIL_PARENT_OF_CONNECTED_CHILD;
    DatabaseReference onlineRef, counterRef, currentUserRef;
    User user;
    private static String[] PERMISSIONS_CONTACT = {Manifest.permission.READ_CONTACTS,
            Manifest.permission.WRITE_CONTACTS};
    public static String role;
    public static List<Child> childrenListMainActivity;
    public static final String ipAdress = "192.168.1.3";
    public static List<Parent> listParent = new ArrayList<>();

    public static Context getContextOfApplication() {
        return contextOfApplication;
    }

    @Override
    protected void onStart() {
        super.onStart();
        firebaseAuth.addAuthStateListener(firebaseAuthListener);
    }


    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    protected void onCreate(Bundle savedInstanceState) {

        SharedPreferences prefs = getSharedPreferences("USER", MODE_PRIVATE);
        String restoredText = prefs.getString("userConnected", null);
        if (restoredText != null) {
            userConnected = prefs.getString("userConnected", "No name defined");//"No name defined" is the default value.

            Log.e("user",userConnected);
        }
        listLocation = new ArrayList<>();
        super.onCreate(savedInstanceState);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            getWindow().setStatusBarColor(Color.LTGRAY);
        }

        setContentView(R.layout.activity_main);
        InitialiseOnlineReference();
        contextOfApplication = getApplicationContext();
        spaceNavigationView = findViewById(R.id.space);
        spaceNavigationView.initWithSaveInstanceState(savedInstanceState);
        firebaseAuth = FirebaseAuth.getInstance();
        btnChooseChild = findViewById(R.id.btn_choose_child);
        btnChooseProfile = findViewById(R.id.btn_choose_profile);
        btnChooseLight = findViewById(R.id.btn_light);

        btnChooseLight.setVisibility(View.VISIBLE);
        YoYo.with(Techniques.Bounce)
                .duration(700)
                .repeat(100)
                .playOn(btnChooseLight);
        checkRole();
        checkPermissionGps();
        checkPermissionStorage();
        checkPermissionCall();
        requestContactsPermissions();
        startServicesChild();

        if (role.equals("CHILD")) {
            getEmailParentOfConnectedChild();


        } else if (role.equals("PARENT")) {
            EMAIL_PARENT_OF_CONNECTED_CHILD = userConnected;
            displayChild();
            getFragmentManager().beginTransaction().add(R.id.displayContainer, new HomeFragment()).commit();
            applicationControl();

        }
        checkAuthentification();

        showSpaceNavigation();

        toolbar = findViewById(R.id.tool_bar);
        toolbar.setSubtitle("Welcome");
        toolbar.setSubtitleTextColor(Color.BLACK);
        toolbar.setTitleTextColor(Color.BLACK);
        setSupportActionBar(toolbar);
    }


    @Override
    protected void onResume() {
        super.onResume();

        SharedPreferences prefs = getSharedPreferences("USER", MODE_PRIVATE);
        String restoredText = prefs.getString("regId", null);
        if (restoredText != null) {
            String tokken = prefs.getString("regId", "No name defined");//"No name defined" is the default value.

            Response.Listener<String> responseListener3 = new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {

                }
            };

            RefreshTokkenRequest addlocation = new RefreshTokkenRequest(userConnected, tokken, responseListener3);
            RequestQueue queue = Volley.newRequestQueue(MainActivity.this);

            queue.add(addlocation);
        }
        btnChooseChild.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                frg = new ChooseChildFragment();
                showChooseChildrenDialog();
            }
        });

        btnChooseLight.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this, SettingEnableUninstallActivity.class);
                startActivity(intent);
            }
        });
        // activateGps();

        btnChooseProfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Response.Listener<String> responseListener3 = new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        listParent.clear();

                        try {

                            JSONObject jsonResponse = new JSONObject(response);
                            JSONArray jsonArray = jsonResponse.getJSONArray("parent");
                            int count = 0;
                            while (count < jsonArray.length()) {
                                JSONObject jo = jsonArray.getJSONObject(count);
                                listParent.add(new Parent(jo.getString("email"),
                                        jo.getString("password"), jo.getString("username"), jo.getString("picture"),
                                        jo.getString("phone")));
                                count++;

                            }
                            Intent intent = new Intent(MainActivity.this, DiagonalLayoutMainActivity.class);
                            startActivity(intent);

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                };

                GetParentByEmailRequest addlocation = new GetParentByEmailRequest(userConnected, responseListener3);
                RequestQueue queue = Volley.newRequestQueue(MainActivity.this);
                queue.add(addlocation);

            }
        });
    }


    public void getEmailParentOfConnectedChild() {
        childrenList = new ArrayList<>();
        Response.Listener<String> responseListener = new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    JSONArray jsonArray = jsonObject.getJSONArray("oneChild");
                    int count = 0;
                    while (count < jsonArray.length()) {
                        JSONObject jo = jsonArray.getJSONObject(count);
                        childrenList.add(new Child(jo.getString("code"), jo.getString("email"), jo.getString("username")
                                , jo.getString("phone"), jo.getString("image_name"), jo.getString("email_parent"), jo.getString("birthdate"), jo.getString("image_qr")));
                        count++;
                    }
                    if (childrenList.size() != 0) {
                        EMAIL_PARENT_OF_CONNECTED_CHILD = childrenList.get(0).getEmail_parent();
                        Intent intent = new Intent(MainActivity.this, Home_Child_Activity.class);
                        startActivity(intent);
                    }

                } catch (JSONException e) {

                    e.printStackTrace();
                }
            }
        };
        Response.ErrorListener responseListener5 = new Response.ErrorListener() {


            @Override
            public void onErrorResponse(VolleyError error) {

            }
        };
        GetOneChildByEmailRequest getOneChildByEmailRequest = new GetOneChildByEmailRequest(FirebaseAuth.getInstance().getCurrentUser().getEmail(), responseListener, responseListener5);
        RequestQueue queue = Volley.newRequestQueue(this);
        queue.add(getOneChildByEmailRequest);


    }


    public void startServicesChild() {
        if (role.equals("PARENT")) {
            Intent intent3 = new Intent(this, PickMeUpParent.class);
            this.startService(intent3);

        } else if (role.equals("CHILD")) {

            Intent intent1 = new Intent(this, SendingLocation.class);
            this.startService(intent1);
            Intent intent5 = new Intent(this, getStepService.class);
            this.startService(intent5);
            Intent intent4 = new Intent(this, PickMeChildResponse.class);
            this.startService(intent4);
            Intent intent6 = new Intent(this, ForwardCallRespose.class);
            this.startService(intent6);
            Intent intent7 = new Intent(this, DisableForwardCallRespose.class);
            this.startService(intent7);
            Intent intent8 = new Intent(this, GestureDetection.class);
            this.startService(intent8);

        }

    }

    public void checkPermissionGps() {

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION)
                != PackageManager.PERMISSION_GRANTED &&
                (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)
                        != PackageManager.PERMISSION_GRANTED)) {

            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION,
                    Manifest.permission.ACCESS_COARSE_LOCATION}, MY_PERMISSION_REQUEST_CODE);


        }
    }

    public void checkPermissionCall() {

        if (ContextCompat.checkSelfPermission(MainActivity.this,
                Manifest.permission.READ_PHONE_STATE)
                != PackageManager.PERMISSION_GRANTED) {

            ActivityCompat.requestPermissions(MainActivity.this,
                    new String[]{Manifest.permission.READ_PHONE_STATE},
                    MY_PERMISSIONS_REQUEST_CALL_PHONE);
        } else {


        }

    }

    private void requestContactsPermissions() {
        // BEGIN_INCLUDE(contacts_permission_request)
        if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                Manifest.permission.READ_CONTACTS)
                || ActivityCompat.shouldShowRequestPermissionRationale(this,
                Manifest.permission.WRITE_CONTACTS)) {


        } else {
            // Contact permissions have not been granted yet. Request them directly.
            ActivityCompat.requestPermissions(this, PERMISSIONS_CONTACT, MY_PERMISSION_SMS);
        }
        // END_INCLUDE(contacts_permission_request)
    }

    public void checkPermissionStorage() {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED &&
                (ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE)
                        != PackageManager.PERMISSION_GRANTED)) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION,
                    Manifest.permission.READ_EXTERNAL_STORAGE}, CODE_GALLERY_REQUEST);
        }
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        spaceNavigationView.onSaveInstanceState(outState);
    }

    public void checkAuthentification() {

        firebaseAuthListener = new FirebaseAuth.AuthStateListener() {

            @Override
            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
                if (firebaseAuth.getCurrentUser() == null) {
                    startActivity(new Intent(MainActivity.this, LoginActivity.class));
                }
            }
        };
    }

    public void showSpaceNavigation() {

        spaceNavigationView.addSpaceItem(new SpaceItem("CHILDREN", R.drawable.account));
        spaceNavigationView.addSpaceItem(new SpaceItem("LOCATION", R.drawable.google_maps));
        spaceNavigationView.addSpaceItem(new SpaceItem("ZONE", R.drawable.ic_allowed));
        spaceNavigationView.addSpaceItem(new SpaceItem("CHAT", R.drawable.ic_lock_out));
        spaceNavigationView.shouldShowFullBadgeText(true);
        spaceNavigationView.setCentreButtonIconColorFilterEnabled(false);
        spaceNavigationView.showIconOnly();
        spaceNavigationView.setSpaceOnClickListener(new SpaceOnClickListener() {
            @Override
            public void onCentreButtonClick() {
                getFragmentManager().beginTransaction().replace(R.id.displayContainer, new HomeFragment()).addToBackStack(null).commit();

                spaceNavigationView.shouldShowFullBadgeText(true);
            }

            @Override
            public void onItemClick(int itemIndex, String itemName) {

                if (itemIndex == 0) {
                    getFragmentManager().beginTransaction().replace(R.id.displayContainer, new DisplayChildFragment()).addToBackStack(null).commit();

                }
                if (itemIndex == 1) {

                    Intent intent = new Intent(MainActivity.this, TrackingMapActivity.class);
                    startActivity(intent);
                }

                if (itemIndex == 2) {
                    Intent intent = new Intent(MainActivity.this, CreationZoneMap.class);
                    startActivity(intent);
                }
                if (itemIndex == 3) {
                    firebaseAuth.signOut();
                    Intent intent = new Intent(MainActivity.this, LoginActivity.class);
                    startActivity(intent);
                }

            }

            @Override
            public void onItemReselected(int itemIndex, String itemName) {
                if (itemIndex == 0) {
                    getFragmentManager().beginTransaction().replace(R.id.displayContainer, new DisplayChildFragment()).addToBackStack(null).commit();

                }
                if (itemIndex == 1) {

                    Intent intent = new Intent(MainActivity.this, TrackingMapActivity.class);
                    startActivity(intent);
                }

                if (itemIndex == 2) {
                    Intent intent = new Intent(MainActivity.this, CreationZoneMap.class);
                    startActivity(intent);
                }
                if (itemIndex == 3) {
                    firebaseAuth.signOut();
                    Intent intent = new Intent(MainActivity.this, LoginActivity.class);
                    startActivity(intent);
                }
            }
        });

        spaceNavigationView.setSpaceOnLongClickListener(new SpaceOnLongClickListener() {
            @Override
            public void onCentreButtonLongClick() {

            }

            @Override
            public void onItemLongClick(int itemIndex, String itemName) {
                //Toast.makeText(MainActivity.this, itemIndex + " " + itemName, Toast.LENGTH_SHORT).show();
            }
        });

    }

    public void showChooseChildrenDialog() {
        DialogFragment newFragment = new ChooseChildFragment();
        newFragment.show(getFragmentManager(), "chooseChildrenDialog");

    }

    public void confirFireMisselsPickMeUp() {
        DialogFragment newFragment = new PickMeUpDialog();
        newFragment.show(getFragmentManager(), "missiles");
    }


    public void applicationControl() {
        //ADMIN DEVICE//
        ComponentName devAdminReceiver = new ComponentName(this, LockedPhoneReceiver.class);
        DevicePolicyManager dpm = (DevicePolicyManager) this.getSystemService(Context.DEVICE_POLICY_SERVICE);
        SharedPreferences pref = PreferenceManager.getDefaultSharedPreferences(this);
        dpm.isAdminActive(devAdminReceiver);
        //AMIN DIVICE//

        List<ApplicationInfo> packages;
        PackageManager pm;
        pm = getPackageManager();
        //get a list of installed apps.
        packages = pm.getInstalledApplications(0);

        ActivityManager mActivityManager = (ActivityManager) this.getSystemService(Context.ACTIVITY_SERVICE);
        String myPackage = getApplicationContext().getPackageName();
        for (ApplicationInfo packageInfo : packages) {
            if ((packageInfo.flags & ApplicationInfo.FLAG_SYSTEM) == 1) continue;
            if (packageInfo.packageName.equals(myPackage)) continue;
            Log.e("PackageName :", packageInfo.packageName);
            mActivityManager.killBackgroundProcesses(packageInfo.packageName);

        }
    }


    public void checkRole() {

        if (userConnected.contains("@guardian.com")) {
            role = "CHILD";
            SharedPreferences pref = getApplicationContext().getSharedPreferences("USER", 0);
            SharedPreferences.Editor editor = pref.edit();
            editor.putString("role", role);
            editor.commit();
        } else {
            role = "PARENT";




        }
    }

    public void InitialiseOnlineReference() {

        onlineRef = FirebaseDatabase.getInstance().getReference().child(".info/connected");
        counterRef = FirebaseDatabase.getInstance().getReference().child("lastOnline");
        currentUserRef = FirebaseDatabase.getInstance().getReference("lastOnline")
                .child(FirebaseAuth.getInstance().getCurrentUser().getUid());

        setupSystem();
    }

    public void setupSystem() {
        onlineRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if (dataSnapshot.getValue(Boolean.class)) {
                    currentUserRef.onDisconnect().removeValue();
                    counterRef.child(FirebaseAuth.getInstance().getCurrentUser().getUid())
                            .setValue(new User(FirebaseAuth.getInstance().getCurrentUser().getEmail(), "Online"));
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
        counterRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                for (DataSnapshot postSnapshot : dataSnapshot.getChildren()) {
                    user = postSnapshot.getValue(User.class);

                    Log.e("Connected", user.toString());
                }

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    public void displayChild() {


        RequestQueue mRequestQueue;
        childrenListMainActivity = new ArrayList<>();
        Response.Listener<String> responseListener = new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    JSONArray jsonArray = jsonObject.getJSONArray("child");
                    int count = 0;


                    while (count < jsonArray.length()) {
                        JSONObject jo = jsonArray.getJSONObject(count);
                        childrenListMainActivity.add(new Child(jo.getString("code"), jo.getString("email"), jo.getString("username")
                                , jo.getString("phone"), jo.getString("image_name"), jo.getString("email_parent"), jo.getString("birthdate"), jo.getString("image_qr")));
                        count++;


                    }


                } catch (JSONException e) {

                    e.printStackTrace();
                }
            }
        };
        Response.ErrorListener responseListener5 = new Response.ErrorListener() {


            @Override
            public void onErrorResponse(VolleyError error) {


            }
        };
        GetChildrenByEmailParentRequest getChildrenByEmailParentRequest = new GetChildrenByEmailParentRequest(userConnected, responseListener, responseListener5);
        MySingleton.getInstance(this).addToRequestQueue(getChildrenByEmailParentRequest);
    }
}