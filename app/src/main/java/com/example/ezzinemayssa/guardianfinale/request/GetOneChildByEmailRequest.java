package com.example.ezzinemayssa.guardianfinale.request;

import android.util.Log;

import com.android.volley.Response;
import com.android.volley.toolbox.StringRequest;

import java.util.HashMap;
import java.util.Map;

import static com.example.ezzinemayssa.guardianfinale.activity.MainActivity.ipAdress;

/**
 * Created by Ezzine Mayssa on 19/11/2017.
 */

public class GetOneChildByEmailRequest extends StringRequest {


    private static final String REGISTER_REQUEST_URL = "http://"+ipAdress+"/guardenScript/getChildrenByEmail.php";
    private Map<String, String> params;

    public GetOneChildByEmailRequest(String email, Response.Listener<String> listener,Response.ErrorListener errorListener) {
       super(Method.POST, REGISTER_REQUEST_URL,listener,errorListener);
        params = new HashMap<>();
        params.put("email",email);
        Log.e("response","get");

    }

    @Override
    public Map<String, String> getParams() {
        return params;
    }
}
