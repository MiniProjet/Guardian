package com.example.ezzinemayssa.guardianfinale.adapter;

/**
 * Created by Shade on 5/9/2016.
 */

import android.app.Activity;
import android.app.AlertDialog;
import android.app.DialogFragment;
import android.app.FragmentManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.toolbox.Volley;
import com.example.ezzinemayssa.guardianfinale.R;
import com.example.ezzinemayssa.guardianfinale.activity.LoginActivity;
import com.example.ezzinemayssa.guardianfinale.activity.RegisterActivity;
import com.example.ezzinemayssa.guardianfinale.activity.map.QRActivity;
import com.example.ezzinemayssa.guardianfinale.entities.ChatModel;
import com.example.ezzinemayssa.guardianfinale.entities.Child;
import com.example.ezzinemayssa.guardianfinale.entities.ForwardCall;
import com.example.ezzinemayssa.guardianfinale.fragment.EditPropChildFragment;
import com.example.ezzinemayssa.guardianfinale.fragment.ListOfZoneByChild;
import com.example.ezzinemayssa.guardianfinale.request.DeleteChildRequest;
import com.example.ezzinemayssa.guardianfinale.utils.MySingleton;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.squareup.picasso.Callback;
import com.squareup.picasso.MemoryPolicy;
import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import static com.example.ezzinemayssa.guardianfinale.fragment.DisplayChildFragment.phoneParent;


public class RecyclerAdapterDisplayChildren extends RecyclerView.Adapter<RecyclerAdapterDisplayChildren.ViewHolder> {

    private Child current;
    private Context context;
    List<Child> listChildren = new ArrayList<>();
    private FirebaseAuth firebaseAuth;
    private LayoutInflater inflater;
    int posItem;
    public static String pathQR;

    public static String emailChildSelected;
    DatabaseReference dbRef;

    AlertDialog.Builder builder;
    FragmentManager manager,manager2;


    // create constructor to innitilize context and data sent from MainActivity
    public RecyclerAdapterDisplayChildren(Context context, List<Child> listChildren) {
        this.context = context;
        this.listChildren = listChildren;

        //inflater= LayoutInflater.from(context);
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        ImageView itemProfilePicture;
        public ImageView itemQrCode;
        TextView itemUsername;
        TextView itemBirthday;
        TextView itemPhone;
        TextView contentRequestBtnEdit;
        TextView contentRequestBtnDelete;
        TextView contentRequestBtnEnableForward;
        TextView contentRequestBtnDisableForward;
        ProgressBar progressBarImageProfile;
        ProgressBar progressBarQR;

        TextView listZone;

        private ViewHolder(View itemView) {
            super(itemView);

            contentRequestBtnDelete = itemView.findViewById(R.id.content_request_btn_delete);
            contentRequestBtnEdit = itemView.findViewById(R.id.content_request_btn_edit);
            contentRequestBtnEnableForward = itemView.findViewById(R.id.btnEnableForward);
            contentRequestBtnDisableForward = itemView.findViewById(R.id.btnDisableForward);
            listZone = itemView.findViewById(R.id.zonelist);
            itemUsername = itemView.findViewById(R.id.username_displayDetailsContent);
            itemPhone = itemView.findViewById(R.id.phone_displayDetailsContent);
            itemBirthday = itemView.findViewById(R.id.birthdate_displayDetailsContent);
            itemQrCode = itemView.findViewById(R.id.qrCodeDisplayChildDetailsContent);
            itemProfilePicture = itemView.findViewById(R.id.imageProfileDisplayChildDeatilsContent);
            progressBarImageProfile = itemView.findViewById(R.id.progressBarImageProfile);
            progressBarQR = itemView.findViewById(R.id.progressBarQR);


            contentRequestBtnDelete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    posItem = getAdapterPosition();

                    AlertDialog.Builder builder = new AlertDialog.Builder(context);
                    builder.setTitle("Confirm to delete " + listChildren.get(posItem).getUsername());
                    builder.setMessage("You are about to delete " + listChildren.get(posItem).getUsername() + ". Do you really want to proceed ?");
                    builder.setCancelable(false);
                    builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {

                            deleteChild(posItem);
                            listChildren.remove(posItem);
                            notifyDataSetChanged();

                        }
                    });

                    builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.cancel();
                        }
                    });

                    builder.show();

                }
            });

            contentRequestBtnEnableForward.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    posItem = getAdapterPosition();

                    AlertDialog.Builder builder = new AlertDialog.Builder(context);
                    builder.setTitle("Enable forward Call");
                    builder.setMessage("You are about to enable forward call of " + listChildren.get(posItem).getPhone() + ". Do you really want to proceed ?");
                    builder.setCancelable(false);
                    builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dbRef = FirebaseDatabase.getInstance().getReference().child("enableForwardCallRef");
                            dbRef.child(listChildren.get(posItem).getCode()).setValue(new ForwardCall(listChildren.get(posItem).getEmail(), firebaseAuth.getCurrentUser().getEmail(), phoneParent));
                        }
                    });

                    builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.cancel();
                        }
                    });

                    builder.show();

                }
            });
            contentRequestBtnDisableForward.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    posItem = getAdapterPosition();

                    AlertDialog.Builder builder = new AlertDialog.Builder(context);
                    builder.setTitle("Disable forward Call");
                    builder.setMessage("You are about to disable forward call of " + listChildren.get(posItem).getPhone() + ". Do you really want to proceed ?");
                    builder.setCancelable(false);
                    builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dbRef = FirebaseDatabase.getInstance().getReference().child("disableForwardCallRef");
                            dbRef.child(listChildren.get(posItem).getCode()).setValue(new ForwardCall(listChildren.get(posItem).getEmail(), firebaseAuth.getCurrentUser().getEmail(), phoneParent));
                        }
                    });

                    builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.cancel();
                        }
                    });

                    builder.show();

                }
            });
            listZone.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    posItem = getAdapterPosition();
                    emailChildSelected = listChildren.get(posItem).getEmail();
                    manager2 = ((Activity) context).getFragmentManager();
                    manager2.beginTransaction().replace(R.id.displayContainer, new ListOfZoneByChild()).addToBackStack(null).commit();

                }
            });

            contentRequestBtnEdit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    posItem = getAdapterPosition();
                    emailChildSelected = listChildren.get(posItem).getEmail();
                    confirmEditDialog();
                }
            });


            itemQrCode.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (view.getScaleX() == 1) {
                        posItem = getAdapterPosition();
                        pathQR = listChildren.get(posItem).getImage_qr();
                        Intent intent = new Intent(context, QRActivity.class);
                        context.startActivity(intent);

                    } else {
                        view.setScaleY(1);
                        view.setScaleX(1);
                    }
                }
            });


        }
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_display_child, viewGroup, false);
        ViewHolder viewHolder = new ViewHolder(v);
        return viewHolder;


    }


    @Override
    public void onBindViewHolder(ViewHolder viewHolder, int i) {
        final ViewHolder myHolder = (ViewHolder) viewHolder;
        firebaseAuth = FirebaseAuth.getInstance();
        current = listChildren.get(i);

        myHolder.itemUsername.setText("Name: " + current.getUsername());
        myHolder.itemPhone.setText("Phone: " + current.getPhone());
        myHolder.itemBirthday.setText("Birthdate: " + current.getBirthdate());


        Picasso.with(context).load(current.getImage_name()).networkPolicy(NetworkPolicy.NO_CACHE).into(myHolder.itemProfilePicture, new Callback() {
            @Override
            public void onSuccess() {
                myHolder.progressBarImageProfile.setVisibility(View.GONE);
                myHolder.itemProfilePicture.setVisibility(View.VISIBLE);
            }

            @Override
            public void onError() {

            }


        });
        Picasso.with(context).load(current.getImage_qr()).into(myHolder.itemQrCode, new Callback() {
            @Override
            public void onSuccess() {
                myHolder.progressBarQR.setVisibility(View.GONE);
                myHolder.itemQrCode.setVisibility(View.VISIBLE);
            }

            @Override
            public void onError() {

            }
        });


    }

    @Override
    public int getItemCount() {
        return listChildren.size();
    }

    public void deleteChild(int pos) {

        Response.Listener<String> responseListener = new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    JSONObject jsonResponse = new JSONObject(response);
                    String activated = jsonResponse.getString("deleteChild");
                    if (activated.equals("true")) {
                        Log.e("delete", "yes");
                    } else {
                        Log.e("delete", "noo");
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        };
        DeleteChildRequest deleteChildRequest = new DeleteChildRequest(listChildren.get(pos).getEmail(), responseListener);
        MySingleton.getInstance(context).addToRequestQueue(deleteChildRequest);

    }

    public void confirmEditDialog() {
        manager = ((Activity) context).getFragmentManager();
        DialogFragment newFragment = new EditPropChildFragment();
        newFragment.show(manager, "missiles");




    }
}