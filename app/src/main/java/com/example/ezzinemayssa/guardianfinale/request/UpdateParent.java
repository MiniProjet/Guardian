package com.example.ezzinemayssa.guardianfinale.request;

import android.util.Log;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.toolbox.StringRequest;
import com.example.ezzinemayssa.guardianfinale.activity.MainActivity;
import com.squareup.picasso.Downloader;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by mayss on 02/01/2018.
 */

public class UpdateParent extends StringRequest {


    private static final String REGISTER_REQUEST_URL = "http://"+ MainActivity.ipAdress+"/guardenScript/UpdateParent.php";
    private Map<String, String> params;

    public UpdateParent(String email, String username, String phone, String picture, String password, Response.Listener<String> listener, Response.ErrorListener resp) {
        super(Request.Method.POST, REGISTER_REQUEST_URL, listener, resp);
        params = new HashMap<>();
Log.e("email",email.trim());
        params.put("email",email);
        params.put("username",username);
        params.put("phone",phone+"");
        params.put("picture",picture+"");
        params.put("password",password+"");



    }

    @Override
    public Map<String, String> getParams() {
        return params;
    }
}