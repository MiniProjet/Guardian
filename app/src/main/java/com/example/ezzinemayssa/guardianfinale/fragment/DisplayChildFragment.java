package com.example.ezzinemayssa.guardianfinale.fragment;

import android.app.DialogFragment;
import android.app.Fragment;
import android.app.FragmentTransaction;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;

import com.android.volley.Cache;
import com.android.volley.Network;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.BasicNetwork;
import com.android.volley.toolbox.DiskBasedCache;
import com.android.volley.toolbox.HurlStack;
import com.android.volley.toolbox.Volley;
import com.example.ezzinemayssa.guardianfinale.R;
import com.example.ezzinemayssa.guardianfinale.activity.map.AppercuChildMap;
import com.example.ezzinemayssa.guardianfinale.adapter.RecyclerAdapterDisplayChildren;
import com.example.ezzinemayssa.guardianfinale.entities.Child;
import com.example.ezzinemayssa.guardianfinale.entities.ForwardCall;
import com.example.ezzinemayssa.guardianfinale.entities.Parent;
import com.example.ezzinemayssa.guardianfinale.request.GetChildrenByEmailParentRequest;
import com.example.ezzinemayssa.guardianfinale.request.GetParentByEmailRequest;
import com.example.ezzinemayssa.guardianfinale.utils.MySingleton;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import java.util.ArrayList;
import java.util.List;

import static android.content.Context.MODE_PRIVATE;
import static com.example.ezzinemayssa.guardianfinale.activity.MainActivity.btnChooseChild;


public class DisplayChildFragment extends Fragment {

    private FirebaseAuth firebaseAuth;
    public static RecyclerView recyclerViewDisplayChild;
    public static RecyclerView.Adapter adapterDisplayChild;
    public static  List<Child> childrenListDisplayChild;
    private List<Parent> parentListt ;
    FloatingActionButton btnAddChild;
    public static String phoneParent;
    ProgressDialog progress;
    String userConnected;
    ImageView emptyChild;
    public DisplayChildFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        SharedPreferences prefs = this.getActivity().getSharedPreferences("USER", MODE_PRIVATE);
        String restoredText = prefs.getString("userConnected", null);
        if (restoredText != null) {
            userConnected = prefs.getString("userConnected", "No name defined");//"No name defined" is the default value.

            Log.e("user",userConnected);
        }


        // Inflate the layout for this fragment
        View view= inflater.inflate(R.layout.fragment_display_child, container, false);
        btnChooseChild.setVisibility(View.GONE);
        emptyChild = view.findViewById(R.id.emptyChildDisplay);
        recyclerViewDisplayChild = view.findViewById(R.id.recycler_view);
        btnAddChild = view.findViewById(R.id.btnAddChildren);
        progress = new ProgressDialog(getActivity());
        progress.setTitle("Please wait..");
        progress.setMessage("Processing   ");
        progress.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progress.setIndeterminate(true);


        firebaseAuth = FirebaseAuth.getInstance();

        btnAddChild.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                confirmFireMissiles();
            }
        });




        return view;
    }


    @Override
    public void onResume() {
        super.onResume();
        displayChild();
        getParentPropreties();
    }
    public void confirmFireMissiles() {
        DialogFragment newFragment = new AddChildFragment();
        newFragment.show(getFragmentManager(), "missiles");


    }
    public  void displayChild()
    {

        progress.show();
        RequestQueue mRequestQueue;
        Cache cache = new DiskBasedCache(getActivity().getCacheDir(), 1024 * 1024); // 1MB cap
        Network network = new BasicNetwork(new HurlStack());
        mRequestQueue = new RequestQueue(cache, network);
        mRequestQueue.start();
        childrenListDisplayChild = new ArrayList<>();
        Response.Listener<String> responseListener = new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    JSONObject jsonObject= new JSONObject(response);
                    JSONArray jsonArray= jsonObject.getJSONArray("child");
                    int count =0;


                    while(count< jsonArray.length())
                    {
                        JSONObject jo= jsonArray.getJSONObject(count);
                        childrenListDisplayChild.add(new Child(jo.getString("code"),jo.getString("email"),jo.getString("username")
                                ,jo.getString("phone"),jo.getString("image_name"),jo.getString("email_parent"),jo.getString("birthdate"),jo.getString("image_qr")));
                        count++;


                    }
                    if(childrenListDisplayChild.size() != 0) {
                        adapterDisplayChild = new RecyclerAdapterDisplayChildren(getActivity(), childrenListDisplayChild);
                        recyclerViewDisplayChild.setLayoutManager(new LinearLayoutManager(getActivity()));
                        recyclerViewDisplayChild.setAdapter(adapterDisplayChild);
                        recyclerViewDisplayChild.invalidate();

                    }
                    else
                    {
                        emptyChild.setVisibility(View.VISIBLE);
                    }
                    progress.dismiss();

                } catch (JSONException e) {

                    e.printStackTrace();
                }
            }
        };
        Response.ErrorListener responseListener5 = new Response.ErrorListener() {


            @Override
            public void onErrorResponse(VolleyError error) {
                progress.dismiss();

            }
        };
        GetChildrenByEmailParentRequest getChildrenByEmailParentRequest = new GetChildrenByEmailParentRequest(userConnected, responseListener,responseListener5);
        RequestQueue queue = MySingleton.getInstance(getActivity()).getRequestQueue();
// Add a request (in this example, called stringRequest) to your RequestQueue.
        MySingleton.getInstance(getActivity()).addToRequestQueue(getChildrenByEmailParentRequest);
    }
    public void getParentPropreties()
    {
        parentListt = new ArrayList<>();

        Response.Listener<String> responseListener = new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    JSONObject jsonObject= new JSONObject(response);
                    JSONArray jsonArray= jsonObject.getJSONArray("parent");
                    int count =0;


                    while(count< jsonArray.length())
                    {
                        JSONObject jo= jsonArray.getJSONObject(count);
                        parentListt.add(new Parent(jo.getString("email"),jo.getString("password"),jo.getString("username")
                                ,jo.getString("picture"),jo.getString("phone")));
                        count++;

                    }

                    if(parentListt.size()!=0) {
                        phoneParent = parentListt.get(0).getPhone();
                    }
                } catch (JSONException e) {

                    e.printStackTrace();
                }
            }
        };
        GetParentByEmailRequest getParentByEmailRequest = new GetParentByEmailRequest(userConnected, responseListener);
        RequestQueue queue = MySingleton.getInstance(getActivity()).getRequestQueue();
// Add a request (in this example, called stringRequest) to your RequestQueue.
        MySingleton.getInstance(getActivity()).addToRequestQueue(getParentByEmailRequest);


    }


}
