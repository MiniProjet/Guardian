package com.example.ezzinemayssa.guardianfinale.activity.map;

import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.WindowManager;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.toolbox.Volley;
import com.example.ezzinemayssa.guardianfinale.R;
import com.example.ezzinemayssa.guardianfinale.adapter.RecyclerAdapterChooseChildren;
import com.example.ezzinemayssa.guardianfinale.adapter.RecyclerAdapterDisplayChildren;
import com.example.ezzinemayssa.guardianfinale.entities.StepsChild;
import com.example.ezzinemayssa.guardianfinale.fragment.HomeFragment;
import com.example.ezzinemayssa.guardianfinale.fragment.StepsStatFragment;
import com.example.ezzinemayssa.guardianfinale.request.GetStepsByChild;
import com.example.ezzinemayssa.guardianfinale.service.getStepService;
import com.example.ezzinemayssa.guardianfinale.utils.MySingleton;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolylineOptions;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import static com.example.ezzinemayssa.guardianfinale.activity.MainActivity.listLocation;


public class AppercuChildMap extends FragmentActivity implements OnMapReadyCallback {
    private GoogleMap mMap;
    DatabaseReference statStepsResponse;
    StepsChild steps;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            getWindow().setStatusBarColor(Color.LTGRAY);
        }
        setContentView(R.layout.activity_appercu_child_map);
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
    }

    @Override
    protected void onResume() {
        super.onResume();
        getStepsChild();
    }

    public void getStepsChild(){
        steps = new StepsChild();
        Response.Listener<String> responseListener3 = new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {


                try {

                    JSONObject jsonResponse = new JSONObject(response);
                    JSONArray jsonArray= jsonResponse.getJSONArray("steps");
                    int count=0;
                    while(count<jsonArray.length())
                    {
                        JSONObject jo= jsonArray.getJSONObject(count);
                        steps =new StepsChild(jo.getString("email_child"),jo.getInt("hour1"),jo.getInt("hour2"),jo.getInt("hour3")
                                ,jo.getInt("hour4"),jo.getInt("hour5"),jo.getInt("hour6"),jo.getInt("hour7")
                                ,jo.getInt("hour8"),jo.getInt("hour9"),jo.getInt("hour10"),jo.getInt("hour11")
                                ,jo.getInt("hour12"),jo.getInt("hour13"),jo.getInt("hour14"),jo.getInt("hour15")
                                ,jo.getInt("hour16"),jo.getInt("hour17"),jo.getInt("hour18"),jo.getInt("hour19")
                                ,jo.getInt("hour20"),jo.getInt("hour21"),jo.getInt("hour22"),jo.getInt("hour23")
                        );

                        count++;
                    }


                    StepsStatFragment stepsFragment= new StepsStatFragment();
                    Bundle bundle = new Bundle();
                    bundle.putInt("hour0",steps.Hours0);
                    bundle.putInt("hour1",steps.Hours1);
                    bundle.putInt("hour2",steps.Hours2);
                    bundle.putInt("hour3",steps.Hours3);
                    bundle.putInt("hour4",steps.Hours4);
                    bundle.putInt("hour5",steps.Hours5);
                    bundle.putInt("hour6",steps.Hours6);
                    bundle.putInt("hour7",steps.Hours7);
                    bundle.putInt("hour8",steps.Hours8);
                    bundle.putInt("hour9",steps.Hours9);
                    bundle.putInt("hour10",steps.Hours10);
                    bundle.putInt("hour11",steps.Hours11);
                    bundle.putInt("hour12",steps.Hours12);
                    bundle.putInt("hour13",steps.Hours13);
                    bundle.putInt("hour14",steps.Hours14);
                    bundle.putInt("hour15",steps.Hours15);
                    bundle.putInt("hour16",steps.Hours16);
                    bundle.putInt("hour17",steps.Hours17);
                    bundle.putInt("hour18",steps.Hours18);
                    bundle.putInt("hour19",steps.Hours19);
                    bundle.putInt("hour20",steps.Hours20);
                    bundle.putInt("hour21",steps.Hours21);
                    bundle.putInt("hour22",steps.Hours22);
                    bundle.putInt("hour23",steps.Hours23);

                    stepsFragment.setArguments(bundle);
                    getFragmentManager().beginTransaction().add(R.id.contenaire,stepsFragment).commit();
                } catch (JSONException e) {
                    e.printStackTrace();
                }



            }
        };

        GetStepsByChild addlocation = new GetStepsByChild(RecyclerAdapterChooseChildren.email_child_chosen,responseListener3);
     //   RequestQueue queue5 = Volley.newRequestQueue(AppercuChildMap.this);
        RequestQueue queue = MySingleton.getInstance(AppercuChildMap.this).getRequestQueue();
// Add a request (in this example, called stringRequest) to your RequestQueue.
        MySingleton.getInstance(this).addToRequestQueue(addlocation);
       // queue.add(addlocation);







                }





    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        mMap.clear();
        for(int i = 0; i< HomeFragment.listLocation.size()-1; i++){
            mMap.addMarker(new MarkerOptions()
                    .position(new LatLng(Double.parseDouble(HomeFragment.listLocation.get(i).getLat()),
                            Double.parseDouble(HomeFragment.listLocation.get(i).getLng())))
                    .title("Marker in Sydney"));
            mMap.addMarker(new MarkerOptions()
                    .position(new LatLng(Double.parseDouble(HomeFragment.listLocation.get(i+1).getLat()),
                            Double.parseDouble(HomeFragment.listLocation.get(i+1).getLng())))
                    .title("Marker in Sydney"));
            mMap.addPolyline(new PolylineOptions().add(new LatLng(Double.parseDouble(HomeFragment.listLocation.get(i).getLat()),
                            Double.parseDouble(HomeFragment.listLocation.get(i).getLng()))
                    ,new LatLng(Double.parseDouble(HomeFragment.listLocation.get(i+1).getLat()),
                            Double.parseDouble(HomeFragment.listLocation.get(i+1).getLng()))));

        }


        CameraPosition cameraPosition = new CameraPosition.Builder().target(new LatLng(Double.parseDouble(HomeFragment.listLocation.get(HomeFragment.listLocation.size()-1).getLat()),
                Double.parseDouble(HomeFragment.listLocation.get(HomeFragment.listLocation.size()-1).getLng()))).zoom(15.0f).build();
        CameraUpdate cameraUpdate = CameraUpdateFactory.newCameraPosition(cameraPosition);
        mMap.moveCamera(cameraUpdate);
    }

 }
