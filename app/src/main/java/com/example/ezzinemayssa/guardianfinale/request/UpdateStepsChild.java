package com.example.ezzinemayssa.guardianfinale.request;

import android.util.Log;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.toolbox.StringRequest;
import com.example.ezzinemayssa.guardianfinale.activity.MainActivity;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by mayss on 18/12/2017.
 */

public class UpdateStepsChild extends StringRequest {


    private static final String REGISTER_REQUEST_URL = "http://"+ MainActivity.ipAdress+"guardenScript/UpdateStepsChild.php";
    private Map<String, String> params;

    public UpdateStepsChild(String emailChild, String hour, int steps, Response.Listener<String> listener) {
        super(Request.Method.POST, REGISTER_REQUEST_URL, listener, null);
        params = new HashMap<>();
        Log.e("update","kkkkkkkkk");
        params.put("email_child",emailChild);
        params.put("hour",hour);
        params.put("step",steps+"");



    }

    @Override
    public Map<String, String> getParams() {
        return params;
    }
}