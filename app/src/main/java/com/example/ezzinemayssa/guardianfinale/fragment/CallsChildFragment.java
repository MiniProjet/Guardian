package com.example.ezzinemayssa.guardianfinale.fragment;


import android.app.Fragment;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;

import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.toolbox.Volley;
import com.example.ezzinemayssa.guardianfinale.R;
import com.example.ezzinemayssa.guardianfinale.activity.map.AppercuChildMap;
import com.example.ezzinemayssa.guardianfinale.adapter.RecyclerAdapterDisplayCalls;
import com.example.ezzinemayssa.guardianfinale.adapter.RecyclerAdapterDisplayChildren;
import com.example.ezzinemayssa.guardianfinale.entities.Call;
import com.example.ezzinemayssa.guardianfinale.entities.Child;
import com.example.ezzinemayssa.guardianfinale.entities.ForwardCall;
import com.example.ezzinemayssa.guardianfinale.entities.Parent;
import com.example.ezzinemayssa.guardianfinale.entities.PickMeUp;
import com.example.ezzinemayssa.guardianfinale.request.GetCallsByEmailChildRequest;
import com.example.ezzinemayssa.guardianfinale.request.GetParentByEmailRequest;
import com.example.ezzinemayssa.guardianfinale.utils.MySingleton;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import devs.mulham.horizontalcalendar.HorizontalCalendar;
import devs.mulham.horizontalcalendar.HorizontalCalendarListener;

import static com.example.ezzinemayssa.guardianfinale.activity.MainActivity.btnChooseChild;
import static com.example.ezzinemayssa.guardianfinale.adapter.RecyclerAdapterChooseChildren.email_child_chosen;


public class CallsChildFragment extends Fragment {

    private FirebaseAuth firebaseAuth;
    RecyclerView recyclerView;
    RecyclerView.Adapter adapter;
    private List<Call> callsList ;
    private List<Parent> parentList ;
    Parent parent;
    String day;
    String month;
    Button forwardCall;
    Button disableForwardCall;

    String forwardCallNumberParent ="92505082";
    LinearLayout linearLayoutCall;
    ImageView empty;
    public CallsChildFragment() {
        // Required empty public constructor
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view= inflater.inflate(R.layout.fragment_calls_child, container, false);
        btnChooseChild.setVisibility(View.GONE);
        linearLayoutCall=view.findViewById(R.id.linearLayout_call);
        empty=view.findViewById(R.id.emptyCall);
        /** end after 1 month from now */
        Calendar endDate = Calendar.getInstance();
        endDate.add(Calendar.MONTH,0);
        /** start before 1 month from now */
        Calendar startDate = Calendar.getInstance();
        startDate.add(Calendar.MONTH, -1);
        HorizontalCalendar horizontalCalendar = new HorizontalCalendar.Builder(view, R.id.calendarView)
                .startDate(startDate.getTime())
                .endDate(endDate.getTime())
                .build();

        horizontalCalendar.setCalendarListener(new HorizontalCalendarListener() {
            @Override
            public void onDateSelected(Date date, int position) {
                day=date.getDate()+"";
                month=date.getMonth()+1+"";


                Log.e("day",day);
                Log.e("month",month);

                callsList = new ArrayList<>();
                Response.Listener<String> responseListener = new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject jsonObject= new JSONObject(response);
                            JSONArray jsonArray= jsonObject.getJSONArray("calls");
                            int count =0;


                            while(count< jsonArray.length())
                            {
                                JSONObject jo= jsonArray.getJSONObject(count);

                                callsList.add(new Call(jo.getString("state"),jo.getString("number"),Timestamp.valueOf(jo.getString("start_time"))
                                        ,jo.getString("contact_name"),jo.getString("email_child")));
                                count++;


                            }

                            adapter=new RecyclerAdapterDisplayCalls(getActivity(), callsList);
                            recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
                            recyclerView.setAdapter(adapter);
                            adapter.notifyDataSetChanged();

                            if(callsList.size()==0)
                            {

                                empty.setVisibility(View.VISIBLE);
                                recyclerView.setVisibility(View.GONE);
                            }
                            else
                            {

                                empty.setVisibility(View.GONE);
                                recyclerView.setVisibility(View.VISIBLE);
                            }

                        } catch (JSONException e) {

                            e.printStackTrace();
                        }
                    }
                };
                GetCallsByEmailChildRequest getCallsByEmailChildRequest = new GetCallsByEmailChildRequest(email_child_chosen, day,month,responseListener);
                RequestQueue queue = MySingleton.getInstance(getActivity()).getRequestQueue();
// Add a request (in this example, called stringRequest) to your RequestQueue.
                MySingleton.getInstance(getActivity()).addToRequestQueue(getCallsByEmailChildRequest);
            }
        });


        recyclerView = view.findViewById(R.id.recycler_view_calls);
        firebaseAuth = FirebaseAuth.getInstance();

//        forwardCall.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//
//              // enableForwardCallRef= FirebaseDatabase.getInstance().getReference().child("enableForwardCallRef");
//              //  enableForwardCallRef.child("enableForwardCallPropreties")
//                      //  .setValue(new ForwardCall("129841924@guardian.com",firebaseAuth.getCurrentUser().getEmail(),"+216"+forwardCallNumberParent));
//
//
//            }
//        });

        return view;
    }



}
