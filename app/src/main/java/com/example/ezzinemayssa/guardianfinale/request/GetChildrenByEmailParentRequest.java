package com.example.ezzinemayssa.guardianfinale.request;

import com.android.volley.Response;
import com.android.volley.toolbox.StringRequest;

import java.util.HashMap;
import java.util.Map;

import static com.example.ezzinemayssa.guardianfinale.activity.MainActivity.ipAdress;

/**
 * Created by Ezzine Mayssa on 19/11/2017.
 */

public class GetChildrenByEmailParentRequest extends StringRequest {


    private static final String REGISTER_REQUEST_URL = "http://"+ipAdress+"/guardenScript/getChildrenByEmailParent.php";
    private Map<String, String> params;

    public GetChildrenByEmailParentRequest(String email_parent, Response.Listener<String> listener,Response.ErrorListener errorListener) {
       super(Method.POST, REGISTER_REQUEST_URL,listener,errorListener);
        params = new HashMap<>();
        params.put("email_parent",email_parent);

    }

    @Override
    public Map<String, String> getParams() {
        return params;
    }
}
