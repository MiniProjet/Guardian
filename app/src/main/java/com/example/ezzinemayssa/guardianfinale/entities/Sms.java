package com.example.ezzinemayssa.guardianfinale.entities;

import java.sql.Timestamp;

/**
 * Created by Ezzine Mayssa on 07/12/2017.
 */

public class Sms {
    int id;
    String state;
    String number;
    Timestamp time;
    String name;
    String content;
    String email_child;

    public Sms() {
    }

    public Sms(String state, String number, Timestamp time, String name, String content, String email_child) {
        this.state = state;
        this.number = number;
        this.time = time;
        this.name = name;
        this.content = content;
        this.email_child = email_child;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public Timestamp getTime() {
        return time;
    }

    public void setTime(Timestamp time) {
        this.time = time;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getEmail_child() {
        return email_child;
    }

    public void setEmail_child(String email_child) {
        this.email_child = email_child;
    }

    @Override
    public String toString() {
        return "Sms{" +
                "id=" + id +
                ", state='" + state + '\'' +
                ", number='" + number + '\'' +
                ", time=" + time +
                ", name='" + name + '\'' +
                ", content='" + content + '\'' +
                ", email_child='" + email_child + '\'' +
                '}';
    }
}
